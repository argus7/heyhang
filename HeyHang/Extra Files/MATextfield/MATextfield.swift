//
//  MATextfield.swift
//  Ajjerlli
//
//  Created by Anup Kumar on 4/30/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

class MATextField: DTTextField {
    
    var bottomLbl: UILabel?
    
    override func draw(_ rect: CGRect) {
        self.setupBottomLbl()
    }
    private func setupBottomLbl() {
        var frame = self.bounds
        frame.size.height = 1
        frame.origin.y = self.bounds.height - 1
        bottomLbl = UILabel(frame: frame)
        bottomLbl?.backgroundColor = UIColor.lightGray
        self.bringSubviewToFront(bottomLbl!)
        self.addSubview(bottomLbl!)
    }
}
