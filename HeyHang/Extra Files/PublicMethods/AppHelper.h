//
//  AppHelper.h
//  Motary
//
//  Created by Yatharth Singh on 05/01/16.
//  Copyright © 2016 Yatharth Singh. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AppHelper : NSObject {
    
}
+(void)alert:(NSString*)alertTitle message:(NSString*)alertMessage cancelButtonTitle:(NSString*)cnclTitle otherButtonTitles:(NSString*)othrTitle;
+(NSString *)HourCalculation:(NSDate *)PostDate;
@end
