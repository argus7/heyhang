    //
//  AppHelper.m
//  Motary
//
//  Created by Yatharth Singh on 05/01/16.
//  Copyright © 2016 Yatharth Singh. All rights reserved.
//


#import "AppHelper.h"
#import <ImageIO/ImageIO.h>

@implementation AppHelper

+(void)alert:(NSString*)alertTitle message:(NSString*)alertMessage cancelButtonTitle:(NSString*)cnclTitle otherButtonTitles:(NSString*)othrTitle
{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:alertTitle message:alertMessage delegate:nil cancelButtonTitle:cnclTitle otherButtonTitles:othrTitle, nil];
    [alert show];
}

+(NSString *)HourCalculation:(NSDate *)PostDate

{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    //    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    //[NSTimeZone timeZoneWithAbbreviation:@"UTC"]
    [dateFormat setTimeZone:[NSTimeZone localTimeZone]];
    NSDate *ExpDate = [dateFormat dateFromString:[dateFormat stringFromDate:PostDate]];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSDayCalendarUnit|NSWeekCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit|NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:ExpDate toDate:[NSDate date] options:0];
    NSString *time;
    if(components.year!=0)
    {
        if(components.year==1)
        {
            time=[NSString stringWithFormat:@"%ld %@",(long)components.year, @"year"];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld %@",(long)components.year, @"years"];
        }
    }
    else if(components.month!=0)
    {
        if(components.month==1)
        {
            time=[NSString stringWithFormat:@"%ld %@",(long)components.month, @"month"];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld %@",(long)components.month,@"months"];
        }
    }
    else if(components.week!=0)
    {
        if(components.week==1)
        {
            time=[NSString stringWithFormat:@"%ld %@",(long)components.week, @"week"];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld %@",(long)components.week, @"weeks"];
        }
    }
    else if(components.day!=0)
    {
        if(components.day==1)
        {
            time=[NSString stringWithFormat:@"%ld %@",(long)components.day,@"day"];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld %@",(long)components.day,@"days"];
        }
    }
    else if(components.hour!=0)
    {
        if(components.hour==1)
        {
            time=[NSString stringWithFormat:@"%ld %@",(long)components.hour,@"hour"];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld %@",(long)components.hour,@"hours"];
        }
    }
    else if(components.minute!=0)
    {
        if(components.minute==1)
        {
            time= [NSString stringWithFormat:@"%ld %@",(long)components.minute,@"min"];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld %@",(long)components.minute,@"mins"];
        }
    }
    else if(components.second>=0)
    {
        if(components.second==0)
        {
            time=[NSString stringWithFormat:@"1 %@",@"sec"];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld %@",(long)components.second,@"secs"];
        }
    }
    return [NSString stringWithFormat:@"%@",time];
}
@end
