//
//  Extention_ViewController.swift
//  SaudiCalendar
//
//  Created by TechGropse on 10/16/18.
//  Copyright © 2018 TechGropse Pvt Limited. All rights reserved.
//

import Foundation
import CoreLocation
import Alamofire
import UIKit
import Alamofire
import AKSideMenu
//import ReachabilitySwift

enum dateComparedBy {
    case lessThan
    case greaterThan
    case equal
}

extension UIViewController {
    
    public var isLogged : Bool {
        get {
            return UserDefaults.standard.bool(forKey: Constant.userDefaults.kIsRegistered)
        }
    }
    public var widthRatio : CGFloat {
        get {
            return UIScreen.main.bounds.size.width / 320
        }
    }
    public var heightRatio : CGFloat {
        get {
            return UIScreen.main.bounds.size.height / 568
        }
    }
    open var isArabicActive : Bool {
        get {
            let str = UserDefaults.standard.string(forKey: Constant.userDefaults.kLanguageSelected) ?? ""
            return str == "ar" ? true : false
        }
    }
    func getAttText(bySelectedText: String,string: String, fontName: UIFont) -> NSAttributedString {
        let myString : NSString = string as NSString
        let changeText = bySelectedText
        
        let range = (myString).range(of: changeText)
        let attribute = NSMutableAttributedString(string: myString as String)
        let fontAttribute = [ NSAttributedString.Key.font: fontName]
        attribute.addAttributes(fontAttribute, range: range)
        return attribute
    }
    func checkInternet() -> Bool {
        //        if !(self.hasInternetConnectivity()) {
        //            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: Constant.AlertNetwork)
        //            return false
        //        }
        return true
    }
    
    //    private func hasInternetConnectivity() -> Bool {
    //
    //        let reachability: Reachability = Reachability.init()!
    //        let networkStatus: Int = reachability.currentReachabilityStatus.hashValue
    //        return networkStatus != 0
    //
    //    }
    
    func showAlertMessage(titleStr:String, messageStr:String) {
        AppHelper.alert(titleStr, message: messageStr, cancelButtonTitle: isArabicActive ? "حسنا" : "OK", otherButtonTitles: nil)
    }
}
extension UIViewController {
    func showImageSelectionAlert(picker : UIImagePickerController){
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            print("Camera")
            self.getImageByCamera(picker: picker)
        }
        let galleryAction = UIAlertAction(title: "Album", style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            print("Album")
            self.getImageByPhotoStorage(picker: picker)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("No")
        }
        alertController.addAction(cameraAction)
        alertController.addAction(galleryAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
extension UIViewController {
    //MARK: - ALERT Controller and ImagePickerController Delegate
    func getImageByCamera(picker : UIImagePickerController){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            present(picker,animated: true,completion: nil)
        }
        else{
            self.showAlertMessage(titleStr: "No camera", messageStr: "This mobile device camera is not available")
        }
    }
    func getImageByPhotoStorage(picker : UIImagePickerController){
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        present(picker, animated: true, completion: nil)
    }
    
    func checkViewControllerExist(controllers : [UIViewController],searchedController : UIViewController) -> Bool{
        for object in controllers{
            if object == searchedController{
                return true
            }
        }
        return false
    }
    
    func validateDateAccordingly (pickedDate : Date, comparedDate: Date, caparedByType : dateComparedBy ) -> Bool{
        switch  caparedByType {
        case .equal:
            return pickedDate.compare(comparedDate) == .orderedSame
        case .greaterThan :
            return pickedDate.compare(comparedDate) == .orderedDescending
        case .lessThan :
            return pickedDate.compare(comparedDate) == .orderedAscending
        }
    }
    
    func getDateFromString (dateStr : String) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let convertedDate = formatter.date(from: dateStr)
        return convertedDate ?? Date()
    }
    func convertHexStringToColor(stringColor: String) -> UIColor {
        var hexInt: UInt32 = 0
        let scanner = Scanner(string: stringColor)
        scanner.scanHexInt32(&hexInt)
        let color = UIColor(
            red: CGFloat((hexInt & 0xFF0000) >> 16)/255,
            green: CGFloat((hexInt & 0xFF00) >> 8)/255,
            blue: CGFloat((hexInt & 0xFF))/255,
            alpha: 1)
        
        return color
    }
    
    
    func isValidEmail(_ testStr:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
        
    }
    func isValidAlphaNumeric(_ testStr:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z]"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
        
    }
    func isValidAlphaNumericWithEmail(_ testStr:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z@._-]"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
        
    }
    func isValidPanCard(_ testStr:String) -> Bool {
        
        let panRegEx = "[A-Z]{3}P[A-Z]{1}[0-9]{4}[A-Z]{1}"
        let panTest = NSPredicate(format:"SELF MATCHES %@", panRegEx)
        let result = panTest.evaluate(with: testStr)
        return result
    }
    func isValidIFSCCode(_ testStr:String) -> Bool {
        
        let panRegEx = "^[A-Za-z]{4}[a-zA-Z0-9]{7}$"
        let panTest = NSPredicate(format:"SELF MATCHES %@", panRegEx)
        let result = panTest.evaluate(with: testStr)
        return result
    }
    func isValidAlphabet(_ testStr:String) -> Bool {
        
        let emailRegEx = "[A-Za-z ]"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
        
    }
    func isValidPassword(_ testStr:String) -> Bool {
        
        let passwordRegEx = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        let result = passwordTest.evaluate(with: testStr)
        return result
        
    }
    func isValidUrl(urlString: String) -> Bool {
        let urlRegEx = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
        return NSPredicate(format: "SELF MATCHES %@", urlRegEx).evaluate(with: urlString)
    }
}

extension UIViewController {
    public func pushSideViewController(sideMenu: UIViewController, centerView: UIViewController, isLeft: Bool) {
        if isLeft {
            let sideMenuViewController: AKSideMenu = AKSideMenu(contentViewController: centerView, leftMenuViewController: sideMenu, rightMenuViewController: nil)
            sideMenuViewController.interactivePopGestureRecognizerEnabled = false
            sideMenuViewController.panFromEdge = false
            sideMenuViewController.bouncesHorizontally = false
            self.navigationController?.pushViewController(sideMenuViewController, animated: true)
        } else {
            
            let sideMenuViewController: AKSideMenu = AKSideMenu(contentViewController: centerView, leftMenuViewController: nil, rightMenuViewController: sideMenu)
            sideMenuViewController.interactivePopGestureRecognizerEnabled = false
            sideMenuViewController.panFromEdge = false
            sideMenuViewController.bouncesHorizontally = false
            self.navigationController?.pushViewController(sideMenuViewController, animated: true)
        }
    }
}

extension UIViewController {
    func showActivityIndicator(uiView: UIView) {
        
        let backgroundview = UIView.init(frame: CGRect.init(x: 0, y: 0, width:  view.frame.size.width, height:  view.frame.size.height))
        backgroundview.tag=1024;
        backgroundview.backgroundColor = UIColor.init(red: 255.0 / 255.0, green: 255.0 / 255.0, blue: 255.0 / 255.0, alpha: 0.4)
        
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        actInd.center = uiView.center
        actInd.style = UIActivityIndicatorView.Style.whiteLarge
        actInd.color = UIColor.purpleSelected
        actInd.startAnimating()
        backgroundview.addSubview(actInd)
        view.addSubview(backgroundview)
    }
    func hideActivityIndicator(uiView: UIView) {
        view.viewWithTag(1024)?.removeFromSuperview()
    }
}
