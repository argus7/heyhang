//
//  Extention_PageControl.swift
//  SaudiCalendar
//
//  Created by TechGropse on 9/24/18.
//  Copyright © 2018 TechGropse Pvt Limited. All rights reserved.
//

import Foundation
import UIKit
extension UIPageControl {
    func setDot () {
        self.currentPageIndicatorTintColor =  UIColor(patternImage: UIImage(named: "pageactive.png")!)
        self.pageIndicatorTintColor =  UIColor(patternImage: UIImage(named: "pageinactive.png")!)
//        self.currentPageIndicatorTintColor = UIColor.bluishNew
//        self.pageIndicatorTintColor =  UIColor.lightGray
        self.numberOfPages =  3
    }
}

class MAPageControl: UIPageControl {
    
    //Override Methods
    override var numberOfPages: Int {
        didSet {
            updateDots()
        }
    }
    
    override var currentPage: Int {
        didSet {
            updateDots()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.pageIndicatorTintColor = UIColor.clear
        self.currentPageIndicatorTintColor = UIColor.clear
        self.clipsToBounds = false
    }
    
    //Helping Methods
    var activeDot: UIImage {
        return UIImage (named: "pageactive.png")! // Image you want to replace with dots
    }
    
    var unActiveDot: UIImage {
        return UIImage (named: "pageinactive.png")!//Default Image
    }
    
    func updateDots() {
        var i = 0
        for view in self.subviews {
            var imageView = self.imageView(forSubview: view)
            imageView?.contentMode = .scaleAspectFit
            if imageView == nil {
                if i == 0 {
                    imageView = UIImageView(image: activeDot)
                } else {
                    imageView = UIImageView(image: unActiveDot)
                }
                imageView?.frame = getUpdatedFrame(rect: imageView!.frame)
                imageView!.center = view.center
                imageView?.contentMode = .scaleAspectFit
                view.addSubview(imageView!)
                view.clipsToBounds = false
            }
            if i == self.currentPage {
                imageView!.alpha = 1.0
                imageView?.image = activeDot
            } else {
                imageView!.alpha = 0.5
                imageView?.image = unActiveDot
            }
            i += 1
        }
    }
    
    fileprivate func imageView(forSubview view: UIView) -> UIImageView? {
        var dot: UIImageView?
        if let dotImageView = view as? UIImageView {
            dot = dotImageView
        } else {
            for foundView in view.subviews {
                if let imageView = foundView as? UIImageView {
                    dot = imageView
                    break
                }
            }
        }
        return dot
    }
    private func getUpdatedFrame(rect: CGRect) -> CGRect {
        var frame = rect
        frame.size.width = 17.0
        frame.size.height = 17.0
        return frame
    }
}
