//
//  Extention_UIColor.swift
//  SaudiCalendar
//
//  Created by TechGropse on 9/25/18.
//  Copyright © 2018 TechGropse Pvt Limited. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    //Updated From new design
    open class var blueSelected: UIColor { get {
        return UIColor(red: (62.0/255.0), green:  (73.0/255.0), blue:  (236.0/255.0), alpha: 1)
        }
    }
    open class var grayDeSelected: UIColor { get {
        return UIColor(red: (108/255.0), green:  (111/255.0), blue:  (121/255.0), alpha: 1)
        }
    }
    open class var purpleSelected: UIColor { get {
        return UIColor(red: (30.0/255.0), green:  (11.0/255.0), blue:  (71.0/255.0), alpha: 1)
        }
    }
    open class var shineGray: UIColor { get {
        return UIColor(red: 210/255.0, green: 217/255.0, blue: 229/255.0, alpha: 0.5)
        }
    }
    open class var backgroundGray: UIColor { get {
        return UIColor(red: 236/255.0, green: 240/255.0, blue: 245/255.0, alpha: 0.5)
        }
    }
    open class var eventTypePurple: UIColor { get {
        return UIColor(red: 104/255.0, green: 5/255.0, blue: 232/255.0, alpha: 1.0)
        }
    }
    open class var eventTypeYellow: UIColor { get {
        return UIColor(red: 232/255.0, green: 162/255.0, blue: 0/255.0, alpha: 1.0)
        }
    }
    open class var greenJoin: UIColor { get {
        return UIColor(red: 0/255.0, green: 212/255.0, blue: 99/255.0, alpha: 1.0)
        }
    }
    open class var redLeave: UIColor { get {
        return UIColor(red: 249/255.0, green: 60/255.0, blue: 100/255.0, alpha: 1.0)
        }
    }
    public convenience init?(hexString: String) {
        
        if hexString.hasPrefix("#") {
            let start = hexString.index(hexString.startIndex, offsetBy: 1)
            let hexColor = String(hexString[start...])
            
            let scanner = Scanner(string: hexColor)
            scanner.scanLocation = 0
            
            var rgbValue: UInt64 = 0
            
            scanner.scanHexInt64(&rgbValue)
            
            let r = (rgbValue & 0xff0000) >> 16
            let g = (rgbValue & 0xff00) >> 8
            let b = rgbValue & 0xff
            
            self.init(
                red: CGFloat(r) / 0xff,
                green: CGFloat(g) / 0xff,
                blue: CGFloat(b) / 0xff, alpha: 1
            )
            return
        }
        
        return nil
    }
}
