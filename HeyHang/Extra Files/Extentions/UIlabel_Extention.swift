//
//  UIlabel_Extention.swift
//  SaudiCalendar
//
//  Created by TechGropse on 10/26/18.
//  Copyright © 2018 TechGropse Pvt Limited. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    func textWidth() -> CGFloat {
        return UILabel.textWidth(label: self)
    }
    class func textWidth(label: UILabel) -> CGFloat {
        return textWidth(label: label, text: label.text!)
    }
    class func textWidth(label: UILabel, text: String) -> CGFloat {
        return textWidth(font: label.font, text: text)
    }
    class func textWidth(font: UIFont, text: String) -> CGFloat {
        let myText = text as NSString
        let rect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude)
        let labelSize = myText.boundingRect(with: rect, options: .usesLineFragmentOrigin, attributes: [.font : font], context: nil)
        return ceil(labelSize.width)
    }
}
extension UILabel {
    @IBInspectable
    var rotation: Int {
        get {
            return 0
        } set {
            let radians = CGFloat(CGFloat(Double.pi) * CGFloat(newValue) / CGFloat(180.0))
            self.transform = CGAffineTransform(rotationAngle: radians)
        }
    }

    func underline(newText: String) {
        if let textString = self.text {
            let myString : NSString = textString as NSString
            let changeText = newText
            
            let textColor: UIColor = UIColor.black
            let underLineColor: UIColor = UIColor.black
            let underLineStyle = NSUnderlineStyle.single.rawValue
            
            let range = (myString).range(of: changeText)
            let attribute = NSMutableAttributedString(string: myString as String)
            let labelAtributes:[NSAttributedString.Key : Any]  = [
                NSAttributedString.Key.foregroundColor: textColor,
                NSAttributedString.Key.underlineStyle: underLineStyle,
                NSAttributedString.Key.underlineColor: underLineColor
            ]
            
            attribute.addAttributes(labelAtributes, range: range)
            self.attributedText = attribute
            
        }
    }
}
