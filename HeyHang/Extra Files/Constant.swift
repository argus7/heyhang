//
//  Constant.swift
//  HeyHang
//
//  Created by Anup Kumar on 5/1/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import Foundation
import UIKit

struct Constant {
    
    static let kCornerRadius: CGFloat = 4.0
    static let APP_NAME: String = "HeyHang"
    static let Currency: String = "$"
    static let CalendarName: String = "Heyhang"
    static let AlertNetwork: String = "No Internet Available"
    static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    struct userDefaults {
        
        static let kLanguageSelected: String = "kLanguageSelected"
        static let kIsRegistered: String = "kIsRegistered"
        static let kLoginType: String = "kLoginType"
        static let kLatitude: String = "kLatitude"
        static let kLongitude: String = "kLongitude"
        static let kLocAddress: String = "kLocAddress"
        static let kDeviceID: String = "kDeviceID"
        static let kDeviceToken: String = "kDeviceToken"
        static let kUserSecuirityToken: String = "kUserSecuirityToken"
        
        static let kUserId: String = "userId"
        static let kUserName: String = "kUserName"
        static let kUserEmail: String = "kUserEmail"
        static let kUserMobileCountryCode: String = "kUserMobileCountryCode"
        static let kUserMobile: String = "kUserMobile"
        static let kUserCivilId: String = "kUserCivilId"
        static let kUserImage: String = "kUserImage"
        static let kSocialId: String = "kSocialId"
        
        static let kNotiPostNoti: String = "kNotiPostNoti"
        static let kNotiInviteNoti: String = "kNotiInviteNoti"
        static let kNotiStartNoti: String = "kNotiStartNoti"
        static let kNotiFriendNoti: String = "kNotiFriendNoti"
        
    }
}
