//
//  RestAPI.swift
//  HeyHang
//
//  Created by Mohd Arsad on 18/05/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import Foundation
import Alamofire

struct APIConstant {
    
    static let MainUrl = "https://auctionbuy.in/heyhang/user/"
    
    static let signUpMobile = "mobileSignup"
    static let signUpEmail = "emailSignup"
    static let verifyOTP = "verifyOtp"
    static let resendOTP = "createOtp"
    static let login = "login"
    static let socialLogin = "socialLogin"
    static let myProfile = "myProfile"
    static let editProfile = "editProfile"
    static let uploadImage = "uploadImage"
    static let uploadImageId = "uploadImageId"
    static let resetPassword = "setNewPassword"
    static let changePassword = "changePassword"
    static let forgotPassword = "forgotPassword"
    static let contactUs = "contactUs"
    static let logout = "logout"
    static let updateNotificationSetitngs = "notificationSetting"
    
    static let homeEventList = "homescreen"
    static let homeFilterEventList = "filterEvents"
    static let createEvent = "createEvent"
    static let addDetailCreateEvent = "addEventDetails"
    static let addEventImageCreateEvent = "addEventImages"
    static let userList = "getUserList"
    static let deleteAccount = "deleteUser"
    
    static let myEventList = "myEvents"
    static let eventDetail = "eventDetail"
    static let editEvent = "editEvent"
    static let cancelEvent = "cancelEvent"
    static let bookedEvent = "bookedEvents"
    
    static let myGroupList = "myGroups"
    static let createGroup = "createGroup"
    static let groupInformation = "groupInfo"
    static let deleteGroup = "deleteGroup"
    static let removeGroupMember = "removeGroupMember"
    static let addGroupMember = "addGroupMember"
    static let chatList = "getMessageList"
    static let groupMessageList = "getMessageDetailGroup"
    static let userMessageList = "getMessageDetail"
    static let sendGroupMessage = "sendMessageGroup"
    static let sendUserMessage = "sendMessage"
    static let reportUserGroup = "reportUser"
    static let unfriendUser = "removeFriend"
    static let addFriend = "addFriend"
    static let deleteChat = "deleteChat"
    static let deleteGroupChat = "deleteChatGroup"
    
    static let cancelAcceptRequest = "friendResponse"
    
    static let allRequest = "allRequest"
    static let bookTicket = "bookNow"
    static let leaveEventTicket = "leaveEvent"
    static let myTicketList = "myTicket"
    static let feedback = "feedback"
    
    //Webview Supported Url(use with main Url)
    static let privacyPolicyUrl = "privacyPolicy"
    static let termsConditionUrl = "termsCondition"
    static let aboutUsUrl = "aboutUs"
    
    //Synching API
    struct Sync {
        static let contact = "syncContacts"
        static let calendar = "syncCalendar"
    }
    
}//DeviceId
//SecurityToken

open class RestAPI {
    
    static let shared = RestAPI()
    
    private var headerData : [String : String] {
        get {
            let deviceId = UserDefaults.standard.string(forKey: Constant.userDefaults.kDeviceID) ?? ""
            let tokenStr = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserSecuirityToken) ?? ""
            return ["Securitytoken" : tokenStr, "Deviceid" : deviceId]
        }
    }
    private func saveUserDetailsIntoCache(details: [String: Any]) {
        
        UserDefaults.standard.setValue("\(details["id"]!)", forKey: Constant.userDefaults.kUserId)
        UserDefaults.standard.setValue("\(details["login_type"]!)", forKey: Constant.userDefaults.kLoginType)
        UserDefaults.standard.setValue(details["full_name"] as? String ?? "", forKey: Constant.userDefaults.kUserName)
        UserDefaults.standard.setValue(details["email"] as? String ?? "", forKey: Constant.userDefaults.kUserEmail)
        UserDefaults.standard.setValue(details["mobile"] as? String ?? "", forKey: Constant.userDefaults.kUserMobile)
        UserDefaults.standard.setValue(details["profile_image"] as? String ?? "", forKey: Constant.userDefaults.kUserImage)
        UserDefaults.standard.setValue(details["device_id"] as? String ?? "", forKey: Constant.userDefaults.kDeviceID)
        UserDefaults.standard.setValue(details["security_token"] as? String ?? "", forKey: Constant.userDefaults.kUserSecuirityToken)
        UserDefaults.standard.setValue(details["social_id"] as? String ?? "", forKey: Constant.userDefaults.kSocialId)
        
    }
    private func updateProfileDetailsIntoCache(details: [String: Any]) {
        let name = details["full_name"] as? String ?? ""
        let email = details["email"] as? String ?? ""
        let mobile = details["mobile"] as? String ?? ""
        let image = details["profile_image"] as? String ?? ""
        
        UserDefaults.standard.setValue(name, forKey: Constant.userDefaults.kUserName)
        UserDefaults.standard.setValue(email, forKey: Constant.userDefaults.kUserEmail)
        UserDefaults.standard.setValue(mobile, forKey: Constant.userDefaults.kUserMobile)
        UserDefaults.standard.setValue(image, forKey: Constant.userDefaults.kUserImage)
    }
}

// MARK: - Registration Section
extension RestAPI {
    
    /// SignUp API
    ///
    /// - Parameters:
    ///   - isForMobile: Methods works for both email and mobile type
    ///   - params: related paramaters to do the signup
    ///   - completion: get success result with generated OTP
    func signUp(isForMobile: Bool,params: [String : Any], completion: @escaping(String?, String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + (isForMobile ? APIConstant.signUpMobile : APIConstant.signUpEmail)
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(nil, "API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(nil,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            if let serverData = jsonResponse["data"] as? [String: Any] {
                if let userId = serverData["user_id"] as? Int {
                    completion("\(userId)", jsonResponse["message"] as? String ?? "OTP Sent Successfully", statusCode)
                } else {
                    completion(nil, "Data not found", statusCode)
                }
            } else {
                completion(nil,jsonResponse["message"] as? String ??  "Data Not found", statusCode)
            }
        }
    }
    
    /// Login API
    ///
    /// - Parameters:
    ///   - params: required params for do login
    ///   - completion: get user data in user model or return some error
    func login(params: [String : Any], completion: @escaping(String?, String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.login
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(nil, "API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                if let serverData = jsonResponse["data"] as? [String: Any] {
                    if let userId = serverData["user_id"] as? String {
                        completion(userId, jsonResponse["message"] as? String ?? "OTP Sent Successfully", statusCode)
                    } else {
                        completion(nil, jsonResponse["message"] as? String ?? "Data not found", statusCode)
                    }
                } else {
                    completion(nil,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                }
                return
            }
            if let serverData = jsonResponse["data"] as? [String: Any] {
                UserDefaults.standard.setValue("Yes", forKey: Constant.userDefaults.kIsRegistered)
                self.saveUserDetailsIntoCache(details: serverData)
                completion("Success", nil, statusCode)
            } else {
                completion(nil,jsonResponse["message"] as? String ??  "Data Not found", statusCode)
            }
        }
    }
    
    /// Social Login API
    ///
    /// - Parameters:
    ///   - params: required params for do login
    ///   - completion: get user data in user model or return some error
    func socialLogin(params: [String : Any], completion: @escaping(String?, String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.socialLogin
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(nil, "API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                if let serverData = jsonResponse["data"] as? [String: Any] {
                    if let userId = serverData["user_id"] as? String {
                        completion(userId, jsonResponse["message"] as? String ?? "OTP Sent Successfully", statusCode)
                    } else {
                        completion(nil, jsonResponse["message"] as? String ?? "Data not found", statusCode)
                    }
                } else {
                    completion(nil,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                }
                return
            }
            if let serverData = jsonResponse["data"] as? [String: Any] {
                UserDefaults.standard.setValue("Yes", forKey: Constant.userDefaults.kIsRegistered)
                self.saveUserDetailsIntoCache(details: serverData)
                completion("Success", nil, statusCode)
            } else {
                completion(nil,jsonResponse["message"] as? String ??  "Data Not found", statusCode)
            }
        }
    }
    
    /// VerifyOTP API
    ///
    /// - Parameters:
    ///   - params: related paramaters to verify the signup/forgot otp
    ///   - completion: get success/error result for the verification
    func verifyOTP(params: [String : Any], completion: @escaping(String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.verifyOTP
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion("API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            if let serverData = jsonResponse["data"] as? [String: Any] {
                self.saveUserDetailsIntoCache(details: serverData)
            }
            completion(jsonResponse["message"] as? String ??  "", statusCode)
        }
    }
    
    /// ResendOTP API
    ///
    /// - Parameters:
    ///   - params: related paramaters to verify the signup/forgot otp
    ///   - completion: get success/error result for the verification
    func resendOTP(params: [String : Any], completion: @escaping(String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.resendOTP
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion("API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            completion(jsonResponse["message"] as? String ??  "", statusCode)
        }
    }
    
    /// Forgot Password API
    ///
    /// - Parameters:
    ///   - params: related paramaters to send the request for Forgot password
    ///   - completion: get success/error result and existed user id for reset password
    func forgotPassword(params: [String : Any], completion: @escaping(String?,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.forgotPassword
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(nil,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(nil,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            if let serverData = jsonResponse["data"] as? [String: Any] {
                if let userId = serverData["user_id"] as? String {
                    completion("\(userId)", jsonResponse["message"] as? String ?? "", statusCode)
                } else {
                    completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
                }
            } else {
                completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
            }
        }
    }
    
    /// Reset Password API
    ///
    /// - Parameters:
    ///   - params: related paramaters to verify the signup/forgot otp
    ///   - completion: get success/error result for the verification
    func resetPassword(params: [String : Any], completion: @escaping(String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.resetPassword
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion("API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            completion(jsonResponse["message"] as? String ??  "", statusCode)
        }
    }
    
    /// Change Password API
    ///
    /// - Parameters:
    ///   - params: related paramaters to verify the signup/forgot otp
    ///   - completion: get success/error result for the verification
    func changePassword(params: [String : Any], completion: @escaping(String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.changePassword
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion("API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            completion(jsonResponse["message"] as? String ??  "", statusCode)
        }
    }
}

// MARK: - Profile Section
extension RestAPI {
    
    /// GetProfile Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of profile data
    func getProfile(completion: @escaping(ProfileViewModel?,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.myProfile
        let headerData = RestAPI.shared.headerData
        
        let params: [String: Any] = ["user_id" : (UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0")]
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(nil,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(nil,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            if let serverData = jsonResponse["data"] as? [String: Any] {
                self.updateProfileDetailsIntoCache(details: serverData)
                completion(ProfileViewModel(json: serverData),jsonResponse["message"] as? String ?? "", statusCode)
            } else {
                completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
            }
        }
    }
    
    /// UpdateProfile Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result for update profile details
    func updateProfile(params: [String: Any], completion: @escaping(ProfileViewModel?,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.editProfile
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(nil,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(nil,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            if let serverData = jsonResponse["data"] as? [String: Any] {
                completion(ProfileViewModel(json: serverData),jsonResponse["message"] as? String ?? "", statusCode)
            } else {
                completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
            }
        }
    }
    
    /// UPload Image on Server API
    ///
    /// - Parameters:
    ///   - completion: get success/error result for update profile details
    func uploadImage(imgData: Data, completion: @escaping(String?,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.uploadImage
        let headerData = RestAPI.shared.headerData
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            // import image to request
            multipartFormData.append(imgData, withName: "image", fileName: "fileImage.jpeg", mimeType: "image/jpeg")
        }, to: urlStr,headers : headerData,
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: Any]
                    print("Json Object: \(jsonResponse)")
                    let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
                    guard statusCode == 100 else {
                        completion(nil,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                        return
                    }
                    if let imageObject = jsonResponse["data"] as? [String: Any] {
                        if let imageUrl = imageObject["image"] as? String {
                            completion(imageUrl,jsonResponse["message"] as? String ?? "", statusCode)
                        } else {
                            completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
                        }
                    } else {
                        completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
                    }
                }
            case .failure( _):
                completion(nil,"API Failed", 0)
            }
        })
    }
    
    /// UpdateProfile Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result for update profile details
    func updateNotificationSettings(params: [String: Any], completion: @escaping([String: Any]?,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.updateNotificationSetitngs
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(nil,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(nil,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            if let serverData = jsonResponse["data"] as? [String: Any] {
                completion(serverData,jsonResponse["message"] as? String ?? "", statusCode)
            } else {
                completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
            }
        }
    }
    
    /// Contact Us Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result for Contact Us details
    func contactUs(params: [String: Any], completion: @escaping(String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.contactUs
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion("API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            completion(jsonResponse["message"] as? String ?? "", statusCode)
        }
    }
}

// MARK: - Event Section
extension RestAPI {
    
    /// Get Event List for Home Screen Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of profile data
    func getHomeEventList(params: [String: Any], completion: @escaping([Event]?,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.homeEventList
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(nil,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(nil,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            if let serverData = jsonResponse["data"] as? [[String: Any]] {
                let tempArr = serverData.map({ return Event(json: $0) })
                completion(tempArr,jsonResponse["message"] as? String ?? "", statusCode)
            } else {
                completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
            }
        }
    }
    /// Get Filter Event List for Home Screen Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of profile data
    func getHomeFilterEventList(params: [String: Any], completion: @escaping([Event]?,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.homeFilterEventList
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(nil,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(nil,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            if let serverData = jsonResponse["data"] as? [[String: Any]] {
                let tempArr = serverData.map({ return Event(json: $0) })
                completion(tempArr,jsonResponse["message"] as? String ?? "", statusCode)
            } else {
                completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
            }
        }
    }
    
    /// Create Event Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of profile data
    func createEvent(params: [String: Any], completion: @escaping(String?,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.createEvent
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(nil,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(nil,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            if let serverData = jsonResponse["data"] as? [String: Any] {
                completion("\(serverData["event_id"]!)",jsonResponse["message"] as? String ?? "", statusCode)
            } else {
                completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
            }
        }
    }
    
    /// Create Event Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of profile data
    func deleteAccount(params: [String: Any], completion: @escaping(String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.deleteAccount
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion("API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            completion(jsonResponse["message"] as? String ?? "", statusCode)
        }
    }
    
    /// Add Details in Create Event Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of Adding Detail of creating event data
    func addDetailCreateEvent(params: [String: Any], completion: @escaping([SImage]?,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.addDetailCreateEvent
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(nil,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(nil,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            if let serverData = jsonResponse["data"] as? [String: Any] {
                if let images = serverData["suggested_images"] as? [[String: Any]] {
                    let imgArr = images.map({ return SImage(jsonData: $0) })
                    completion(imgArr,jsonResponse["message"] as? String ?? "", statusCode)
                } else {
                    completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
                }
            } else {
                completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
            }
        }
    }
    
    /// Add Gallery in Create Event Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of Adding GalleryDetails of creating event data
    func addGalleryDetailCreateEvent(params: [String: Any], completion: @escaping(Bool,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.addEventImageCreateEvent
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(false,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(false,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            completion(true,jsonResponse["message"] as? String ?? "", statusCode)
        }
    }
    
    /// Update Details in Update Event Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of Updating Event Details of update event data
    func updateEventDetailUpdateEvent(params: [String: Any], completion: @escaping(Bool,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.editEvent
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(false,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(false,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            completion(true,jsonResponse["message"] as? String ?? "", statusCode)
        }
    }
    
    /// UPload Image on Server API
    ///
    /// - Parameters:
    ///   - completion: get success/error result for update profile details
    func uploadImageId(imgData: Data, completion: @escaping([String : String]?,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.uploadImageId
        let headerData = RestAPI.shared.headerData
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            // import image to request
            multipartFormData.append(imgData, withName: "image", fileName: "fileImage.jpeg", mimeType: "image/jpeg")
        }, to: urlStr,headers : headerData,
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: Any]
                    print("Json Object: \(jsonResponse)")
                    let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
                    guard statusCode == 100 else {
                        completion(nil,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                        return
                    }
                    if let imageObject = jsonResponse["data"] as? [String: Any] {
                        let imageUrl = imageObject["image"] as? String ?? ""
                        let imageId = "\(imageObject["id"] as? Int ?? 0)"
                        completion([imageId: imageUrl],jsonResponse["message"] as? String ?? "", statusCode)
                    } else {
                        completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
                    }
                }
            case .failure( _):
                completion(nil,"API Failed", 0)
            }
        })
    }
    
    /// Get User List Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of User Invite List data
    func userList(params: [String: Any], completion: @escaping([User]?,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.userList
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(nil,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(nil,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            if let serverData = jsonResponse["data"] as? [[String: Any]] {
                let userArr = serverData.map({ return User(jsonData: $0) })
                completion(userArr,jsonResponse["message"] as? String ?? "", statusCode)
            } else {
                completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
            }
        }
    }
    
    /// Get My Event List Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of my event data
    func getMyEventList(params: [String: Any], completion: @escaping([Event]?,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.myEventList
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(nil,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(nil,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            if let serverData = jsonResponse["data"] as? [[String: Any]] {
                let tempArr = serverData.map({ return Event(json: $0) })
                completion(tempArr,jsonResponse["message"] as? String ?? "", statusCode)
            } else {
                completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
            }
        }
    }
    
    /// Get Booked/Upcoming Event List Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of Booked/Upcoming event data
    func getBookedEventList(completion: @escaping([String: Any]?,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.bookedEvent
        let headerData = RestAPI.shared.headerData
        
        let param: [String: Any] = ["user_id": UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"]
        
        Alamofire.request(urlStr, method: .post, parameters: param, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(nil,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(nil,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            if let serverData = jsonResponse["data"] as? [String: Any] {
                completion(serverData,jsonResponse["message"] as? String ?? "", statusCode)
            } else {
                completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
            }
        }
    }
    /// Get Event Detail Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of my event data
    func getEventDetails(params: [String: Any], completion: @escaping(Event?,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.eventDetail
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(nil,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(nil,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            if let serverData = jsonResponse["data"] as? [String: Any] {
                let tempObj = Event(json: serverData)
                completion(tempObj,jsonResponse["message"] as? String ?? "", statusCode)
            } else {
                completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
            }
        }
    }
    
    /// Cancel Event Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of Cancel Event
    func cancelEvent(params: [String: Any], completion: @escaping(Bool,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.cancelEvent
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(false,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(false,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            completion(true,jsonResponse["message"] as? String ?? "", statusCode)
        }
    }
}
// MARK: - Group Section
extension RestAPI {
    /// Get My Group List Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of my group list data
    func getMyGroupList(completion: @escaping([Group]?,String?, Int) -> ()) {
        
        let params = ["user_id" : UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"]
        
        let urlStr = APIConstant.MainUrl + APIConstant.myGroupList
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(nil,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(nil,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            if let serverData = jsonResponse["data"] as? [[String: Any]] {
                let tempArr = serverData.map({ return Group(json: $0) })
                completion(tempArr,jsonResponse["message"] as? String ?? "", statusCode)
            } else {
                completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
            }
        }
    }
    
    
    /// Group Information Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of group info data
    func groupInformation(groupId: String, completion: @escaping([String: Any
        ]?,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.groupInformation
        let headerData = RestAPI.shared.headerData
        
        let params = ["group_id" : groupId]
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(nil,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(nil,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            if let serverData = jsonResponse["data"] as? [String: Any] {
                completion(serverData,jsonResponse["message"] as? String ?? "", statusCode)
            } else {
                completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
            }
        }
    }
    
    /// Group Leave Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of group leave data
    func groupLeave(params: [String: Any], completion: @escaping(Bool,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.removeGroupMember
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(false,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(false,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            completion(true,jsonResponse["message"] as? String ?? "", statusCode)
        }
    }
    
    /// Remove Group Members Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of remove group members data
    func removeGroupMembers(params: [String: Any], completion: @escaping(Bool,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.removeGroupMember
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(false,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(false,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            completion(true,jsonResponse["message"] as? String ?? "", statusCode)
        }
    }
    
    /// Add Group Members Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of add group members data
    func addGroupMembers(params: [String: Any], completion: @escaping(Bool,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.addGroupMember
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(false,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(false,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            completion(true,jsonResponse["message"] as? String ?? "", statusCode)
        }
    }
    
    /// Delete Group Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of group delete data
    func groupDelete(groupId: String, completion: @escaping(Bool,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.deleteGroup
        let headerData = RestAPI.shared.headerData
        
        let params = ["group_id" : groupId]
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(false,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(false,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            completion(true,jsonResponse["message"] as? String ?? "", statusCode)
        }
    }
    
    /// Create Group Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of create group data
    func createGroup(params: [String: Any], completion: @escaping(Bool,String?, Int) -> ()) {
       
        let urlStr = APIConstant.MainUrl + APIConstant.createGroup
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(false,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(false,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            completion(true,jsonResponse["message"] as? String ?? "", statusCode)
        }
    }
}

// MARK: - Ticket Section
extension RestAPI {
    
    /// Book Ticket Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of create group data
    func bookTicket(params: [String: Any], completion: @escaping(Bool,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.bookTicket
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(false,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard (statusCode == 100 || statusCode == 104) else {
                completion(false,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            completion(true,jsonResponse["message"] as? String ?? "", statusCode)
        }
    }
    
    /// Leave Event Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of create group data
    func leaveEventTicket(params: [String: Any], completion: @escaping(Bool,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.leaveEventTicket
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(false,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(false,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            completion(true,jsonResponse["message"] as? String ?? "", statusCode)
        }
    }
    
    /// Get Booked/Upcoming Event List Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of Booked/Upcoming event data
    func ticketList(completion: @escaping([Ticket]?,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.myTicketList
        let headerData = RestAPI.shared.headerData
        
        let param: [String: Any] = ["user_id": UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"]
        
        Alamofire.request(urlStr, method: .post, parameters: param, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(nil,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(nil,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            if let serverData = jsonResponse["data"] as? [[String: Any]] {
                let arr = serverData.map({ return Ticket(json: $0) })
                completion(arr,jsonResponse["message"] as? String ?? "", statusCode)
            } else {
                completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
            }
        }
    }
    
    /// Feedback Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of Feedback data
    func feedback(params: [String: Any], completion: @escaping(Bool,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.feedback
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(false,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(false,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            completion(true,jsonResponse["message"] as? String ?? "", statusCode)
        }
    }
    
}

// MARK: - Background Synching
extension RestAPI {
    
    /// Contact Synching API
    func syncContact(params: [String: Any]) {
        
        DispatchQueue.global(qos: .background).async {
            let urlStr = APIConstant.MainUrl + APIConstant.Sync.contact
            let headerData = RestAPI.shared.headerData
            
            Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
                guard response.result.isSuccess else {
                    print("Error while fetching remote data: \(String(describing: response.result.error))")
                    return
                }
                let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
                print(jsonResponse)
                let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
                guard statusCode == 100 else {
                    print(jsonResponse["message"] as? String ?? "Network Error")
                    return
                }
                print(jsonResponse["message"] as? String ?? "Success")
            }
        }
    }
    
    /// Calendar Synching API
    func syncCalendar(params: [String: Any]) {
        
        DispatchQueue.global(qos: .background).async {
            let urlStr = APIConstant.MainUrl + APIConstant.Sync.calendar
            let headerData = RestAPI.shared.headerData
            
            Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
                guard response.result.isSuccess else {
                    
                    print("Error while fetching remote data: \(String(data: response.data!, encoding: String.Encoding.utf8) ?? ""))")
                    return
                }
                let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
                print(jsonResponse)
                let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
                guard statusCode == 100 else {
                    print(jsonResponse["message"] as? String ?? "Network Error")
                    return
                }
                print(jsonResponse["message"] as? String ?? "Success")
            }
        }
    }
}

// MARK: - Chat APIs
extension RestAPI {
    /// Get My Group List Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of my group list data
    func getMyChatList(completion: @escaping([String: Any]?,String?, Int) -> ()) {
        
        let params = ["user_id" : UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"]
        
        let urlStr = APIConstant.MainUrl + APIConstant.chatList
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(nil,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(nil,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            if let serverData = jsonResponse["data"] as? [String: Any] {
                completion(serverData,jsonResponse["message"] as? String ?? "", statusCode)
            } else {
                completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
            }
        }
    }
    
    /// Get My Message List Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of my message list data
    func getMyChatMessageList(isForGroup:Bool, params: [String: Any], completion: @escaping([[String: Any]]?,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + (isForGroup ? APIConstant.groupMessageList : APIConstant.userMessageList)
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(nil,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(nil,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            if let serverData = jsonResponse["data"] as? [[String: Any]] {
                completion(serverData,jsonResponse["message"] as? String ?? "", statusCode)
            } else {
                completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
            }
        }
    }
    
    /// Send User Message Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of my message list data
    func sendChatMessage(isForGroup:Bool, params: [String: Any], completion: @escaping(Bool,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + (isForGroup ? APIConstant.sendGroupMessage : APIConstant.sendUserMessage)
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                if isForGroup {
                    completion(true,"Success", 100)
                } else {
                    completion(false,"API Failed", 0)
                }
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(false,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            completion(true,jsonResponse["message"] as? String ?? "", statusCode)
        }
    }
    
    /// Report User Group Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of report data
    func reportUserGroup(params: [String: Any], completion: @escaping(Bool,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.reportUserGroup
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(false,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(false,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            completion(true,jsonResponse["message"] as? String ?? "", statusCode)
        }
    }
    
    /// Delete Chat User Group Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of report data
    func deleteChatGroup(isGroup: Bool, params: [String: Any], completion: @escaping(Bool,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + (isGroup ? APIConstant.deleteGroupChat : APIConstant.deleteChat)
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(false,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(false,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            completion(true,jsonResponse["message"] as? String ?? "", statusCode)
        }
    }
    
    /// UnFriend User Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of unfriend data
    func unfriendUser(requestId: String, completion: @escaping(Bool,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.unfriendUser
        let headerData = RestAPI.shared.headerData
        
        let params = ["request_id" : requestId]
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(false,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(false,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            completion(true,jsonResponse["message"] as? String ?? "", statusCode)
        }
    }
    
    /// Add Friend User Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of unfriend data
    func addFriendUser(friendId: String, completion: @escaping(String?,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.addFriend
        let headerData = RestAPI.shared.headerData
        
        let params = ["friend_id" : friendId, "user_id" : UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? ""]
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(nil,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(nil,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            if let data = jsonResponse["data"] as? [String: Any] {
                let requestId = "\(data["request_id"] ?? "0")"
                completion(requestId,jsonResponse["message"] as? String ?? "", statusCode)
            } else {
                completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
            }
        }
    }
    
    /// All Request Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of unfriend data
    func allRequest(type: String, eventId: String, completion: @escaping([User]?,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.allRequest
        let headerData = RestAPI.shared.headerData
        
        var params = [String: Any]()
        params["type"] = type
        params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? ""
        if type == "2" {
            params["event_id"] = eventId
        }
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(nil,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(nil,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            if let serverData = jsonResponse["data"] as? [[String: Any]] {
                let userArr = serverData.map({ return User(jsonData: $0) })
                completion(userArr,jsonResponse["message"] as? String ?? "", statusCode)
            } else {
                completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
            }
        }
    }
    
    /// CancelAcceptRequest Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of cancelAcceptRequest data
    func cancelAcceptRequest(params: [String: Any], completion: @escaping(Bool,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.cancelAcceptRequest
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(false,"API Failed", 0)
                return
            }
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(false,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            completion(true,jsonResponse["message"] as? String ?? "", statusCode)
        }
    }
}


//MARK: - Decodable Encodable Example

/*
struct EventSD:Codable {
    
    let error_code: Int
    let message: String
    let data: [EventD]?
    
}
struct EventD:Codable {
    
    let event_id: String
    let calendar_event_id: String
    
    let event_name: String
    let event_location: String
    
}

extension RestAPI {
    
    /// Get Event List for Home Screen Data API
    ///
    /// - Parameters:
    ///   - completion: get success/error result of profile data
    func getHomeEventList(params: [String: Any], completion: @escaping([Event]?,String?, Int) -> ()) {
        
        let urlStr = APIConstant.MainUrl + APIConstant.homeEventList
        let headerData = RestAPI.shared.headerData
        
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:headerData).responseJSON{ response in
            guard response.result.isSuccess else {
                print("Error while fetching remote data: \(String(describing: response.result.error))")
                completion(nil,"API Failed", 0)
                return
            }
            guard let data = response.data else {
                completion(nil,"API Failed", 0)
                return
            }
            print(String(data: data, encoding: .utf8) ?? "No Data Found")
            
            do {
                let serverD = try JSONDecoder().decode(EventSD.self, from: data)
                print("let use the data \(serverD.error_code)")
            } catch {
                print("Error Occurred: \(error)")
            }
            return
            let jsonResponse = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject]
            //            print(jsonResponse)
            let statusCode = Int("\(jsonResponse["error_code"] as? Int ?? 0)") ?? 0
            guard statusCode == 100 else {
                completion(nil,jsonResponse["message"] as? String ?? "Network Error", statusCode)
                return
            }
            if let serverData = jsonResponse["data"] as? [[String: Any]] {
                let tempArr = serverData.map({ return Event(json: $0) })
                completion(tempArr,jsonResponse["message"] as? String ?? "", statusCode)
            } else {
                completion(nil,jsonResponse["message"] as? String ?? "", statusCode)
            }
        }
    }
}

*/
