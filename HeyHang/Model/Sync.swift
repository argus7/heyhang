//
//  Helper.swift
//  HeyHang
//
//  Created by Anup Kumar on 7/2/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import Foundation
import Contacts
import EventKit

class Sync: NSObject {
    
    static let instance = Sync()
    
    //Backgroud thread performance
    func syncContacts() {
        var params: [String: Any] = [:]
        params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"
        params["mobile"] = fetchPhoneContactList().joined(separator: ",").replacingOccurrences(of: "+", with: "")
        RestAPI.shared.syncContact(params: params)
    }
    func syncCalendar() {
        var params: [String: Any] = [:]
        params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"
        let eventsData = getSynchingCalendarParams()
        if eventsData.count > 0 {
            if let theJSONData = try? JSONSerialization.data(
                withJSONObject: eventsData,
                options: []) {
                let theJSONText = String(data: theJSONData,
                                         encoding: .ascii)
                params["calendar_event"] = "\(theJSONText ?? "")"
//                print("JSON string = \(params)")
                RestAPI.shared.syncCalendar(params: params)
            }
        }
    }
    func createCalendar() {
        self.createCalendarInLocal()
    }
    
    func getAllEventList() -> [EKEvent] {
        return getEventList()
    }
    
    //Calendar direct action Insert/Update/Delete
    func insertNewEvent(by title: String, start: Date, end: Date, location: String, latitude: Double, longitude: Double) -> String? {
        switch EKEventStore.authorizationStatus(for: .event) {
        case .authorized:
            return self.createCalendarEvent(by: title, start: start, end: end, location: location, latitude: latitude, longitude: longitude)
        case .denied:
            break
        case .notDetermined:
            EKEventStore().requestAccess(to: .event, completion:
                {(granted: Bool, error: Error?) -> Void in
            })
            break
        default:
            break
        }
        return nil
    }
    func updateOldEvent(by title: String, identifier: String, start: Date, end: Date, location: String, latitude: Double, longitude: Double) -> String? {
        return self.updateCalendarEvent(by: title, identifier: identifier, start: start, end: end, location: location, latitude: latitude, longitude: longitude)
    }
    func deleteEvent(_ identifier: String) {
        DispatchQueue.global(qos: .background).async {
            self.deleteEventInLocal(identifier)
        }
    }
    func joinCalendarEvent(by event: Event) {
        let title = event.name ?? ""
        let startDate = Date(timeIntervalSince1970: event.startDateTimeInterval ?? 0.0)
        let endDate = Date(timeIntervalSince1970: event.endDateTimeInterval ?? 0.0)
        let latitude = Double(event.latitude ?? "0.0") ?? 0.0
        let longitude = Double(event.longitude ?? "0.0") ?? 0.0
        let location = event.location ?? ""
        
        switch EKEventStore.authorizationStatus(for: .event) {
        case .authorized:
            let _ = self.createCalendarEvent(by: title, start: startDate, end: endDate, location: location, latitude: latitude, longitude: longitude)
        case .denied:
            break
        case .notDetermined:
            EKEventStore().requestAccess(to: .event, completion:
                {(granted: Bool, error: Error?) -> Void in
                    let _ = self.createCalendarEvent(by: title, start: startDate, end: endDate, location: location, latitude: latitude, longitude: longitude)
            })
            break
        default:
            break
        }
    }
    func leaveCalendarEvent(by event: Event) {
        
        let title = event.name ?? ""
        let startDate = Date(timeIntervalSince1970: event.startDateTimeInterval ?? 0.0)
        let endDate = Date(timeIntervalSince1970: event.endDateTimeInterval ?? 0.0)

        var identifier: String = ""
        let events = self.getEventList()
        for eventT in events {
            if ((eventT.title == title) && (eventT.startDate == startDate) && (eventT.endDate == endDate)) {
                identifier = eventT.eventIdentifier
                break
            }
        }
        if identifier.isEmpty {
            print("No Identifier Found")
        } else {
            DispatchQueue.global(qos: .background).async {
                self.deleteEventInLocal(identifier)
            }
        }
    }
}
// MARK: - Private Methods
extension Sync {
    
    //Feching Phone Contacts
    private func fetchPhoneContactList() -> [String] {
        var phoneNumberList: [String] = []
        let contactStore = CNContactStore()
        let keys = [ CNContactPhoneNumbersKey ] as [Any]
        let request = CNContactFetchRequest(keysToFetch: keys as! [CNKeyDescriptor])
        do {
            try contactStore.enumerateContacts(with: request){
                (contact, stop) in
                for phoneNumber in contact.phoneNumbers {
                    let number = (phoneNumber.value).value(forKey: "digits") as? String ?? ""
                    phoneNumberList.append(number)
                }
            }
            print(phoneNumberList)
        } catch {
            print("unable to fetch contacts")
        }
        return phoneNumberList
    }
    
    //Update event detail into server specified params
    private func getSynchingCalendarParams() -> [[String: Any]] {
        var eventArr: [[String: Any]] = []
        
        let mainEventArr = self.getEventList()
        for event in mainEventArr {
            var params = [String: Any]()
            params["calendar_event_id"] = event.eventIdentifier
            params["start"] = String(format: "%.f", event.startDate.timeIntervalSince1970)
            params["end"] = String(format: "%.f", event.endDate.timeIntervalSince1970)
            params["event_name"] = event.title
            params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"
            eventArr.append(params)
        }
        
        return eventArr
    }
    
    //check permission for calendar and get event list
    private func initializeCalendarEvents() {
        switch EKEventStore.authorizationStatus(for: .event) {
        case .authorized:
            break
        case .denied:
            break
        case .notDetermined:
            EKEventStore().requestAccess(to: .event, completion:
                {(granted: Bool, error: Error?) -> Void in
            })
            break
        default:
            break
        }
    }
    
    //Fetching event list if permission authorized
    private func getEventList() -> [EKEvent] {
        let store = EKEventStore()
        let calendars = store.calendars(for: .event)
        
        var eventList: [EKEvent] = []
        
        for calendar in calendars {
            let oneMonthAgo = Date(timeIntervalSinceNow: -30*24*3600)
            let oneMonthAfter = Date(timeIntervalSinceNow: +30*24*3600)
            
            let predicate = store.predicateForEvents(withStart: oneMonthAgo, end: oneMonthAfter, calendars: [calendar])
            
            let events = store.events(matching: predicate)
            for event in events {
                print(event)
                eventList.append(event)
            }
        }
        return eventList
    }
    
    //Insert New event in mobile phone calendar
    private func createCalendarEvent(by title: String, start: Date, end: Date, location: String, latitude: Double, longitude: Double) -> String? {
        let store = EKEventStore()
        let calendars = store.calendars(for: .event)
        for calendar in calendars {
            if calendar.title == Constant.CalendarName {
                
                let event = EKEvent(eventStore: store)
                event.calendar = calendar
                event.title = title
                event.startDate = start
                event.endDate = end
                
                let locationT = EKStructuredLocation()
                locationT.title = location
                locationT.geoLocation = CLLocation(latitude: latitude, longitude: longitude)
                event.structuredLocation = locationT
                
                do {
                    try store.save(event, span: .thisEvent)
                    print("Event Inserted Successfully")
                    self.syncCalendar() //Upload phone events to server for updating your schedule automatically
                    return event.eventIdentifier
                }
                catch {
                    print("Error saving event in calendar: \(error)")
                }
            }
        }
        return nil
    }
    //Update Old event in mobile phone calendar
    private func updateCalendarEvent(by title: String, identifier: String, start: Date, end: Date, location: String, latitude: Double, longitude: Double) -> String? {
        
        let store = EKEventStore()
        if let event = store.event(withIdentifier: identifier) {
            event.title = title
            event.startDate = start
            event.endDate = end
            
            let locationT = EKStructuredLocation()
            locationT.title = location
            locationT.geoLocation = CLLocation(latitude: latitude, longitude: longitude)
            event.structuredLocation = locationT
            
            do {
                try store.save(event, span: .thisEvent)
                self.syncCalendar() //Upload phone events to server for updating your schedule automatically
                return event.eventIdentifier
            }
            catch {
                print("Error deleting event in calendar: \(error)")
            }
        }
        return nil
    }
    
    //Delete event in mobile phone calendar to update availability status
    private func deleteEventInLocal(_ identifier: String) {
        let store = EKEventStore()
        if let event = store.event(withIdentifier: identifier) {
            do {
                try store.remove(event, span: .thisEvent)
                self.syncCalendar() //Upload phone events to server for updating your schedule automatically
                print("Successfully Removed Event")
                return
            }
            catch {
                print("Error deleting event in calendar: \(error)")
            }
        }
    }
    
    private func createCalendarInLocal() {
        var isCalendarExist: Bool = false
        let store = EKEventStore()
        let calendars = store.calendars(for: .event)
        for calendar in calendars {
            if calendar.title == Constant.CalendarName {
                isCalendarExist = true
                break
            }
        }
        
        if !isCalendarExist {
            let eventStore = EKEventStore()
            let newCalendar = EKCalendar(for: .event, eventStore: eventStore)
            newCalendar.title = Constant.CalendarName
            
            newCalendar.source = eventStore.defaultCalendarForNewEvents?.source
            do {
                try eventStore.saveCalendar(newCalendar, commit: true)
                print("Calendar Saved")
            } catch {
                print("Error in creating calendar!: \(error)")
            }
        } else {
            print("Calendar already exist")
        }
    }
}
