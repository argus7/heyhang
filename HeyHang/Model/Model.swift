//
//  Model.swift
//  HeyHang
//
//  Created by Anup Kumar on 5/9/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import Foundation

struct CustomPickerTitle {
    
    let title: String
    
    init(title: String) {
        self.title = title
    }
}

enum RideType {
    case ola
    case meru
    case uber
    case jugnoo
}

class UserTest: Decodable {
    
    var id: String?
    var first_name: String?
    var last_name: String?
    var mobile: String?
    var countryCode: String?
    var email: String?
    var image: String?
    var type: String?
    var latitude: String?
    var longitude: String?
    var civilID: String?
    
    init(jsonData: [String: Any]) {
        self.id = jsonData["id"] as? String
        self.first_name = jsonData["first_name"] as? String
        self.last_name = jsonData["last_name"] as? String
        self.mobile = jsonData["mobile"] as? String
        self.countryCode = jsonData["country_code"] as? String
        self.email = jsonData["email"] as? String
        self.image = jsonData["image"] as? String
        self.type = jsonData["type"] as? String
        self.latitude = jsonData["lat"] as? String
        self.longitude = jsonData["lon"] as? String
        self.civilID = jsonData["civil_id"] as? String
    }
    
    func saveUserDataIntoCache() {
        UserDefaults.standard.setValue(self.id ?? "", forKey: Constant.userDefaults.kUserId)
        UserDefaults.standard.setValue("\(first_name ?? "")", forKey: Constant.userDefaults.kUserName)
        UserDefaults.standard.setValue(countryCode ?? "", forKey: Constant.userDefaults.kUserMobileCountryCode)
        UserDefaults.standard.setValue(mobile ?? "", forKey: Constant.userDefaults.kUserMobile)
        UserDefaults.standard.setValue(email ?? "", forKey: Constant.userDefaults.kUserEmail)
        UserDefaults.standard.setValue(image ?? "", forKey: Constant.userDefaults.kUserImage)
        UserDefaults.standard.setValue(latitude ?? "", forKey: Constant.userDefaults.kLatitude)
        UserDefaults.standard.setValue(longitude ?? "", forKey: Constant.userDefaults.kLongitude)
        UserDefaults.standard.setValue(civilID ?? "", forKey: Constant.userDefaults.kUserCivilId)
        UserDefaults.standard.setValue("Yes", forKey: Constant.userDefaults.kIsRegistered)
    }
}

class User: NSObject {
    
    var id: String?
    var requestId: String?
    var userId: String?
    var friendId: String?
    var chatId: String?
    var status: String?
    var fullName: String?
    var image: String?
    var isInvited: Bool = false
    var isSelected: Bool = false
    var isFriend: String = "0"
    
    init(jsonData: [String: Any]) {
        self.id = jsonData["id"] as? String
        self.requestId = jsonData["request_id"] as? String
        self.userId = jsonData["user_id"] as? String
        self.chatId = jsonData["c_id"] as? String
        self.friendId = jsonData["friend_id"] as? String
        self.status = jsonData["status"] as? String
        self.fullName = jsonData["full_name"] as? String
        self.image = jsonData["profile_image"] as? String
        self.isInvited = (jsonData["is_invited"] as? String ?? "0") == "1" ? true : false
        self.isFriend = jsonData["is_friend"] as? String ?? "0"
    }
}

class SImage: NSObject {
    var id: String?
    var url: String?
    var image: UIImage?
    var isSelected: Bool = false
    
    init(jsonData: [String: Any]) {
        self.id = jsonData["id"] as? String
        self.url = jsonData["image"] as? String
    }
}


enum EventType {
    case free
    case paid
}

class Event {
    
    var eventId: String?
    var userId: String?
    var calendarEventId: String?
    var name: String?
    var location: String?
    var type: EventType = .free
    var privacy: String?
    var startDateTime: String?
    var endDateTime: String?
    var startDateTimeInterval: TimeInterval?
    var endDateTimeInterval: TimeInterval?
    var ticketCount: Int = 0
    var ticketPrice: String?
    var descriptionStr: String?
    var imageArr: [SImage] = []
    var suggestedImgArr: [SImage] = []
    var latitude: String?
    var longitude: String?
    var member: [User] = []
    var distance: String?
    var seatAvailable: Int = 0
    var status: Bool = false
    var isFeedback: Bool = true
    
    var ownerName: String?
    var ownerImage: String?
    
    init(json: [String: Any]) {
        
        self.eventId = json["event_id"] as? String ?? "0"
        self.userId = json["user_id"] as? String ?? "0"
        self.name = json["event_name"] as? String ?? ""
        self.calendarEventId = json["calendarEventId"] as? String ?? ""
        self.location = json["event_location"] as? String ?? ""
        self.ticketCount = Int(json["total_ticket"] as? String ?? "0") ?? 0
        self.ticketPrice = json["ticket_price"] as? String ?? "0"
        self.descriptionStr = json["event_description"] as? String ?? ""
        let typeT = json["event_type"] as? String ?? "1"
        self.type = (typeT == "1") ? .paid : .free
        self.privacy = json["event_privacy"] as? String ?? "1"
        self.latitude = json["lat"] as? String ?? "0"
        self.longitude = json["lng"] as? String ?? "0"
        self.seatAvailable = Int("\(json["seat_available"] ?? "0")") ?? 0
        self.ownerName = json["full_name"] as? String ?? ""
        self.ownerImage = json["profile_image"] as? String ?? ""
        
        self.isFeedback = ((json["is_feedback"] as? String ?? "0") == "1") ? true : false
        
        let distD = json["distance"] as? Double ?? 0.0
        if (distD == 0.0 || distD == 0) {
            self.distance = ""
        } else {
            self.distance = "- " + String(format: "%.1f", distD) + " miles"
        }
        self.status = (json["user_status"] as? Bool ?? false)
        
        if let imageArr = json["images"] as? [[String: Any]] {
            self.imageArr = []
            for object in imageArr {
                self.imageArr.append(SImage(jsonData: object))
            }
        }
        if let simageArr = json["suggested_images"] as? [[String: Any]] {
            self.suggestedImgArr = []
            for object in simageArr {
                self.suggestedImgArr.append(SImage(jsonData: object))
            }
        }
        
        if let members = json["members"] as? [[String: Any]] {
            self.member = members.map({ return User(jsonData: $0) })
        }
        
        let startDateT = TimeInterval("\(json["start_datetime"] as? String ?? "0")") ?? 0.0
        let endDateT = TimeInterval("\(json["end_datetime"] as? String ?? "0")") ?? 0.0
        
        self.startDateTimeInterval = startDateT
        self.endDateTimeInterval = endDateT
//        let startDate = Date(timeIntervalSince1970: startDateT)
//        let endDate = Date(timeIntervalSince1970: endDateT)
        
//        let formatter = DateFormatter()
//        formatter.dateFormat = "dd MMM | hh:mm a"
//        self.startDateTime = formatter.string(from: startDate)
//        self.endDateTime = formatter.string(from: endDate)
        
        self.startDateTime = "\(json["start_datetime"] as? String ?? "0")"
        self.endDateTime = "\(json["end_datetime"] as? String ?? "0")"
        
        if endDateT == 0 {
            self.endDateTime = "--"
        }
    }
}

class Ticket {
    
    var eventId: String?
    var name: String?
    var location: String?
    var type: EventType = .free
    var startDateTime: String?
    var endDateTime: String?
    var startDateTimeInterval: TimeInterval?
    var endDateTimeInterval: TimeInterval?
    var latitude: String?
    var longitude: String?
    var imageArr: [SImage] = []
    var seatAvailable: Int = 0
    
    var totalAmount: String?
    var ticketPrice: String?
    
    init(json: [String: Any]) {
        
        self.eventId = json["event_id"] as? String ?? "0"
        self.name = json["event_name"] as? String ?? ""
        self.location = json["event_location"] as? String ?? ""
        let typeT = json["event_type"] as? String ?? "1"
        self.type = (typeT == "1") ? .paid : .free
        
        self.seatAvailable = Int(json["seats"] as? String ?? "0") ?? 0
        self.totalAmount = json["total_amount"] as? String ?? "0"
        self.ticketPrice = json["ticket_price"] as? String ?? "0.0"
        
        self.latitude = json["lat"] as? String ?? "0"
        self.longitude = json["lng"] as? String ?? "0"
        
        if let imageArr = json["images"] as? [[String: Any]] {
            self.imageArr = []
            for object in imageArr {
                self.imageArr.append(SImage(jsonData: object))
            }
        }
        
        let startDateT = TimeInterval("\(json["start_datetime"] as? String ?? "0")") ?? 0.0
        let endDateT = TimeInterval("\(json["end_datetime"] as? String ?? "0")") ?? 0.0
        
        self.startDateTimeInterval = startDateT
        self.endDateTimeInterval = endDateT
        let startDate = Date(timeIntervalSince1970: startDateT)
        let endDate = Date(timeIntervalSince1970: endDateT)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM | hh:mm a"
        self.startDateTime = formatter.string(from: startDate)
        self.endDateTime = formatter.string(from: endDate)
        
    }
}

class Group {
    
    var id: String?
    var name: String?
    var isAdmin: Bool = false
    
    init(json: [String: Any]) {
        
        self.id = json["group_id"] as? String ?? "0"
        self.name = json["group_name"] as? String ?? ""
        let isAdmin = json["is_admin"] as? Int ?? 0
        self.isAdmin = (isAdmin == 0) ? false : true
       
    }
}

//MARK: - Chat Models
class ChatGroup: NSObject {
    
    var id: String?
    var eventId: String?
    var userId: String?
    var groupName: String?
    var memberIds: String?
    var status: Bool = false
    var updatedAt: TimeInterval = 0.0
    var image: UIImage?
    
    var message: String?
    var isSender: Bool = false
    var isAdmin: Bool = false
    
    
    init(json: [String: Any]) {
        
        self.id = json["c_id"] as? String
        self.eventId = json["event_id"] as? String
        self.userId = json["user_id"] as? String
        self.groupName = json["group_name"] as? String
        self.memberIds = json["member"] as? String
        self.status = (json["status"] as? String ?? "0") == "1" ? true : false
        self.updatedAt = TimeInterval(json["updated_at"] as? String ?? "") ?? 0.0
        self.image = UIImage(named: "")
        self.message = json["message"] as? String
        self.isSender = (json["is_sender"] as? String ?? "0") == "1" ? true : false
        self.isAdmin = (json["is_admin"] as? String ?? "0") == "1" ? true : false
    }
}

class ChatUser: NSObject {
    
    var id: String?
    var userId: String?
    var requestId: String?
    var fullName: String?
    var email: String?
    var image: String?
    var message: String?
    var isSender: Bool = false
    var updatedAt: TimeInterval = 0.0
    
    init(json: [String: Any]) {
        self.id = json["c_id"] as? String
        self.userId = json["user_id"] as? String
        self.requestId = json["request_id"] as? String
        self.fullName = json["full_name"] as? String
        self.email = json["email"] as? String
        self.image = json["profile_image"] as? String
        self.message = json["message"] as? String
        self.isSender = (json["is_sender"] as? String ?? "0") == "1" ? true : false
        self.updatedAt = TimeInterval(json["updated_at"] as? String ?? "") ?? 0.0
    }
}
