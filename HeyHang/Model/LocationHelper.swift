//
//  LocationHelper.swift
//  HeyHang
//
//  Created by Anup Kumar on 7/2/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import Foundation
import CoreLocation

protocol LocationHelperDelegate {
    func locationHelper(latitude: String, longitude: String)
    func locationHelper(address: String)
}

//MARK: - Location Helper Class
class LocationHelper: NSObject, CLLocationManagerDelegate {
    
    static let instance = LocationHelper()
    
    var delegate: LocationHelperDelegate?
    private var locationManager  =  CLLocationManager()
    
    func getuserCurrentLattLong(delegate: LocationHelperDelegate?){
        self.delegate = delegate
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let location = locations.last! as CLLocation
        if locations.count > 0 {
            self.locationManager.stopUpdatingLocation()
        }
        let currentLattitude =  "\(location.coordinate.latitude)"
        let currentLongitude =  "\(location.coordinate.longitude)"
        UserDefaults.standard.set(currentLattitude, forKey: Constant.userDefaults.kLatitude)
        UserDefaults.standard.set(currentLongitude, forKey: Constant.userDefaults.kLongitude)
        self.delegate?.locationHelper(latitude: currentLattitude, longitude: currentLongitude)
        self.getAddressFromLocation(location: location)
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed Getting Location")
    }
    private func getAddressFromLocation(location: CLLocation) {
        
        let ceo: CLGeocoder = CLGeocoder()
        let loc: CLLocation = location
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil) {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                if let pm = placemarks {
                    if pm.count > 0 {
                        let pm = placemarks![0]
                        var addressString : String = ""
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                        }
                        if pm.thoroughfare != nil {
                            addressString = addressString + pm.thoroughfare! + ", "
                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                        }
                        if pm.country != nil {
                            addressString = addressString + pm.country! + ", "
                        }
                        if pm.postalCode != nil {
                            addressString = addressString + pm.postalCode! + " "
                        }
                        UserDefaults.standard.set(addressString, forKey: Constant.userDefaults.kLocAddress)
                        self.delegate?.locationHelper(address: addressString)
                    }
                }
        })
    }
}
