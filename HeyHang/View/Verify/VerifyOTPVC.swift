//
//  VerifyOTPVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 5/27/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit
import AKSideMenu

class VerifyOTPVC: UIViewController {
    
    enum Navigation {
        case resetPassVerify
        case emailVerify
        case socialVerify
        case phoneVerify
    }
    
    @IBOutlet weak var logoIV: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    
    @IBOutlet weak var otp1TF: MATextField!
    @IBOutlet weak var otp2TF: MATextField!
    @IBOutlet weak var otp3TF: MATextField!
    @IBOutlet weak var otp4TF: MATextField!
    @IBOutlet weak var timerLbl: UILabel!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var resendBtn: UIButton!
    
    private var timer: Timer?
    private var timeRemain: Int = 15
    var navigation: Navigation = .emailVerify
    var userId: String = ""
    var resetLoginType: String = "1"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initializeNavigation()
        setupTextField()
    }
    override func viewWillAppear(_ animated: Bool) {
        setupTimer()
    }
    override func viewWillLayoutSubviews() {
        self.submitBtn.layer.cornerRadius = Constant.kCornerRadius
    }
    @IBAction func onClickResendBtn(_ sender: Any) {
        if checkInternet() {
            if let params = validateResendParams() {
                self.executeResendOTPApi(params: params)
            }
        }
    }
    @IBAction func onClickSubmitBtn(_ sender: Any) {
        if checkInternet() {
            if let params = validateParams() {
                self.executeVerifyOTPApi(params: params)
            }
        }
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    private func verifyOTPSuccessNavigation() {
        if navigation == .resetPassVerify {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ResetPasswordVC") as! ResetPasswordVC
            vc.userId = self.userId
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SuccessPopUpVC") as! SuccessPopUpVC
            vc.modalPresentationStyle = .overCurrentContext
            switch navigation {
            case .emailVerify:
                vc.navigation = .emailVerify
                break
            case .phoneVerify:
                vc.navigation = .phoneVerify
                break
            case .resetPassVerify:
                vc.navigation = .resetPassword
                break
            case .socialVerify:
                vc.navigation = .emailVerify
                break
            }
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }
    }
}
extension VerifyOTPVC: SuccessPopUpDelegate {
    func onActionPerform(on type: SuccessPopUpNavigation) {
        switch type {
        case .emailVerify, .createHang, .updateHang, .phoneVerify, .changePassword:
            self.moveToHome()
            break
        case .resetPassword:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ResetPasswordVC") as! ResetPasswordVC
            vc.userId = self.userId
            self.navigationController?.pushViewController(vc, animated: true)
            break
        }
    }
    private func moveToHome() {
        UserDefaults.standard.setValue("Yes", forKey: Constant.userDefaults.kIsRegistered)
        for controller: UIViewController in (self.navigationController?.viewControllers)! {
            if controller.isKind(of: AKSideMenu.self) {
                self.navigationController?.popToViewController(controller, animated: true)
                return
            }
        }
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let sideVC = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
        let vc: AKSideMenu = AKSideMenu(contentViewController: homeVC, leftMenuViewController: sideVC, rightMenuViewController: nil)
        vc.interactivePopGestureRecognizerEnabled = true
        vc.panFromEdge = false
        vc.bouncesHorizontally = false
        vc.contentViewShadowEnabled = true
        vc.contentViewShadowOpacity = 10.0
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
// MARK: - Helping Methods
extension VerifyOTPVC {
    private func initializeNavigation() {
        switch navigation {
        case .emailVerify:
            self.logoIV.image = UIImage(named: "email_verify")
            self.titleLbl.text = "Verification Code"
            self.descriptionLbl.text = "We have sent a 4 digit code on your registered email address"
            break
        case .socialVerify:
            self.logoIV.image = UIImage(named: "email_verify")
            self.titleLbl.text = "Verification Code"
            self.descriptionLbl.text = "We have sent a 4 digit code on your registered email address"
            break
        case .phoneVerify:
            self.logoIV.image = UIImage(named: "mobile_verify")
            self.titleLbl.text = "Verification Code"
            self.descriptionLbl.text = "We have sent a 4 digit code on your registered phone number"
            break
        case .resetPassVerify:
            self.logoIV.image = UIImage(named: "mobile_verify")
            self.titleLbl.text = "Verification Code"
            self.descriptionLbl.text = "We have sent a 4 digit code on your registered phone number"
            break
        }
    }
    private func setupTimer() {
        self.timerLbl.text = "00:\(timeRemain)"
        resendBtn.alpha = 0.4
        resendBtn.isUserInteractionEnabled = false
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerFunc), userInfo: nil, repeats: true)
        self.timerLbl.isHidden = false
    }
    @objc private func timerFunc() {
        if timeRemain == 0 {
            self.timer?.invalidate()
            self.timerLbl.isHidden = true
            self.timerLbl.text = "00:00"
            resendBtn.alpha = 1.0
            resendBtn.isUserInteractionEnabled = true
        } else {
            self.timerLbl.text = "00:\(timeRemain)"
            timeRemain -= 1
        }
    }
}
extension VerifyOTPVC : UITextFieldDelegate {
    
    private func setupTextField() {
        self.otp1TF.delegate = self
        self.otp2TF.delegate = self
        self.otp3TF.delegate = self
        self.otp4TF.delegate = self
        self.otp1TF.bottomLbl?.backgroundColor = UIColor.lightGray
        self.otp2TF.bottomLbl?.backgroundColor = UIColor.lightGray
        self.otp3TF.bottomLbl?.backgroundColor = UIColor.lightGray
        self.otp4TF.bottomLbl?.backgroundColor = UIColor.lightGray
        
        self.otp1TF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.otp2TF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.otp3TF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.otp4TF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.otp1TF {
            self.otp1TF.bottomLbl?.backgroundColor = UIColor.darkText
        } else if textField == self.otp2TF {
            self.otp2TF.bottomLbl?.backgroundColor = UIColor.darkText
        } else if textField == self.otp3TF {
            self.otp3TF.bottomLbl?.backgroundColor = UIColor.darkText
        } else if textField == self.otp4TF {
            self.otp4TF.bottomLbl?.backgroundColor = UIColor.darkText
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.otp1TF {
            self.otp1TF.bottomLbl?.backgroundColor = UIColor.lightGray
        } else if textField == self.otp2TF {
            self.otp2TF.bottomLbl?.backgroundColor = UIColor.lightGray
        } else if textField == self.otp3TF {
            self.otp3TF.bottomLbl?.backgroundColor = UIColor.lightGray
        } else if textField == self.otp4TF {
            self.otp4TF.bottomLbl?.backgroundColor = UIColor.lightGray
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if range.length + range.location > (textField.text?.count)! {
            return false
        }
        let newLength: Int = (textField.text?.count)! + string.count - range.length
        return newLength <= 1
    }
    @objc func textFieldDidChange(_ theTextField: UITextField) {
        if theTextField == self.otp1TF {
            if theTextField.text?.count == 1 {
                self.otp1TF.resignFirstResponder()
                self.otp2TF.becomeFirstResponder()
            }
        }
        else if theTextField == self.otp2TF {
            if theTextField.text?.count == 1 {
                self.otp2TF.resignFirstResponder()
                self.otp3TF.becomeFirstResponder()
            } else {
                let  char = self.otp2TF.text?.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                
                if (isBackSpace == -92) {
                    self.otp2TF.resignFirstResponder()
                    self.otp1TF.becomeFirstResponder()
                }
            }
        }
        else if theTextField == self.otp3TF {
            if theTextField.text?.count == 1 {
                self.otp3TF.resignFirstResponder()
                self.otp4TF.becomeFirstResponder()
            } else {
                let  char = self.otp3TF.text?.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                
                if (isBackSpace == -92) {
                    self.otp3TF.resignFirstResponder()
                    self.otp2TF.becomeFirstResponder()
                }
            }
        }
        else if theTextField == self.otp4TF {
            if theTextField.text?.count == 1{
                self.otp4TF.resignFirstResponder()
            } else {
                let  char = self.otp4TF.text?.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                
                if (isBackSpace == -92) {
                    self.otp4TF.resignFirstResponder()
                    self.otp3TF.becomeFirstResponder()
                }
            }
        }
    }
}
// MARK: - APIs
extension VerifyOTPVC {
    private func validateParams() -> [String: Any]? {
        
        let otpStr = self.otp1TF.text! + self.otp2TF.text! + self.otp3TF.text! + self.otp4TF.text!
        if (otpStr.isEmpty) || (otpStr.count < 4) {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter correct OTP")
            return nil
        }
        var params = [String: Any]()
        params["otp"] = otpStr
        params["user_id"] = self.userId
        params["device_type"] = "2"
        params["device_id"] = UIDevice.current.identifierForVendor?.uuidString
        params["device_token"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kDeviceToken) ?? "qwertyuiop"
        
        switch navigation {
        case .emailVerify:
            params["login_type"] = "2" //Email
            break
        case .socialVerify:
            params["login_type"] = "3" //Google
            break
        case .phoneVerify:
            params["login_type"] = "1" //Mobile
            break
        case .resetPassVerify:
            params["login_type"] = self.resetLoginType //Mobile
            break
        }
        return params
        
    }
    private func validateResendParams() -> [String: Any]? {
        
        var params = [String: Any]()
        params["user_id"] = self.userId
        
        switch navigation {
        case .emailVerify:
            params["login_type"] = "2" //Email
            break
        case .socialVerify:
            params["login_type"] = "3" //Email
            break
        case .phoneVerify:
            params["login_type"] = "1" //Mobile
            break
        case .resetPassVerify:
            params["login_type"] = self.resetLoginType //Mobile
            break
        }
        return params
        
    }
    private func executeVerifyOTPApi(params: [String: Any]) {
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.verifyOTP(params: params) { (errorMessage, errorCode) in
            self.hideActivityIndicator(uiView: self.view)
            if errorCode != 100 {
                print(errorMessage ?? "")
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "")
            } else {
                DispatchQueue.main.async {
                    self.verifyOTPSuccessNavigation()
                }
            }
        }
    }
    private func executeResendOTPApi(params: [String: Any]) {
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.resendOTP(params: params) { (errorMessage, errorCode) in
            self.hideActivityIndicator(uiView: self.view)
            self.setupTimer()
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "")
        }
    }
}

