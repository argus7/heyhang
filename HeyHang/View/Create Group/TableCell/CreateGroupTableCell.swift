//
//  CreateGroupTableCell.swift
//  HeyHang
//
//  Created by Anup Kumar on 6/4/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

class CreateGroupTableCell: UITableViewCell {
    
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var selectedIV: UIImageView!
    @IBOutlet weak var seperaterLbl: UILabel!
    @IBOutlet weak var detailBtn: UIButton!
    
    var user: User? {
        didSet {
            updateDetails()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        profileImgView.layer.cornerRadius = profileImgView.frame.size.width/2
        profileImgView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    private func updateDetails() {
        
        if let user = user {
            self.profileImgView?.imageFromServerURL(user.image ?? "", placeHolder: UIImage(named: "profile_placeholder"))
            self.nameLbl.text = user.fullName ?? ""
//            self.selectedIV.isHidden = !user.isSelected
        }
    }
}
