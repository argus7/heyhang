//
//  GroupPopUpVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 6/11/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

protocol GroupCreateDelegate {
    func onGroupCreate(text: String)
}
class GroupPopUpVC: UIViewController {

    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var saveBtnOutlet: UIButton!
    @IBOutlet weak var textFieldOutlet: UITextField!
    
    var delegate: GroupCreateDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        self.textFieldOutlet.delegate = self
    }
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.3) {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.15)
        }
    }
    override func viewWillLayoutSubviews() {
        popUpView.layer.cornerRadius = 15.0
        popUpView.clipsToBounds = true
        saveBtnOutlet.layer.cornerRadius = 10.0
        saveBtnOutlet.clipsToBounds = true
    }
    
    @IBAction func saveBtnAction(_ sender: UIButton) {
        if textFieldOutlet.text?.count == 0 {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter group name")
            return
        }
        self.dismiss(animated: true) {
            self.delegate?.onGroupCreate(text: self.textFieldOutlet.text ?? "")
        }
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        //self.dismiss(animated: true, completion: nil)
        self.view.removeFromSuperview()
        self.dismiss(animated: true, completion: nil)
    }
}
extension GroupPopUpVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
            return true
        }
        if (textFieldOutlet.text?.count == 0) && (string == " ") {
            return false
        }
        return true
    }
}
