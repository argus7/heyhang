//
//  CreateGroupVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 6/4/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit
import AKSideMenu

class CreateGroupVC: UIViewController {
    
    enum Navigation {
        case myFriend
        case myGroup
        case addGroupMember
        case removeGroupMember
    }

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var hdrLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var createGrpBtnOutlet: UIButton!
    
    @IBOutlet weak var selectedCountLbl: UILabel! //4 Members Selected
    
    private var users: [User] = []
    private var isLoading: Bool = false
    var navigation: Navigation = .myFriend
    var previousUsers: [User] = []
    var selectedGroupId: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initialization()
        self.selectedCountLbl.text = "0 Members Selected"
        setupTableView()
    }
    override func viewWillLayoutSubviews() {
        self.headerView.setSideDropShadow(isUp: false)
        
        self.createGrpBtnOutlet.layer.cornerRadius = 5
        self.createGrpBtnOutlet.clipsToBounds = true
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        if checkInternet() {
            
            switch navigation {
            case .myFriend:
                self.isLoading = true
                self.tableView.reloadData()
                self.executeAPIForUserList()
                break
            case .myGroup:
                self.isLoading = true
                self.tableView.reloadData()
                self.executeAPIForUserList()
                break
            case .addGroupMember:
                self.isLoading = true
                self.tableView.reloadData()
                self.executeAPIForUserList()
                break
            case .removeGroupMember:
                self.users = self.previousUsers
                self.tableView.reloadData()
                break
            }
        }
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func createGrpBtnAction(_ sender: UIButton) {
        let selectedA = users.filter({ return $0.isSelected })
        if selectedA.count > 0 {
            switch navigation {
            case .myGroup:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "GroupPopUpVC") as! GroupPopUpVC
                vc.modalPresentationStyle = .overCurrentContext
                vc.delegate = self
                self.present(vc, animated: true, completion: nil)
                break
            case .myFriend:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "GroupPopUpVC") as! GroupPopUpVC
                vc.modalPresentationStyle = .overCurrentContext
                vc.delegate = self
                self.present(vc, animated: true, completion: nil)
                break
            case .addGroupMember:
                if checkInternet() {
                    let tUser = users.filter({ return !($0.isInvited) })
                    let selectedA = tUser.filter({ return $0.isSelected })
                    let ids = selectedA.map({ return ($0.userId ?? "") }).joined(separator: ",")
                    self.executeAPIAddMemberGroup(memberIds: ids)
                }
                break
            case .removeGroupMember:
                let selectedA = users.filter({ return $0.isSelected })
                let ids = selectedA.map({ return ($0.userId ?? "") }).joined(separator: ",")
                removeMemberConfirmAlert(memberIds: ids)
                break
            }
        } else {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please select member first")
        }
    }
    private func initialization() {
        switch navigation {
        case .myGroup:
            self.hdrLbl.text = "Create Group"
            self.createGrpBtnOutlet.setTitle("Create Group", for: .normal)
            break
        case .myFriend:
            self.hdrLbl.text = "Create Group"
            self.createGrpBtnOutlet.setTitle("Create Group", for: .normal)
            break
        case .addGroupMember:
            self.hdrLbl.text = "Add Member"
            self.createGrpBtnOutlet.setTitle("Add Member", for: .normal)
            break
        case .removeGroupMember:
            self.hdrLbl.text = "Remove Member"
            self.createGrpBtnOutlet.setTitle("Remove Member", for: .normal)
            break
        }
    }
    private func removeMemberConfirmAlert(memberIds: String) {
        let alertController = UIAlertController.init(title: Constant.APP_NAME, message: "Are you sure want to remove these members?", preferredStyle: .alert)
        alertController.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
        }))
        alertController.addAction(UIAlertAction.init(title: "Remove", style: .destructive, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
            self.executeAPIRemoveMemberGroup(memberIds: memberIds)
        }))
        self.present(alertController, animated: true, completion: nil)
    }
}
extension CreateGroupVC: GroupCreateDelegate {
    func onGroupCreate(text: String) {
        if checkInternet() {
            self.executeAPIForCreateGroup(name: text)
        }
    }
}
extension CreateGroupVC: UITableViewDelegate, UITableViewDataSource {
    fileprivate func setupTableView() {
        self.tableView.register(UINib(nibName: "CreateGroupTableCell", bundle: Bundle.main), forCellReuseIdentifier: "CreateGroupTableCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.users.count > 0 {
            self.tableView.backgroundView = nil
            return self.users.count
        } else {
            if navigation == .myFriend {
                self.tableView.showBackgroundMsg(msg: isLoading ? "Loading..." : "You have no friends")
            } else {
                self.tableView.showBackgroundMsg(msg: isLoading ? "Loading..." : "No Users Found")
            }
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "CreateGroupTableCell", for: indexPath) as! CreateGroupTableCell
        cell.user = self.users[indexPath.row]
        cell.selectedIV.isHidden = !users[indexPath.row].isSelected
        cell.detailBtn.tag = indexPath.row
        cell.detailBtn.addTarget(self, action: #selector(onClickDetailBtn(_:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    @objc func onClickDetailBtn(_ sender: UIButton) {
        if navigation == .addGroupMember {
            if !users[sender.tag].isInvited {
                users[sender.tag].isSelected = !users[sender.tag].isSelected
                self.tableView.reloadData()
                let selectedA = users.filter({ return $0.isSelected })
                self.selectedCountLbl.text = "\(selectedA.count) Members Selected"
            }
        } else {
            users[sender.tag].isSelected = !users[sender.tag].isSelected
            self.tableView.reloadData()
            let selectedA = users.filter({ return $0.isSelected })
            self.selectedCountLbl.text = "\(selectedA.count) Members Selected"
        }
    }
}

// MARK: - APIs
extension CreateGroupVC {
    
    //API For get intial list of members to add in your group
    private func executeAPIForUserList() {
        //'user_id','event_privacy','event_id'
        var params = [String: Any]()
        params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? ""
        params["event_privacy"] = "1" //For getting friend list out side event creation
        params["event_id"] = ""
        
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.userList(params: params) { (users, errorMessage, errorCode) in
            self.isLoading = false
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                if let userArr = users {
                    if self.navigation == .addGroupMember {
                        var tUser = [User]()
                        for object in userArr {
                            let isExistArr = self.previousUsers.filter({ return ($0.userId ?? "" == (object.userId ?? "")) })
                            if isExistArr.count > 0 {
                                let tobject = object
                                tobject.isSelected = true
                                tobject.isInvited = true
                                tUser.append(tobject)
                            } else {
                                tUser.append(object)
                            }
                        }
                        self.users = tUser
                    } else {
                        self.users = userArr
                    }
                }
                self.tableView.reloadData()
            }
        }
    }
    
    //API For creating group with group name passing by alert popup
    private func executeAPIForCreateGroup(name: String) {
        
        let selectedA = users.filter({ return $0.isSelected }).map({ return $0.userId ?? "" })
        
        var params = [String: Any]()
        params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? ""
        params["group_name"] = name
        params["member_id"] = selectedA.joined(separator: ",")
        
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.createGroup(params: params) { (isSuccess, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                if isSuccess {
                   self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    //API For removing member from my created group
    private func executeAPIRemoveMemberGroup(memberIds: String) {
        
        var params = [String: Any]()
        params["group_id"] = self.selectedGroupId
        params["member_id"] = memberIds
        
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.removeGroupMembers(params: params) { (isSuccess, errorMessage, errorCode) in
            self.hideActivityIndicator(uiView: self.view)
            DispatchQueue.main.async {
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                if isSuccess {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    //API For removing member from my created group
    private func executeAPIAddMemberGroup(memberIds: String) {
        
        var params = [String: Any]()
        params["group_id"] = self.selectedGroupId
        params["member_id"] = memberIds
        
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.addGroupMembers(params: params) { (isSuccess, errorMessage, errorCode) in
            self.hideActivityIndicator(uiView: self.view)
            DispatchQueue.main.async {
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                if isSuccess {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
}

