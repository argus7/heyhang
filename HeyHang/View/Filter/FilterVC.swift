//
//  FilterVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 6/7/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

protocol FilterDelegate {
    func onFilterSelected(params: [String: Any])
}

class FilterVC: UIViewController {
    
    enum EType: Int {
        case allEvent = 3
        case paidEvent = 1
        case freeEvent = 2
    }
    @IBOutlet weak var radiusSlider: MASlider!
    @IBOutlet weak var timeSlider: MASlider!
    @IBOutlet weak var allaEventIV: UIImageView!
    @IBOutlet weak var paidEventIV: UIImageView!
    @IBOutlet weak var freeEventIV: UIImageView!
    @IBOutlet weak var friendIV: UIImageView!
    
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var applyBtnOutlet: UIButton!
    @IBOutlet weak var clearBtnOutlet: UIButton!
    @IBOutlet weak var allEventBtnOutlet: UIButton!
    @IBOutlet weak var paidEventBtnOutlet: UIButton!
    @IBOutlet weak var freeEventBtnOutlet: UIButton!
    @IBOutlet weak var checkedFriendsBtn: UIButton!
    
    private var eventType: EType = .allEvent
    private var isFriendActive: Bool = false
    var delegate: FilterDelegate?
    var filterParams: [String: Any]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        updateEventType(type: .allEvent)
        self.friendIV.image = UIImage(named: "check_normal")
        self.initializeSlider()
    }
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.3) {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.25)
        }
        self.setSelectedValue()
    }
    
    override func viewWillLayoutSubviews() {
        popUpView.layer.cornerRadius = 25.0
        popUpView.clipsToBounds = true
        applyBtnOutlet.layer.cornerRadius = 5.0
        applyBtnOutlet.clipsToBounds = true
        clearBtnOutlet.layer.cornerRadius = 5.0
        clearBtnOutlet.clipsToBounds = true
        clearBtnOutlet.layer.borderColor = UIColor.gray.withAlphaComponent(0.4).cgColor
        clearBtnOutlet.layer.borderWidth = 1.0
        
    }
    //MARK: - ALL IBActions
    @IBAction func onClickBackBtn(_ sender: Any) {
        //self.dismiss(animated: true, completion: nil)
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func allEventBtnAction(_ sender: UIButton) {
        updateEventType(type: .allEvent)
    }
    
    @IBAction func paidEventBtnAction(_ sender: UIButton) {
        updateEventType(type: .paidEvent)
    }
    
    @IBAction func freeEventBtnAction(_ sender: UIButton) {
        updateEventType(type: .freeEvent)
    }
    
    @IBAction func checkedFriendsBtnAction(_ sender: UIButton) {
        self.isFriendActive = !self.isFriendActive
        if isFriendActive {
            self.friendIV.image = UIImage(named: "check_active")
        } else {
            self.friendIV.image = UIImage(named: "check_normal")
        }
    }
    
    @IBAction func applyBtnAction(_ sender: UIButton) {
        if let params = self.validate() {
            self.dismiss(animated: true) {
                self.delegate?.onFilterSelected(params: params)
            }
        }
    }
    
    @IBAction func clearBtnAction(_ sender: UIButton) {
        self.filterParams = [:]
        updateEventType(type: .allEvent)
        self.friendIV.image = UIImage(named: "check_normal")
        isFriendActive = false
        
        self.radiusSlider.animate(with: 1.0)
        self.radiusSlider.setValue(1.0, animated: true)
        self.timeSlider.animate(with: 1.0)
        self.timeSlider.setValue(1.0, animated: true)
    }
}
extension FilterVC {
    private func initializeSlider() {
        
        //Radius Slider Initialization
        self.radiusSlider.setThumbImage(UIImage(named:"seek_icon"), for: .normal)
        self.radiusSlider.setThumbImage(UIImage(named:"seek_icon"), for: .highlighted)
        self.radiusSlider.delegate = self
        self.timeSlider.setThumbImage(UIImage(named:"seek_icon"), for: .normal)
        self.timeSlider.setThumbImage(UIImage(named:"seek_icon"), for: .highlighted)
        self.timeSlider.delegate = self
        
        self.timeSlider.minimumValue = 1
        self.timeSlider.maximumValue = 4
        self.radiusSlider.minimumValue = 1
        self.radiusSlider.maximumValue = 4
    }
    private func updateEventType(type: EType) {
        self.eventType = type
        switch type {
        case .allEvent:
            self.allaEventIV.image = UIImage.init(named: "radio_active")
            self.paidEventIV.image = UIImage.init(named: "radio_normal")
            self.freeEventIV.image = UIImage.init(named: "radio_normal")
            break
        case .freeEvent:
            self.allaEventIV.image = UIImage.init(named: "radio_normal")
            self.paidEventIV.image = UIImage.init(named: "radio_normal")
            self.freeEventIV.image = UIImage.init(named: "radio_active")
            break
        case .paidEvent:
            self.allaEventIV.image = UIImage.init(named: "radio_normal")
            self.paidEventIV.image = UIImage.init(named: "radio_active")
            self.freeEventIV.image = UIImage.init(named: "radio_normal")
            break
        }
    }
}
extension FilterVC : MASliderDelegate {
    func onMASliderChanged(_ slider: MASlider, value: Float) {
    }
    func onMASliderFinishChanged(_ slider: MASlider, value: Float) {
        if slider == self.radiusSlider {
            if value < 1.5 {
                self.radiusSlider.animate(with: 1.0)
                self.radiusSlider.setValue(1.0, animated: true)
            } else if (value > 1.5 && value < 2.5) {
                self.radiusSlider.animate(with: 2.0)
            } else if (value > 2.5 && value < 3.5) {
                self.radiusSlider.animate(with: 3.0)
            } else if (value > 3.5) {
                self.radiusSlider.animate(with: 4.0)
                self.radiusSlider.setValue(5.0, animated: true)
            }
        } else if slider == self.timeSlider {
            if value < 1.5 {
                self.timeSlider.animate(with: 1.0)
                self.timeSlider.setValue(1.0, animated: true)
            } else if (value > 1.5 && value < 2.5) {
                self.timeSlider.animate(with: 2.0)
            } else if (value > 2.5 && value < 3.5) {
                self.timeSlider.animate(with: 3.0)
            } else if (value > 3.5) {
                self.timeSlider.animate(with: 4.0)
                self.timeSlider.setValue(4.0, animated: true)
            }
        }
    }
}
// MARK: - APIs
extension FilterVC {
    private func validate() -> [String: Any]? {
        
//        var isFilterValid: Bool = false
        var radius: String = ""
        var time: String = ""
        var timeKey: String = ""
        if radiusSlider.value != radiusSlider.minimumValue {
//            isFilterValid = true
            let rad = Int(self.radiusSlider.value)
            radius = (rad == 1 ? "1" : (rad == 2 ? "5" : (rad == 3 ? "10" : "20")))
        } else {
            radius = "1"
        }
        if timeSlider.value != timeSlider.minimumValue {
//            isFilterValid = true
            let ttime = Int(self.timeSlider.value)
            time = (ttime == 1 ? "1" : (ttime == 2 ? "6" : (ttime == 3 ? "1" : "7")))
            timeKey = (ttime == 1 ? "1" : (ttime == 2 ? "6" : (ttime == 3 ? "2" : "2")))
        } else {
            time = "1"
            timeKey = "1"
        }
        if isFriendActive {
//            isFilterValid = true
        }
//        if !isFilterValid {
//            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please select at least one filter")
//            return nil
//        }
        var params = [String: Any]()
        params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"
        params["latitude"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kLatitude) ?? "0"
        params["longitude"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kLongitude) ?? "0"
        params["radius"] = radius
        params["time_key"] = timeKey
        params["time_value"] = time
        params["event_type"] = "\(eventType.rawValue)"
        params["start"] = "0"
        params["friends"] = isFriendActive ? "1" : ""
        
        return params
    }
    private func setSelectedValue() {
        guard let params = self.filterParams  else { return }
        let radius = Int("\(params["radius"] ?? "1")") ?? 1
        if radius == 5 {
            self.radiusSlider.value = 2.0
        } else if radius == 10 {
            self.radiusSlider.value = 3.0
        } else if radius == 20 {
            self.radiusSlider.value = 4.0
        } else {
            self.radiusSlider.value = 1.0
        }
        let ttime = Int("\(params["time_value"] ?? "1")") ?? 1
        let timeKey = Int("\(params["time_key"] ?? "1")") ?? 1
        
        
        if (ttime == 1 && timeKey == 2) {
            self.timeSlider.value = 3.0
        } else if (ttime == 7 && timeKey == 2) {
            self.timeSlider.value = 4.0
        } else if (ttime == 6 && timeKey == 6) {
            self.timeSlider.value = 2.0
        } else {
            self.timeSlider.value = 1.0
        }

//        let ttime = Int(self.timeSlider.value)
//        time = (ttime == 1 ? "1" : (ttime == 2 ? "6" : (ttime == 3 ? "1" : "7")))
//        timeKey = (ttime == 1 ? "1" : (ttime == 2 ? "6" : (ttime == 3 ? "2" : "2")))
//        params["time_key"] = timeKey
//        params["time_value"] = time
        
        
        let eventT = params["event_type"] as? String ?? "3"
        if eventT == "1" {
            updateEventType(type: .paidEvent)
        } else if eventT == "2" {
            updateEventType(type: .freeEvent)
        } else {
            updateEventType(type: .allEvent)
        }
        
        let isFrd = params["friends"] as? String ?? "0"
        if isFrd == "1" {
            self.isFriendActive = true
            self.friendIV.image = UIImage(named: "check_active")
        } else {
            self.isFriendActive = false
            self.friendIV.image = UIImage(named: "check_normal")
        }
    }
}
