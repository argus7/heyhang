//
//  ContactUs.swift
//  HeyHang
//
//  Created by Anup Kumar on 5/28/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

class ContactUs: UIViewController {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var hdrLbl: UILabel!
    @IBOutlet weak var nameTF: MATextField!
    @IBOutlet weak var emailTF: MATextField!
    @IBOutlet weak var subjectTF: MATextField!
    @IBOutlet weak var messageTF: MATextField!
    @IBOutlet weak var submitBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupTextField()
    }
    override func viewWillLayoutSubviews() {
        self.submitBtn.layer.cornerRadius = Constant.kCornerRadius
        self.headerView.setSideDropShadow(isUp: false)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func onClickbackbtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickSubmitBtn(_ sender: Any) {
        self.view.endEditing(true)
        if checkInternet() {
            if let params = validateParams() {
                self.executeContactUsApi(params: params)
            }
        }
    }
    @IBAction func onClickCallBtn(_ sender: Any) {
        //+96 752 254 2563
        guard let number = URL(string: "tel://+967522542563") else { return }
        UIApplication.shared.open(number, options: [:], completionHandler: nil)
    }
}

extension ContactUs : UITextFieldDelegate {
    
    private func setupTextField() {
        self.nameTF.delegate = self
        self.emailTF.delegate = self
        self.subjectTF.delegate = self
        self.messageTF.delegate = self
        self.nameTF.bottomLbl?.backgroundColor = UIColor.lightGray
        self.emailTF.bottomLbl?.backgroundColor = UIColor.lightGray
        self.subjectTF.bottomLbl?.backgroundColor = UIColor.lightGray
        self.messageTF.bottomLbl?.backgroundColor = UIColor.lightGray
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.nameTF {
            self.nameTF.bottomLbl?.backgroundColor = UIColor.darkText
        } else if textField == self.emailTF {
            self.emailTF.bottomLbl?.backgroundColor = UIColor.darkText
        } else if textField == self.subjectTF {
            self.subjectTF.bottomLbl?.backgroundColor = UIColor.darkText
        } else if textField == self.messageTF {
            self.messageTF.bottomLbl?.backgroundColor = UIColor.darkText
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.nameTF {
            self.nameTF.bottomLbl?.backgroundColor = UIColor.lightGray
        } else if textField == self.emailTF {
            self.emailTF.bottomLbl?.backgroundColor = UIColor.lightGray
        } else if textField == self.subjectTF {
            self.subjectTF.bottomLbl?.backgroundColor = UIColor.lightGray
        } else if textField == self.messageTF {
            self.messageTF.bottomLbl?.backgroundColor = UIColor.lightGray
        }
    }
}

// MARK: - APIs
extension ContactUs {
    
    private func validateParams() -> [String: Any]? {
        guard let name  = self.nameTF.text, !(self.nameTF.text?.isEmpty)!  else {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter your name")
            return nil
        }
        guard let email  = self.emailTF.text, !(self.emailTF.text?.isEmpty)!  else {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter your email")
            return nil
        }
        if !self.isValidEmail(email) {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter correct email")
            return nil
        }
        guard let subject  = self.subjectTF.text, !(self.subjectTF.text?.isEmpty)!  else {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter subject")
            return nil
        }
        guard let message  = self.messageTF.text, !(self.messageTF.text?.isEmpty)!  else {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please write some message")
            return nil
        }
        var params = [String: Any]()
        params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"
        params["full_name"] = name
        params["email"] = email
        params["subject"] = subject
        params["message"] = message
        return params
        //'user_id', 'email', 'full_name', 'subject', 'message'
    }
    
    private func executeContactUsApi(params: [String: Any]) {
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.contactUs(params: params) { (errorMessage, errorCode) in
            self.hideActivityIndicator(uiView: self.view)
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "")
            if errorCode == 100 {
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
}
