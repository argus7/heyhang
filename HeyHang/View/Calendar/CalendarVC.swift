//
//  CalendarVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 5/28/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit
import EventKit

class CalendarTblCell: UITableViewCell {
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var dateTimeLbl: UILabel! //10:30 AM to 04:30 PM
    //Height 120
    //Cell
}
class HangCal {
    
    var date: Date?
    var title: String = ""
    var events:[EKEvent] = []
    
}

class CalendarVC: UIViewController {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var hdrLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addBtn: UIButton!
    
    private var events = [HangCal]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillLayoutSubviews() {
        self.addBtn.setDropShadow(cornerRadius: 27.5)
        self.addBtn.backgroundColor = UIColor.clear
        self.headerView.setSideDropShadow(isUp: false)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        executeApiForGetEventList()
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickAddbtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddEventVC") as! AddEventVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension CalendarVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.events.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return events[section].events.count
//        let key = Array(events.keys).reversed()[section]
//        if let value = events[key] {
//            return value.count
//        }
//        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CalendarTblCell
        
        cell.cardView.layer.cornerRadius = 15.0
        cell.cardView.clipsToBounds = true
        
//        let key = Array(events.keys).reversed()[indexPath.section]
//        if let value = events[key] {
//
//        }
        
        let event = events[indexPath.section].events[indexPath.row]
        let location = event.location ?? ""
        cell.nameLbl.text = event.title
        cell.locationLbl.text = location.isEmpty ? "-" : location
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM, hh:mm a"
        let startDT = formatter.string(from: event.startDate)
        let endDT = formatter.string(from: event.endDate)
        
        cell.dateTimeLbl.text = "\(startDT) - \(endDT)"
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        
        headerView.backgroundColor = UIColor.clear
        headerView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50.0)
        
        //Circle Creation
        let circleLbl = UILabel(frame: CGRect(x: 10, y: 15, width: 20, height: 20))
        circleLbl.text = ""
        circleLbl.setBorder(color: UIColor.purpleSelected, width: 3.0, radius: 10.0)
        circleLbl.backgroundColor = UIColor(red: 234/255.0, green: 236/255.0, blue: 241/255.0, alpha: 1.0)
        
        //Text label Creation
        let textLbl = UILabel(frame: CGRect(x: 40, y: 0.0, width: (UIScreen.main.bounds.width - 60.0), height: 50.0))
        textLbl.textColor = UIColor.purpleSelected
        textLbl.text = events[section].title
        textLbl.font = self.hdrLbl.font
        textLbl.backgroundColor = UIColor(red: 234/255.0, green: 236/255.0, blue: 241/255.0, alpha: 1.0)
        
        headerView.addSubview(circleLbl)
        headerView.addSubview(textLbl)
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
}
extension CalendarVC {
    private func executeApiForGetEventList() {
        var tempArr = [HangCal]()
        let eventsT = Sync.instance.getAllEventList()
        for event in eventsT {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd MMM, yyyy"
            let startD = formatter.string(from: event.startDate)
            
            if let index = isExist(arr: tempArr, title: startD) {
                tempArr[index].events.append(event)
            } else {
                let temp = HangCal()
                temp.title = startD
                temp.date = event.startDate
                temp.events.append(event)
                tempArr.append(temp)
            }
        }
        tempArr = tempArr.sorted(by: { (first, second) -> Bool in
            return first.date!.compare(second.date!) == .orderedAscending
        })
        self.events = tempArr
        self.tableView.reloadData()
    }
    private func isExist(arr: [HangCal], title: String) -> Int? {
        var index: Int = 0
        for object in arr {
            if object.title == title {
                return index
            }
            index += 1
        }
        return nil
    }
}
