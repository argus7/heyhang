//
//  GroupListTblCell.swift
//  HeyHang
//
//  Created by Mohd Arsad on 18/06/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

class GroupListTblCell: UITableViewCell {
    
    @IBOutlet weak var profileIV: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var descriptionLblHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var optionIV: UIImageView!
    @IBOutlet weak var optionBtn: UIButton!
    @IBOutlet weak var profileWidthConstraints: NSLayoutConstraint!
    @IBOutlet weak var detailBtn: UIButton!
    
    var group: Group? {
        didSet {
            updateDetails()
        }
    }
    var user: User? {
        didSet {
            updateProfileDetails()
        }
    }
    var chat: ChatGroup? {
        didSet {
            updateChatDetails()
        }
    }
    var chatU: ChatUser? {
        didSet {
            updateChatUserDetails()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.profileIV.setCircleCorner()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    private func updateDetails() {
        if let group = group {
            self.titleLbl.text = group.name ?? ""
            self.descriptionLbl.text = ""
            self.descriptionLblHeightConstraints.constant = 0.0
            self.profileIV.image = UIImage(named: "logo")
        }
    }
    private func updateProfileDetails() {
        if let user = user {
            self.titleLbl.text = user.fullName ?? ""
            self.descriptionLbl.text = ""
            self.profileIV.imageFromServerURL(user.image ?? "", placeHolder: UIImage(named: "profile_placeholder"))
            self.descriptionLblHeightConstraints.constant = 0.0
        }
    }
    private func updateChatDetails() {
        if let chat = chat {
            self.titleLbl.text = chat.groupName ?? ""
            self.descriptionLbl.text = chat.message ?? ""
            self.profileIV.image = UIImage(named: "logo")
        }
    }
    private func updateChatUserDetails() {
        if let chatUT = chatU {
            self.titleLbl.text = chatUT.fullName ?? ""
            self.descriptionLbl.text = chatUT.message ?? ""
            self.profileIV.imageFromServerURL(chatUT.image ?? "", placeHolder: UIImage(named: "profile_placeholder"))
        }
    }
}
