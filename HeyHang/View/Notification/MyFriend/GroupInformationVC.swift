//
//  GroupInformationVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 7/4/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit
import AKSideMenu

class GroupInformationVC: UIViewController {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var hdrLbl: UILabel!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var groupNameLbl: UILabel!
    @IBOutlet weak var deleteView: UIView!
    @IBOutlet weak var deleteGroupBtn: UIButton!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var noOfMemberLbl: UILabel!
    @IBOutlet weak var addRemoveView: UIView!
    @IBOutlet weak var addMemberBtn: UIButton!
    @IBOutlet weak var removeMemberBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addRemoveViewHeightConstraints: NSLayoutConstraint!
    
    public var groupId: String = "0"
    public var groupName: String = ""
    public var isAdmin : Bool = false
    
    private var members: [User] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupTableView()
    }
    override func viewWillLayoutSubviews() {
        self.headerView.setSideDropShadow(isUp: false)
        self.titleView.setLightDropShadow(cornerRadius: 10.0)
        self.cardView.setLightDropShadow(cornerRadius: 10.0)
        self.deleteView.setLightDropShadow(cornerRadius: 10.0)
        
        self.addMemberBtn.layer.cornerRadius = 10.0
        self.removeMemberBtn.layer.cornerRadius = 10.0
        self.addMemberBtn.clipsToBounds = true
        self.removeMemberBtn.clipsToBounds = true
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        updatePreviousDetails()
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickDeleteGroupBtn(_ sender: Any) {
        if isAdmin {
            print("Delete Group")
            leaveGroupConfirmAlert(isLeave: false)
        } else {
            print("Leave Group")
            leaveGroupConfirmAlert(isLeave: true)
        }
    }
    @IBAction func onClickAddMemberBtn(_ sender: Any) {
        let currentUserId = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? ""
        let tMember = self.members.filter({ return !($0.userId ?? "" == currentUserId) })
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateGroupVC") as! CreateGroupVC
        vc.navigation = .addGroupMember
        vc.previousUsers = tMember
        vc.selectedGroupId = self.groupId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onClickRemoveMemberBtn(_ sender: Any) {
        let currentUserId = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? ""
        let tMember = self.members.filter({ return !($0.userId ?? "" == currentUserId) })
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateGroupVC") as! CreateGroupVC
        vc.navigation = .removeGroupMember
        vc.previousUsers = tMember
        vc.selectedGroupId = self.groupId
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension GroupInformationVC {
    private func updatePreviousDetails() {
        self.groupNameLbl.text = groupName
        self.noOfMemberLbl.text = ""
        if isAdmin {
            self.addRemoveViewHeightConstraints.constant = 40.0
            self.addRemoveView.isHidden = false
            self.deleteGroupBtn.setTitle("Delete Group", for: .normal)
        } else {
            self.addRemoveViewHeightConstraints.constant = 0.0
            self.addRemoveView.isHidden = true
            self.deleteGroupBtn.setTitle("Leave Group", for: .normal)
        }
        if checkInternet() {
            self.executeAPIMyGroupList(groupId: groupId)
        }
    }
    private func leaveGroupConfirmAlert(isLeave:Bool) {
        let alertController = UIAlertController.init(title: Constant.APP_NAME, message: "Are you sure want to \(isLeave ? "leave" : "delete") this group?", preferredStyle: .alert)
        alertController.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
        }))
        alertController.addAction(UIAlertAction.init(title: "\(isLeave ? "Leave" : "Delete")", style: .destructive, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
            if isLeave {
                self.executeAPILeaveGroup()
            } else {
                self.executeAPIDeleteGroup()
            }
        }))
        self.present(alertController, animated: true, completion: nil)
    }
}
// MARK: - UITableViewDelegate, UITableViewDataSource, UITableViewPullRefreshDelegate
extension GroupInformationVC: UITableViewDelegate, UITableViewDataSource {
    
    fileprivate func setupTableView() {
        self.tableView.register(UINib(nibName: "GroupListTblCell", bundle: Bundle.main), forCellReuseIdentifier: "GroupListTblCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if members.count > 0 {
            self.tableView.backgroundView = nil
            return members.count
        } else {
            self.tableView.showBackgroundMsg(msg: "No Members Found")
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "GroupListTblCell", for: indexPath) as! GroupListTblCell
        cell.optionBtn.isHidden = true
        cell.optionIV.isHidden = true
        cell.profileWidthConstraints.constant = 50.0
        cell.user = self.members[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
}
// MARK: - APIs
extension GroupInformationVC {
    private func executeAPIMyGroupList(groupId: String) {
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.groupInformation(groupId: groupId) { (serverData, errorMessage, errorCode) in
            self.hideActivityIndicator(uiView: self.view)
            DispatchQueue.main.async {
                if let serverData = serverData {
                    if let members = serverData["members"] as? [[String: Any]] {
                        self.members = members.map({ return User(jsonData: $0) })
                    }
                    self.noOfMemberLbl.text = "\(self.members.count)"
                    self.tableView.reloadData()
                }
            }
        }
    }
    private func executeAPILeaveGroup() {
        
        var params = [String: Any]()
        params["group_id"] = self.groupId
        params["member_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"
        
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.groupLeave(params: params) { (isSuccess, errorMessage, errorCode) in
            self.hideActivityIndicator(uiView: self.view)
            DispatchQueue.main.async {
                if isSuccess {
                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Group leave successfully")
                    for controller: UIViewController in (self.navigationController?.viewControllers)! {
                        if controller.isKind(of: AKSideMenu.self) {
                            self.navigationController?.popToViewController(controller, animated: true)
                            return
                        }
                    }
                }
            }
        }
    }
    private func executeAPIDeleteGroup() {
        
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.groupDelete(groupId: self.groupId) { (isSuccess, errorMessage, errorCode) in
            self.hideActivityIndicator(uiView: self.view)
            DispatchQueue.main.async {
                if isSuccess {
                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                    for controller: UIViewController in (self.navigationController?.viewControllers)! {
                        if controller.isKind(of: AKSideMenu.self) {
                            self.navigationController?.popToViewController(controller, animated: true)
                            return
                        }
                    }
                }
            }
        }
    }
}
