//
//  OptionCellVC.swift
//  HeyHang
//
//  Created by Mohd Arsad on 18/06/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

protocol OptionCellDelegate {
    func onOptionSelected(positionTag: Int, index: Int)
}
class OptionCellVC: UIViewController {
    
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var firstBtn: UIButton!
    @IBOutlet weak var secondBtn: UIButton!
    @IBOutlet weak var thirdBtn: UIButton!
    
    @IBOutlet weak var popupTopConstraints: NSLayoutConstraint!
    @IBOutlet weak var thirdBtnHeightConstraints: NSLayoutConstraint!

    var topConstraints: CGFloat = 100.0
    var delegate: OptionCellDelegate?
    var titleArr = [String]()
    var tag: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.popupTopConstraints.constant = topConstraints + 10
        if titleArr.count > 0 {
            self.firstBtn.setTitle(titleArr.first ?? "", for: .normal)
        }
        if titleArr.count > 1 {
            self.secondBtn.setTitle(titleArr[1], for: .normal)
        }
        if titleArr.count > 2 {
            self.thirdBtn.setTitle(titleArr[2], for: .normal)
            self.thirdBtnHeightConstraints.constant = 30 * widthRatio
            self.thirdBtn.isHidden = false
        } else {
            self.thirdBtnHeightConstraints.constant = 0
            self.thirdBtn.isHidden = true
        }
    }
    override func viewWillLayoutSubviews() {
        self.popUpView.layer.cornerRadius = 10.0
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    @IBAction func onClickFirstBtn(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.onOptionSelected(positionTag: self.tag, index: 0)
        }
    }
    @IBAction func onClickSecondBtn(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.onOptionSelected(positionTag: self.tag, index: 1)
        }
    }
    @IBAction func onClickThirdBtn(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.onOptionSelected(positionTag: self.tag, index: 2)
        }
    }
}
