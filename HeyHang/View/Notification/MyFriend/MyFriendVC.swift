//
//  MyFriendVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 5/28/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

class MyFriendVC: UIViewController {
    
    enum Navigation {
        case myFriend
        case myGroup
        case myChat
    }
    
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var searchViewWidthConstraints: NSLayoutConstraint!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var hdrLbl: UILabel!
    @IBOutlet weak var tableView: MATableView!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var createGroupBtn: UIButton!
    @IBOutlet weak var createGroupWidthConstraints: NSLayoutConstraint! //35.0
    
    var navigation: Navigation = .myFriend
    private var isLoading: Bool = false
    
    private var groupMainList: [Group] = []
    private var groupList: [Group] = []
    private var friendMainList: [User] = []
    private var friendList: [User] = []
    
    private var chatListMain: [[String: Any]] = []
    private var chatList: [[String: Any]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("ChatNotificationName"), object: nil)
        initialization()
    }
    override func viewWillLayoutSubviews() {
        self.headerView.setSideDropShadow(isUp: false)
        self.searchView.layer.cornerRadius = 5.0
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        currentChatId = "ChatList"
        if checkInternet() {
            if navigation == .myGroup {
                self.executeAPIMyGroupList()
            } else if navigation == .myChat {
                self.executeAPIMyChatList(isShow: true)
            } else {
                self.executeAPIForGetFriendList()
            }
        }
    }
    @objc func methodOfReceivedNotification(notification: Notification) {
        if checkInternet() {
            if navigation == .myGroup {
                isLoading = true
                self.tableView.reloadData()
                self.executeAPIMyGroupList()
            } else if navigation == .myChat {
                isLoading = true
                self.tableView.reloadData()
                self.executeAPIMyChatList(isShow: false)
            } else {
                isLoading = true
                self.tableView.reloadData()
                self.executeAPIForGetFriendList()
            }
        }
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        currentChatId = ""
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickSearchBtn(_ sender: Any) {
        if self.searchViewWidthConstraints.constant == 0.0 {
            showSearchBar()
        } else {
            hideSearchBar()
        }
    }
    @IBAction func onClickCreateGroupBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateGroupVC") as! CreateGroupVC
        vc.navigation = .myGroup
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension MyFriendVC : UITextFieldDelegate {
    
    private func setupTextField() {
        self.searchTF.delegate = self
        self.searchTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    @objc func textFieldDidChange(_ theTextField: UITextField) {
        
        switch navigation {
        case .myGroup:
            if theTextField.text == "" {
                self.groupList = self.groupMainList
                self.tableView.reloadData()
                return
            }
            let filterArr = self.groupMainList.filter({ return (($0.name ?? "").lowercased().contains((theTextField.text ?? "").lowercased())) })
            self.groupList = filterArr
            self.tableView.reloadData()
            break
        case .myChat:
            if theTextField.text == "" {
                self.chatList = self.chatListMain
                self.tableView.reloadData()
                return
            }
            
            var tempArr = [[String: Any]]()
            for object in self.chatListMain {
                if object.keys.contains("event_id") { //Its identify the group
                    let innerObj = ChatGroup(json: object)
                    if (innerObj.groupName ?? "").lowercased().contains((theTextField.text ?? "").lowercased()) {
                        tempArr.append(object)
                    }
                } else {
                    let innerObj = ChatUser(json: object)
                    if (innerObj.fullName ?? "").lowercased().contains((theTextField.text ?? "").lowercased()) {
                        tempArr.append(object)
                    }
                }
            }
            self.chatList = tempArr
            self.tableView.reloadData()
            break
        case .myFriend:
            if theTextField.text == "" {
                self.friendList = self.friendMainList
                self.tableView.reloadData()
                return
            }
            let filterArr = self.friendMainList.filter({ return (($0.fullName ?? "").lowercased().contains((theTextField.text ?? "").lowercased())) })
            self.friendList = filterArr
            self.tableView.reloadData()
            break
        }
    }
}
// MARK: - Helper Methods
extension MyFriendVC {
    private func initialization() {
        self.searchViewWidthConstraints.constant = 0
        self.searchTF.isHidden = true
        setupTextField()
        if navigation == .myGroup {
            self.hdrLbl.text = "My Group"
            self.createGroupWidthConstraints.constant = 35.0
            self.createGroupBtn.isHidden = false
        } else if navigation == .myChat {
            self.hdrLbl.text = "Chat"
            self.createGroupWidthConstraints.constant = 0.0
            self.createGroupBtn.isHidden = true
        } else {
            self.hdrLbl.text = "My Friends"
            self.createGroupWidthConstraints.constant = 0.0
            self.createGroupBtn.isHidden = true
        }
        setupTableView()
    }
    //Search Show/Hide Methods  for chat list and friend list
    private func showSearchBar() {
        UIView.animate(withDuration: 0.5) {
            self.searchTF.isHidden = false
            self.searchViewWidthConstraints.constant = UIScreen.main.bounds.width - (self.navigation == .myGroup ? 120 : 100)
            self.view.layoutIfNeeded()
            self.searchBtn.setImage(UIImage.init(named: "crossWhite"), for: .normal)
        }
    }
    private func hideSearchBar() {
        UIView.animate(withDuration: 0.5) {
            self.view.endEditing(true)
            self.searchTF.isHidden = true
            self.searchViewWidthConstraints.constant = 0.0
            self.view.layoutIfNeeded()
            self.searchBtn.setImage(UIImage.init(named: "search_white"), for: .normal)
            
            self.searchTF.text = ""
            self.friendList = self.friendMainList
            self.groupList = self.groupMainList
            self.chatList = self.chatListMain
            self.tableView.reloadData()
        }
    }
}
// MARK: - UITableViewDelegate, UITableViewDataSource, UITableViewPullRefreshDelegate
extension MyFriendVC: UITableViewDelegate, UITableViewDataSource, UITableViewPullRefreshDelegate {
    
    fileprivate func setupTableView() {
        self.tableView.register(UINib(nibName: "GroupListTblCell", bundle: Bundle.main), forCellReuseIdentifier: "GroupListTblCell")
        self.tableView.pullToRefreshDelegate = self
    }
    
    func tableView(_ tableView: UITableView, pulltoRefreshStart: Bool) {
        self.groupMainList = []
        self.groupList = []
        self.friendMainList = []
        self.friendList = []
        self.chatListMain = []
        self.chatList = []
        isLoading = true
        self.tableView.reloadData()
        
        if checkInternet() {
            switch navigation {
            case .myGroup:
                self.executeAPIMyGroupList()
                break
            case .myChat:
                self.executeAPIMyChatList(isShow: true)
                break
            case .myFriend:
                self.executeAPIForGetFriendList()
                break
            }
        } else {
            isLoading = false
            self.tableView.reloadData()
            self.tableView.pullToRefreshControl?.endRefreshing()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if navigation == .myGroup {
            if groupList.count > 0 {
                self.tableView.backgroundView = nil
                return groupList.count
            } else {
                self.tableView.showBackgroundMsg(msg: isLoading ? "Loading..." : "No Group List")
                return 0
            }
        } else if navigation == .myChat {
            if chatList.count > 0 {
                self.tableView.backgroundView = nil
                return chatList.count
            } else {
                self.tableView.showBackgroundMsg(msg: isLoading ? "Loading..." : "No Chat List")
                return 0
            }
        } else {
            if friendList.count > 0 {
                self.tableView.backgroundView = nil
                return friendList.count
            } else {
                self.tableView.showBackgroundMsg(msg: isLoading ? "Loading..." : "No Friends Found")
                return 0
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "GroupListTblCell", for: indexPath) as! GroupListTblCell
        
        switch navigation {
        case .myGroup:
            cell.profileWidthConstraints.constant = 50.0
            cell.group = self.groupList[indexPath.row]
            cell.optionIV.isHidden = true
            cell.optionBtn.isHidden = true
        case .myChat:
            cell.profileWidthConstraints.constant = 50.0
            let object = chatList[indexPath.row]
            if object.keys.contains("event_id") { //Its identify the group
                cell.chat = ChatGroup(json: object)
            } else {
                cell.chatU = ChatUser(json: object)
            }
            cell.optionIV.isHidden = true
            cell.optionBtn.isHidden = true
        case .myFriend:
            cell.profileWidthConstraints.constant = 50.0
            cell.user = self.friendList[indexPath.row]
        }
        cell.detailBtn.tag = indexPath.row
        cell.detailBtn.addTarget(self, action: #selector(onClickDetailBtn(_:)), for: .touchUpInside)
        cell.optionBtn.tag = indexPath.row
        cell.optionBtn.addTarget(self, action: #selector(showOptionBtns(_:)), for: .touchUpInside)
        cell.optionBtn.isUserInteractionEnabled = true
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    @objc func onClickDetailBtn(_ sender: UIButton) {
        switch navigation {
        case .myGroup:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
            let object = groupList[sender.tag]
            vc.isMoveForGroup = true
            vc.hdrTitle = object.name
            vc.chatId = object.id
            vc.isAdmin = object.isAdmin
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case .myChat:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
            let object = chatList[sender.tag]
            if object.keys.contains("event_id") { //Its identify the group
                let gObj = ChatGroup(json: object)
                vc.isMoveForGroup = true
                vc.hdrTitle = gObj.groupName
                vc.chatId = gObj.id
                vc.isAdmin = gObj.isAdmin
            } else {
                let uObj = ChatUser(json: object)
                vc.isMoveForGroup = false
                vc.hdrTitle = uObj.fullName
                vc.chatId = uObj.id
                vc.opponentId = uObj.userId
                vc.requestId = uObj.requestId
                vc.isMoveForGroup = false
            }
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case .myFriend:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
            let object = friendList[sender.tag]
            vc.hdrTitle = object.fullName ?? ""
            vc.opponentId = object.userId ?? "0"
            vc.chatId = object.chatId ?? "0"
            vc.requestId = object.requestId
            vc.isMoveForGroup = false
            self.navigationController?.pushViewController(vc, animated: true)
            break
        }
    }
    @objc func showOptionBtns(_ sender: UIButton) {
        var ypos: CGFloat = 100.0
        if let frame = sender.superview?.convert(sender.frame, to: self.view) {
            ypos = frame.origin.y
            if (ypos + 100) > UIScreen.main.bounds.height {
                ypos = UIScreen.main.bounds.height - 150
            }
        }
        
        switch navigation {
        case .myGroup:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OptionCellVC") as! OptionCellVC
            vc.modalPresentationStyle = .overCurrentContext
            vc.titleArr = ["Report", "Delete Chat"]
            vc.delegate = self
            vc.topConstraints = ypos
            vc.tag = sender.tag
            self.present(vc, animated: false, completion: nil)
            break
        case .myChat:
            break
        case .myFriend:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OptionCellVC") as! OptionCellVC
            vc.modalPresentationStyle = .overCurrentContext
            vc.titleArr = ["Unfriend", "Report", "Delete Chat"]
            vc.delegate = self
            vc.topConstraints = ypos
            vc.tag = sender.tag
            self.present(vc, animated: false, completion: nil)
            break
        }
    }
}
// MARK: - OptionCellDelegate For getting the action of option button
extension MyFriendVC: OptionCellDelegate {
    func onOptionSelected(positionTag: Int, index: Int) {
        //Option menu actions
        switch navigation {
        case .myGroup:
            //["Report", "Delete Chat"]
            if index == 0 {
                let object = groupList[positionTag]
                if checkInternet() {
                    self.executeAPIForReportUserGroup(isGroup: true, idStr: object.id ?? "0")
                }
            } else {
                print("Delete Chat: \(positionTag)")
                let object = groupList[positionTag]
                if checkInternet() {
                    self.executeAPIForDeleteChatGroup(isGroup: true, idStr: object.id ?? "0")
                }
            }
            break
        case .myChat:
            break
        case .myFriend:
            //["Unfriend", "Report", "Delete Chat"]
            if index == 0 {
                let object = friendList[positionTag]
                if checkInternet() {
                    if let reqID = object.requestId {
                        self.unFriendConfirmAlert(id: reqID, name: object.fullName ?? "User")
                    }
                }
            } else if index == 1 {
                let object = friendList[positionTag]
                if checkInternet() {
                    self.executeAPIForReportUserGroup(isGroup: false, idStr: object.chatId ?? "0")
                }
            } else {
                print("Delete Chat: \(positionTag)")
                let object = friendList[positionTag]
                if checkInternet() {
                    self.executeAPIForDeleteChatGroup(isGroup: false, idStr: object.chatId ?? "0")
                }
            }
            break
        }
    }
}

// MARK: - APIs
extension MyFriendVC {
    private func unFriendConfirmAlert(id: String, name: String) {
        let alertController = UIAlertController.init(title: Constant.APP_NAME, message: "Are you sure want to unfriend \(name)?", preferredStyle: .alert)
        alertController.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
        }))
        alertController.addAction(UIAlertAction.init(title: "Confirm", style: .destructive, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
            self.executeAPIForUnfriend(requestId: id)
        }))
        self.present(alertController, animated: true, completion: nil)
    }
    func executeAPIForUnfriend(requestId: String) {
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.unfriendUser(requestId: requestId) {  (isSuccess, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                if isSuccess {
                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                    self.executeAPIForGetFriendList()
                } else {
                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                }
            }
        }
    }
    func executeAPIForReportUserGroup(isGroup: Bool, idStr: String) {
        var params = [String: Any]()
        params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"
        params["reported_id"] = idStr
        params["type"] = isGroup ? "2" : "1"
        
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.reportUserGroup(params: params) {  (isSuccess, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
            }
        }
    }
    func executeAPIForDeleteChatGroup(isGroup: Bool, idStr: String) {
        /*
         //request key->c_id,user_id
         */
        var params = [String: Any]()
        params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"
        if isGroup {
            params["group_id"] = idStr
        } else {
            params["c_id"] = idStr
        }
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.deleteChatGroup(isGroup: isGroup, params: params) {  (isSuccess, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                self.executeAPIMyChatList(isShow: true)
            }
        }
    }
    private func executeAPIMyGroupList() {
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.getMyGroupList { (groups, errorMessage, errorCode) in
            self.isLoading = false
            self.tableView.reloadData()
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                self.tableView.pullToRefreshControl?.endRefreshing()
                if errorCode == 100 {
                    if let groups = groups {
                        self.groupList = groups
                        self.groupMainList = groups
                        self.tableView.reloadData()
                    }
                } else {
                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                }
            }
        }
    }
    private func executeAPIForGetFriendList() {
        //'user_id','event_privacy'
        var params = [String: Any]()
        params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? ""
        params["event_privacy"] = "2"
        params["event_id"] = ""
        
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.userList(params: params) { (users, errorMessage, errorCode) in
            self.isLoading = false
            self.tableView.reloadData()
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                self.tableView.pullToRefreshControl?.endRefreshing()
                if let userArr = users {
                    self.friendList = userArr
                    self.friendMainList = userArr
                } else {
                    self.friendList = []
                    self.friendMainList = []
                }
                self.tableView.reloadData()
            }
        }
    }
    private func executeAPIMyChatList(isShow: Bool) {
        if isShow {
            self.showActivityIndicator(uiView: self.view)
        }
        RestAPI.shared.getMyChatList { (serverData, errorMessage, errorCode) in
            self.isLoading = false
            self.tableView.reloadData()
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                self.tableView.pullToRefreshControl?.endRefreshing()
                if errorCode == 100 {
                    if let serverData = serverData {
                        var tempArr = [[String: Any]]()
                        if let groupChat = serverData["group_chat"] as? [[String: Any]] {
                            tempArr = groupChat
                        }
                        if let friendChat = serverData["friend_chat"] as? [[String: Any]] {
                            for object in friendChat {
                                tempArr.append(object)
                            }
                        }
                        self.chatListMain = self.getSortedArr(array: tempArr)
                        self.chatList = self.chatListMain
                        self.tableView.reloadData()
                    }
                } else {
//                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                }
            }
        }
    }
    private func getSortedArr(array: [[String: Any]]) -> [[String: Any]] {
        let sorted = array.sorted { (object1, object2) -> Bool in
            let firstTime = TimeInterval(object1["updated_at"] as? String ?? "") ?? 0.0
            let secondTime = TimeInterval(object2["updated_at"] as? String ?? "") ?? 0.0
            return firstTime > secondTime
        }
        return sorted
    }
}
