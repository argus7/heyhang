//
//  NotificationVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 5/30/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit


class NotificationTblCell: UITableViewCell {
    
    @IBOutlet weak var logoIV: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var seperaterLbl: UILabel!
    
}

class NotificationVC: UIViewController {
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.isScrollEnabled = false
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }
    override func viewWillLayoutSubviews() {
        self.cardView.layer.cornerRadius = 15.0
        self.headerView.setSideDropShadow(isUp: false)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func onClickBakcBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickSettingBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationSettingVC") as! NotificationSettingVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension NotificationVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! NotificationTblCell
        if indexPath.row == 4 {
            cell.seperaterLbl.isHidden = true
        } else {
            cell.seperaterLbl.isHidden = false
        }
        cell.seperaterLbl.backgroundColor = UIColor.lightGray.withAlphaComponent(0.4)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
}
