//
//  TutorialVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 5/27/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

class TutorialCollCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    
}

class TutorialVC: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: MAPageControl!
    @IBOutlet weak var skipBtn: UIButton!
    
    let imageArr = [UIImage(named: "tuto01.png"),UIImage(named: "tuto02.png"),UIImage(named: "tuto03.png")]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.pageControl.numberOfPages = 3
        
    }
    override func viewWillLayoutSubviews() {
        self.skipBtn.setCircleCorner()
    }
    override func viewDidAppear(_ animated: Bool) {
//        self.pageControl.setDot()
    }
    @IBAction func onClickSkipBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "StartedVC") as! StartedVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension TutorialVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! TutorialCollCell
        cell.imgView.image = imageArr[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize.init(width: UIScreen.main.bounds.width, height: self.collectionView.bounds.height)
        return size
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let xpos = scrollView.contentOffset.x / self.collectionView.bounds.width
        self.pageControl.currentPage = Int(xpos)
        if xpos == 2 {
            self.skipBtn.setTitle("Get Started", for: .normal)
            UIView.animate(withDuration: 0.4, animations: {
            }) { (isTrue) in
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "StartedVC") as! StartedVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            self.skipBtn.setTitle("Skip", for: .normal)
        }
    }
}
