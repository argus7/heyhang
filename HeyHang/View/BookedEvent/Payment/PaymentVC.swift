//
//  PaymentVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 6/6/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit
import Stripe

class PaymentVC: UIViewController {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var hdrLbl: UILabel!
    
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var numOfTicketsLbl: UILabel!
    @IBOutlet weak var dollerLblOutlet: UILabel!
    @IBOutlet weak var payNowBtnOutlet: UIButton!
    @IBOutlet weak var termsIV: UIImageView!
    @IBOutlet weak var termFixedLbl: UILabel!
    
    private var isTerms: Bool = false
    
    var selectedEvent: Event?
    var ticketCount: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.termFixedLbl.text = "By pressing you accept the terms & conditions of payment & purchases through the app"
        self.termFixedLbl.underline(newText: "terms & conditions")
        self.numOfTicketsLbl.text = "\(ticketCount)"
        
        guard let event = self.selectedEvent else { return }
        let price = Int(event.ticketPrice ?? "0") ?? 0
        self.dollerLblOutlet.text = "\(Constant.Currency)\(price*ticketCount)"
        
    }
    override func viewWillLayoutSubviews() {
        self.headerView.setSideDropShadow(isUp: false)
        payNowBtnOutlet.layer.cornerRadius = 5.0
        payNowBtnOutlet.clipsToBounds = true
        self.detailView.setLightDropShadow(cornerRadius: 10.0)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickTermsBtn(_ sender: Any) {
        self.isTerms = !self.isTerms
        if isTerms {
            self.termsIV.image = UIImage.init(named: "check_active")
        } else {
            self.termsIV.image = UIImage.init(named: "check_normal")
        }
    }
    @IBAction func onClickTermsConditionBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyTermVC") as! PrivacyTermVC
        vc.navigation = .terms
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func payNowBtnAction(_ sender: UIButton) {
        if isTerms {
            if checkInternet() {
                self.handleAddPaymentOptionButtonTapped()
            }
        } else {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please accept terms and conditions")
        }
    }
}
extension PaymentVC: STPAddCardViewControllerDelegate {
    func handleAddPaymentOptionButtonTapped() {
        // Setup add card view controller
        let addCardViewController = STPAddCardViewController()
        addCardViewController.delegate = self
        addCardViewController.title = "Payment"
        
        // Present add card view controller
        let navigationController = UINavigationController(rootViewController: addCardViewController)
        present(navigationController, animated: true)
    }
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        // Dismiss add card view controller
        addCardViewController.dismiss(animated: true, completion: nil)
        dismiss(animated: true)
    }
    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: @escaping STPErrorBlock) {
        addCardViewController.dismiss(animated: true, completion: nil)
        print("Token: \(token)")
        self.executeAPIForCreateTicket(token: "\(token)")
    }
    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateSource source: STPSource, completion: @escaping STPErrorBlock) {
        addCardViewController.dismiss(animated: true, completion: nil)
    }
}
// MARK: - APIs
extension PaymentVC {
    
    /*
     Required keys=>
     event_type 1=paid 'user_id','event_id','event_type','ticket_count','total_amount'
     event_type 2=free 'user_id','event_id','event_type'
     */
    
    private func executeAPIForCreateTicket(token: String) {
        if let event = self.selectedEvent {
            
            let price = Int(event.ticketPrice ?? "0") ?? 0
            
            var params = [String: Any]()
            params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"
            params["event_id"] = event.eventId ?? "0"
            params["event_type"] = (event.type == .paid) ? "1" : "2"
            params["ticket_count"] = "\(self.ticketCount)"
            params["total_amount"] = "\(price*ticketCount)"
            params["stripeToken"] = token
            print(params)
            
            self.showActivityIndicator(uiView: self.view)
            RestAPI.shared.bookTicket(params: params) { (isSuccess, errorMessage, errorCode) in
                DispatchQueue.main.async {
                    self.hideActivityIndicator(uiView: self.view)
                    if isSuccess {
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PaymentSuccessfullVC") as! PaymentSuccessfullVC
                        vc.totalCost = "\(Constant.Currency)\(price*self.ticketCount)"
                        self.navigationController?.pushViewController(vc, animated: true)
                    } else {
                        self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                    }
                }
            }
        } else {
            print("No Event Found")
        }
    }
}
