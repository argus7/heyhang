//
//  BookTicketsVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 6/6/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

class BookTicketCollectionCell: UICollectionViewCell {
    @IBOutlet weak var num1LblOutlet: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
            num1LblOutlet.layer.cornerRadius = num1LblOutlet.frame.size.width/2
            num1LblOutlet.clipsToBounds = true
        }
    }

class BookTicketsVC: UIViewController {
    
    @IBOutlet weak var collectionViewOutlet: UICollectionView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var hdrLbl: UILabel!
    @IBOutlet weak var numOfTicketView: UIView!
    @IBOutlet weak var continueBtnOutlet: UIButton!
    
    @IBOutlet weak var eventNameLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var startDateTimeLbl: UILabel!
    @IBOutlet weak var endDateTimeLbl: UILabel!
    @IBOutlet weak var availableTicketLbl: UILabel!
    @IBOutlet weak var ticketPriceLbl: UILabel!
    
    private var ticketCounts: [Int] = []
    private var selectedTicket: Int = 1
    
    var selectedEvent: Event?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        updateEventDetails()
        
    }
    override func viewWillLayoutSubviews() {
        self.headerView.setSideDropShadow(isUp: false)
        continueBtnOutlet.layer.cornerRadius = 5.0
        numOfTicketView.layer.cornerRadius = 5.0
        numOfTicketView.clipsToBounds = true
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func continueBtnAction(_ sender: UIButton) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
        vc.selectedEvent = self.selectedEvent
        vc.ticketCount = self.selectedTicket
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
// MARK: - Selected Details Update
extension BookTicketsVC {
    private func updateEventDetails() {
        if let event = self.selectedEvent {
            ticketCounts = []
            for index in 1...event.seatAvailable {
                ticketCounts.append(index)
            }
            self.eventNameLbl.text = event.name ?? ""
            self.selectedTicket = ticketCounts.first ?? 1
            self.collectionViewOutlet.reloadData()
            
            self.locationLbl.text = event.location
            self.availableTicketLbl.text = "Available tickets \(event.seatAvailable)"
            self.ticketPriceLbl.text = "\(Constant.Currency)\(event.ticketPrice ?? "0")"
//            self.startDateTimeLbl.text = event.startDateTime
//            self.endDateTimeLbl.text = event.endDateTime
            
            let startDate = Date(timeIntervalSince1970: event.startDateTimeInterval ?? 0.0)
            let endDate = Date(timeIntervalSince1970: event.endDateTimeInterval ?? 0.0)
            let formatter = DateFormatter()
            formatter.dateFormat = "dd MMM | hh:mm a"
            let startDateTime = formatter.string(from: startDate)
            let endDateTime = formatter.string(from: endDate)
            
            self.startDateTimeLbl.text = startDateTime
            self.endDateTimeLbl.text = endDateTime
        }
    }
}
// MARK: - UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension BookTicketsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ticketCounts.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! BookTicketCollectionCell
        cell.num1LblOutlet.setCircleCorner()
        cell.num1LblOutlet.text = "\(ticketCounts[indexPath.row])"
        if selectedTicket == ticketCounts[indexPath.row] {
            cell.num1LblOutlet.backgroundColor = UIColor.purpleSelected
            cell.num1LblOutlet.textColor = UIColor.white
        } else {
            cell.num1LblOutlet.backgroundColor = UIColor.white
            cell.num1LblOutlet.textColor = UIColor.lightGray.withAlphaComponent(0.7)
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedTicket = ticketCounts[indexPath.row]
        
        if let event = self.selectedEvent {
            let price = Int(event.ticketPrice ?? "0") ?? 0
            self.ticketPriceLbl.text = "\(Constant.Currency)\(price * self.selectedTicket)"
        }
        
        self.collectionViewOutlet.reloadData()
    }
}
