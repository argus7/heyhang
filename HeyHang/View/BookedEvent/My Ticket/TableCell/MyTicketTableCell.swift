//
//  MyTicketTableCell.swift
//  HeyHang
//
//  Created by Anup Kumar on 6/11/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

class MyTicketTableCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var liveConcertImgView: UIImageView!
    @IBOutlet weak var eventNameLbl: UILabel!
    @IBOutlet weak var locationImgView: UIImageView!
    @IBOutlet weak var calenderImgView: UIImageView!
    @IBOutlet weak var clockImgView: UIImageView!
    @IBOutlet weak var paidEventLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var startDateTimeLbl: UILabel!
    @IBOutlet weak var endDateTimeLbl: UILabel!
    @IBOutlet weak var forAdultLbl: UILabel! //For 1 Adult
    @IBOutlet weak var totleAmountLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var circleLeftLbl: UILabel!
    @IBOutlet weak var circleRtLbl: UILabel!
    @IBOutlet weak var dottedLineLbl: UILabel!
    
    var ticket: Ticket? {
        didSet {
            updateDetails()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        circleLeftLbl.layer.cornerRadius = circleLeftLbl.frame.size.width/2
        circleLeftLbl.clipsToBounds = true
        circleRtLbl.layer.cornerRadius = circleRtLbl.frame.size.width/2
        circleRtLbl.clipsToBounds = true
        
        cardView.layer.cornerRadius = 10
        cardView.clipsToBounds = true
        paidEventLbl.layer.cornerRadius = 5
        paidEventLbl.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    private func updateDetails() {
        if let ticketT = ticket {
            if ticketT.type == .paid { //Paid
                self.paidEventLbl.text = "Paid Hang"
                self.paidEventLbl.backgroundColor = UIColor.eventTypeYellow
            } //Free
            else {
                self.paidEventLbl.text = "Free Hang"
                self.paidEventLbl.backgroundColor = UIColor.eventTypePurple
            }
            
            self.eventNameLbl.text = ticketT.name ?? ""
            self.addressLbl.text = ticketT.location ?? ""
            self.startDateTimeLbl.text = ticketT.startDateTime
            self.endDateTimeLbl.text = ticketT.endDateTime
            self.forAdultLbl.text = "For \(ticketT.seatAvailable) Person"
            self.priceLbl.text = "\(Constant.Currency) \(ticketT.totalAmount ?? "0.0")"
            
            let image = ticketT.imageArr.last?.url ?? ""
            self.liveConcertImgView.imageFromServerURL(image, placeHolder: nil)
            
        }
    }
}
