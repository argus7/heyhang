//
//  MyTicketVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 6/7/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit
import AKSideMenu

class MyTicketVC: UIViewController {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var hdrLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    private var ticketArr: [Ticket] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupTableView()
    }
    override func viewWillLayoutSubviews() {
        self.headerView.setSideDropShadow(isUp: false)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        if checkInternet() {
            self.executeAPIForTicketList()
        }
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        for controller: UIViewController in (self.navigationController?.viewControllers)! {
            if controller.isKind(of: AKSideMenu.self) {
                self.navigationController?.popToViewController(controller, animated: true)
                return
            }
        }
        self.navigationController?.popViewController(animated: true)
    }
}
extension MyTicketVC: UITableViewDelegate, UITableViewDataSource {
    fileprivate func setupTableView() {
        self.tableView.register(UINib(nibName: "MyTicketTableCell", bundle: Bundle.main), forCellReuseIdentifier: "MyTicketTableCell")
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if ticketArr.count > 0 {
            self.tableView.backgroundView = nil
            return ticketArr.count
        } else {
            self.tableView.showBackgroundMsg(msg: "No Tickets List")
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "MyTicketTableCell", for: indexPath) as! MyTicketTableCell
        
        var frame = cell.dottedLineLbl.frame
        frame.size.width = (UIScreen.main.bounds.width - 30.0)
        cell.dottedLineLbl.frame = frame
        cell.dottedLineLbl.setDottedtLine()
        cell.dottedLineLbl.backgroundColor = UIColor.clear
        
        cell.ticket = ticketArr[indexPath.row]
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 285.0
    }
}

// MARK: - APIs Data
extension MyTicketVC {
    private func executeAPIForTicketList() {
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.ticketList {  (tickets, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                if errorCode != 100 {
                } else {
                    if let ticketArr = tickets {
                        self.ticketArr = ticketArr
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
}
