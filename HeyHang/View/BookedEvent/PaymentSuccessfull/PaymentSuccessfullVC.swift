//
//  PaymentSuccessfullVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 6/6/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

class PaymentSuccessfullVC: UIViewController {
    
    enum ScreenType {
        case payment
        case seat
    }
    
    @IBOutlet weak var successfullMsgLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var viewMyTicketOutlet: UIButton!
    
    var type: ScreenType = .payment
    var totalCost: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if type == .seat {
            self.priceLbl.isHidden = true
            self.successfullMsgLbl.text = "Seat Book Successfully"
        } else {
            self.priceLbl.text = totalCost
        }
        viewMyTicketOutlet.setAttributedTitle(viewMyTicketOutlet.getUnderlineAttributeString(UIColor.white), for: .normal)
        viewMyTicketOutlet.setTitleColor(UIColor.white, for: .normal)
    }
    
    @IBAction func viewMyTicketAction(_ sender: UIButton) {
        if type == .seat {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookedEventVC") as! BookedEventVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyTicketVC") as! MyTicketVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
