//
//  BookedEventVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 5/28/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit
import AKSideMenu

class BookedEventVC: UIViewController {

    @IBOutlet weak var hdrLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var attendedEventBtn: UIButton!
    @IBOutlet weak var upcomingEventBtn: UIButton!
    @IBOutlet weak var activeLbl: UILabel!
    @IBOutlet weak var activeLblLeadingConstraints: NSLayoutConstraint!
    @IBOutlet weak var activeHeaderView: UIView!
    
    private var isUpcomingActive: Bool = false
    private var attendedEvent: [Event] = []
    private var upcomingEvent: [Event] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupTableView()
        setActiveIndex(isUpcoming: isUpcomingActive)
        
    }
    override func viewWillLayoutSubviews() {
        self.activeHeaderView.setSideDropShadow(isUp: false)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        if checkInternet() {
            executeAPIForBookedEventList()
        }
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        for controller: UIViewController in (self.navigationController?.viewControllers)! {
            if controller.isKind(of: AKSideMenu.self) {
                self.navigationController?.popToViewController(controller, animated: true)
                return
            }
        }
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickAttendedEventBtn(_ sender: Any) {
        setActiveIndex(isUpcoming: false)
    }
    @IBAction func onClickUpcomingEventBtn(_ sender: Any) {
        setActiveIndex(isUpcoming: true)
    }
}
// MARK: - Helping Methods
extension BookedEventVC {
    private func setActiveIndex(isUpcoming: Bool) {
        self.isUpcomingActive = isUpcoming
        if isUpcoming {
            self.upcomingEventBtn.alpha = 1.0
            self.attendedEventBtn.alpha = 0.6
            UIView.animate(withDuration: 0.3) {
                self.activeLblLeadingConstraints.constant = (UIScreen.main.bounds.width / 2)
                self.view.layoutIfNeeded()
            }
        } else {
            self.upcomingEventBtn.alpha = 0.6
            self.attendedEventBtn.alpha = 1.0
            UIView.animate(withDuration: 0.3) {
                self.activeLblLeadingConstraints.constant = 0
                self.view.layoutIfNeeded()
            }
        }
        self.tableView.reloadData()
    }
}
extension BookedEventVC: UITableViewDelegate, UITableViewDataSource {
    fileprivate func setupTableView() {
        self.tableView.register(UINib(nibName: "EventTblCell", bundle: Bundle.main), forCellReuseIdentifier: "EventTblCell")
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isUpcomingActive {
            if upcomingEvent.count > 0 {
                self.tableView.backgroundView = nil
                return upcomingEvent.count
            } else {
                self.tableView.showBackgroundMsg(msg: "No Upcoming Hang")
                return 0
            }
        } else {
            if attendedEvent.count > 0 {
                self.tableView.backgroundView = nil
                return attendedEvent.count
            } else {
                self.tableView.showBackgroundMsg(msg: "No Past Hang")
                return 0
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "EventTblCell", for: indexPath) as! EventTblCell
        
        if isUpcomingActive {
            cell.eventTypeView.isHidden = false
            cell.event = upcomingEvent[indexPath.row]
        } else {
            cell.eventTypeView.isHidden = false
            cell.event = attendedEvent[indexPath.row]
        }
        
        if isUpcomingActive {
            cell.timerLbl.isHidden = true
            cell.leftPlaceLbl.isHidden = true
            cell.joinBtn.isHidden = true
            cell.detailBtn.isHidden = false
            cell.distanceLbl.isHidden = true
        } else {
            cell.timerLbl.isHidden = true
            cell.leftPlaceLbl.isHidden = true
            cell.distanceLbl.isHidden = true
            cell.joinBtn.backgroundColor = UIColor.init(red: 0/255.0, green: 212/255.0, blue: 99/255.0, alpha: 1.0)
            if attendedEvent[indexPath.row].isFeedback {
                cell.joinBtn.isHidden = true
            } else {
                cell.joinBtn.isHidden = false
            }
            cell.joinBtn.setTitle("Feedback", for: .normal)
        }
        cell.detailBtn.tag = indexPath.row
        cell.detailBtn.addTarget(self, action: #selector(onClickDetailEventCellBtn(_:)), for: .touchUpInside)
        
        cell.joinBtn.tag = indexPath.row
        cell.joinBtn.addTarget(self, action: #selector(onClickFeedbackEventCellBtn(_:)), for: .touchUpInside)
        
        var frame = cell.gradientView.frame
        frame.size.width = UIScreen.main.bounds.width - 40
        frame.size.height = (250 * widthRatio) - 64.0
        cell.gradientView.frame = frame
        cell.gradientView.setBottomToTopGradient(cornerRadius: 0.0, firstColor: .clear, secoundColor: UIColor(red: 0, green: 0, blue: 0, alpha: 0.8))
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250 * widthRatio
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    @IBAction func onClickDetailEventCellBtn(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailVC") as! EventDetailVC
        vc.navigation = .booked
        vc.selectedEvent = isUpcomingActive ? upcomingEvent[sender.tag] : attendedEvent[sender.tag]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onClickFeedbackEventCellBtn(_ sender: UIButton) {
        if !isUpcomingActive {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "FeedbackVC") as! FeedbackVC
            vc.selectedEvent = attendedEvent[sender.tag]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

// MARK: - APIs Data
extension BookedEventVC {
    private func executeAPIForBookedEventList() {
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.getBookedEventList {  (serverData, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                if errorCode != 100 {
                } else {
                    if let data = serverData {
                        if let attendET = data["attended_event"] as? [[String: Any]] {
                            self.attendedEvent = attendET.map({ return Event(json: $0) })
                        }
                        if let upcomeET = data["upcoming_event"] as? [[String: Any]] {
                            self.upcomingEvent = upcomeET.map({ return Event(json: $0) })
                        }
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
}
