//
//  SideMenuVC.swift
//  Ajjerlli
//
//  Created by Anup Kumar on 5/1/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//  fghfyhfddfu

import UIKit

class SideMenuCell: UITableViewCell {
    
    @IBOutlet weak var logoIV: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    
}

class SideMenuVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var list = [[String:UIImage?]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        list.append(["My Profile" : UIImage(named: "profile")!])
        list.append(["Calendar" : UIImage(named: "calendar_black")!])
        list.append(["Hangs you made" : UIImage(named: "myevent")!])
        list.append(["Booked Hangs" : UIImage(named: "bookedEvent")!])
        list.append(["Tickets" : UIImage(named: "ticket_black")!])
        list.append(["Pals" : UIImage(named: "friend")!])
        list.append(["Pal Requests" : UIImage(named: "friend")!])
//        list.append(["About App" : UIImage(named: "info")!])
        list.append(["Settings" : UIImage(named: "setting")!])
//        list.append(["Term of use" : UIImage(named: "terms")!])
//        list.append(["Privacy Policy" : UIImage(named: "terms")!])
        list.append(["Contact Us" : UIImage(named: "contact_us")!])
    }
}

extension SideMenuVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SideMenuCell
        if let image = list[indexPath.row].values.first {
            cell.logoIV.image = image
        }
        cell.titleLbl.text = list[indexPath.row].keys.first
        if indexPath.row < 7 {
            cell.titleLbl.textColor = UIColor.darkText
        } else {
            cell.titleLbl.textColor = UIColor.grayDeSelected
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.sideMenuViewController?.hideMenuViewController()
        switch indexPath.row {
        case 0: //My profile
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case 1: //Calendar
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CalendarVC") as! CalendarVC
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case 2: //My event
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyEventVC") as! MyEventVC
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case 3: //Booked event
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookedEventVC") as! BookedEventVC
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case 4: //Ticket
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyTicketVC") as! MyTicketVC
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case 5: //Pal
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyFriendVC") as! MyFriendVC
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case 6: //Pal Request
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AllMembersVC") as! AllMembersVC
            vc.navigation = .allRequest
            self.navigationController?.pushViewController(vc, animated: true)
            break
//        case 7: //About Us
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyTermVC") as! PrivacyTermVC
//            vc.navigation = .aboutUs
//            self.navigationController?.pushViewController(vc, animated: true)
//            break
        case 7: //Settings
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
            self.navigationController?.pushViewController(vc, animated: true)
            break
//        case 9: //Terms
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyTermVC") as! PrivacyTermVC
//            vc.navigation = .terms
//            self.navigationController?.pushViewController(vc, animated: true)
//            break
//        case 10: //Privacy Policy
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyTermVC") as! PrivacyTermVC
//            vc.navigation = .privacy
//            self.navigationController?.pushViewController(vc, animated: true)
//            break
        case 8: //Contact Us
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContactUs") as! ContactUs
            self.navigationController?.pushViewController(vc, animated: true)
            break
        default:
            print("Index out of bound")
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45.0
    }
}
