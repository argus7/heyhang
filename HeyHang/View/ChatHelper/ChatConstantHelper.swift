//
//  ChatConstantHelper.swift
//  DXB
//
//  Created by Anup Kumar on 8/30/18.
//  Copyright © 2018 Mohd Arsad. All rights reserved.
//

import Foundation
import UIKit

//MARK: - Constant Using in chat messages and listing
public enum ChatTheme {
    case Border
    case BorderWithCorner
    case CornerEdge
}
struct ChatConstant {
    
    static let rightBubbleBorderColor = UIColor(red: 121/255.0, green: 213/255.0, blue: 139/255.0, alpha: 1.0).cgColor
    static let leftBubbleBorderColor = UIColor(red: 121/255.0, green: 213/255.0, blue: 139/255.0, alpha: 1.0).cgColor
    static let leftBubbleBackColor = UIColor(red: 237/255.0, green: 237/255.0, blue: 237/255.0, alpha: 1.0)
    static let rightBubbleBackColor = UIColor(red: 11/255.0, green: 176/255.0, blue: 134/255.0, alpha: 1.0)
    static let sectionBackgroundColor = UIColor.purpleSelected.withAlphaComponent(0.2)// UIColor(red: 121/255.0, green: 213/255.0, blue: 139/255.0, alpha: 1.0)
    static let bubbleDateTimeFormat = "hh:mm a"
    static let bubbleCornerRadius : CGFloat = 15.0
    static let bubbleImageCornerRadius : CGFloat = 5.0
    static let bubbleBorderWidth : CGFloat = 1.5
    static let sectionTitleFont : UIFont = UIFont.systemFont(ofSize: 12.0)
    static let sectionTitleTextColor = UIColor.purpleSelected
    static let sectionViewWidth : CGFloat = 85.0
    static let sectionViewHeight : CGFloat = 30.0
    
    struct FIRSupportTicketKeys {
        
        static let TicketId = "ticket_id"
        static let Messages = "messages"
        static let Category =  "category"
        static let Subject =  "message"
        static let Description =  "last_reply"
        static let Images =  "images"
        static let Status =  "status"
        static let CreatedBy =  "user_id"
        static let CreatedAt =  "created_at"
        static let ModifiedAt =  "modified_At"
        static let UserDeviceToken = "device_Token"
        
    }
    struct ChatList {
        
        static let Chat_Id  = "chatId"
        static let MessageType  = "msgType"
        static let Picture  = "picture"
        static let Text  = "text"
        static let ProviderId  = "providerId"
        static let ProviderName  = "providerName"
        static let ProviderFCMToken  = "providerFCMToken"
        static let ProviderOnline  = "providerOnline"
        static let UserId  = "userId"
        static let UserName  = "userName"
        static let UserFCMToken  = "userFCMToken"
        static let UserOnline  = "userOnline"
        static let Status  = "status"
        static let CreatedAt  = "createdAt"
        static let UpdatedAt  = "updatedAt"
        
    }
    struct Message {
        
        static let Chat_Id  = "cr_id"
        static let SenderName  = "full_name"
        static let SenderId  = "user_id_fk"
        static let MessageType  = "msgType"
        static let Picture  = "picture"
        static let Text  = "message"
        static let CreatedAt  = "created_at"
        static let SendBy  = "is_sender"
        
    }
    
    struct SupportMessageKeys {
        
        static let MsgType = "type"
        static let Text = "message"
        static let Picture =  "picture"
        static let Status =  "status"
        static let SendBy =  "send_by"
        static let CreatedAt =  "created_at"
        
    }
}

//MARK: - Models
class PaddingLabel: UILabel {
    
    @IBInspectable var topInset: CGFloat = 8.0
    @IBInspectable var bottomInset: CGFloat = 8.0
    @IBInspectable var leftInset: CGFloat = 8.0
    @IBInspectable var rightInset: CGFloat = 8.0
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            var contentSize = super.intrinsicContentSize
            contentSize.height += topInset + bottomInset
            contentSize.width += leftInset + rightInset
            return contentSize
        }
    }
}
class Support: NSObject {
    
    var ticketId : String = ""
    var title : String = ""
    var descriptionStr : String = ""
    var status : Bool = true
    var createdAt : String = ""
    var modifiedAt : String = ""
    
}

class Chat: NSObject {
    
    var chatId : String?
    var userId : String?
    var userName : String?
    var userFCMToken : String?
    var providerId : String?
    var providerName : String?
    var providerFCMToken : String?
    var messageType : String?
    var text : String?
    var picture : String?
    var status : String?
    var createdAt : String?
    var updatedAt : String?
}

class Message: NSObject {
    
    enum MsgType {
        case Text
        case Picture
    }
    
    var id : String = ""
    var type : MsgType = .Text
    var text : String = ""
    var userName: String = ""
    var picture : String = ""
    var isSendByMe : Bool = true
    var status : String = ""
    var dateTime : String = ""
    
}
class MessageSection: NSObject {
    
    var createdAt : Date?
    var headingTxt : String = ""
    var messages = [Message]()
    
}

class ChatHelper: NSObject {
    
    static let shared : ChatHelper = {
        return ChatHelper()
    }()
    
    func getDiffreceTime(by startDate : Date) -> String {
        
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormat.timeZone = TimeZone.current
        
        let expDate = dateFormat.date(from: dateFormat.string(from: startDate))
        let currentDate = dateFormat.date(from: dateFormat.string(from: Date()))
        
        let calendar = NSCalendar.current
        let dateComponent = calendar.dateComponents([Calendar.Component.year, Calendar.Component.month, Calendar.Component.weekday, Calendar.Component.day, Calendar.Component.hour, Calendar.Component.minute , Calendar.Component.second], from: expDate!, to: currentDate!)
        
        var resultTime = ""
        let resultFormatter = DateFormatter()
        resultFormatter.dateFormat = "dd-MM-yyyy"
        resultFormatter.timeZone = TimeZone.current
        
        if dateComponent.year != 0 {
            resultTime = "\(resultFormatter.string(from: startDate))"
        } else if dateComponent.month != 0 {
            resultTime = "\(resultFormatter.string(from: startDate))"
        } else if dateComponent.weekday != 0 {
            resultTime = "\(resultFormatter.string(from: startDate))"
        } else if dateComponent.day != 0 {
            resultTime = (dateComponent.day ?? 0 == 1) ? "Yesterday" : "\(resultFormatter.string(from: startDate))"
        } else if dateComponent.hour != 0 {
            resultTime = "Today"
        } else if dateComponent.minute != 0 {
            resultTime = (dateComponent.minute ?? 0 > 0) ? "Today" : "Just Now"
        } else if dateComponent.second ?? 0 >= 0 {
            resultTime = "Just Now"
        }
        if resultTime.isEmpty {
            resultTime = "Just Now"
        }
        return resultTime
    }
    func convertObjectToMessage(object : [String : Any]) -> Message {
        let singleMessage = Message()
        
        let msgType = "text"
        let sendBy = object[ChatConstant.Message.SendBy] as? String ?? "0"
        let updatedAt : Int = Int("\(object[ChatConstant.Message.CreatedAt]!)")!
        
        singleMessage.type = msgType == "text" ? .Text : .Picture
        singleMessage.text = object[ChatConstant.Message.Text] as? String ?? ""
        singleMessage.userName = object[ChatConstant.Message.SenderName] as? String ?? ""
        singleMessage.picture = ""
        singleMessage.dateTime = "\(updatedAt)"
        singleMessage.isSendByMe = (sendBy == "1") ? true : false
        
        return singleMessage
    }
    func convertObjectToSupportMessage(object : [String : Any]) -> Message {
        let singleMessage = Message()
        
        let msgType = "text"
        let sendBy = object[ChatConstant.SupportMessageKeys.SendBy] as? String ?? "user"
        
        singleMessage.type = msgType == "text" ? .Text : .Picture
        singleMessage.text = object[ChatConstant.SupportMessageKeys.Text] as? String ?? ""
        singleMessage.picture = ""
        singleMessage.status = object[ChatConstant.SupportMessageKeys.Status] as? String ?? "0"
        singleMessage.dateTime = "\(object[ChatConstant.SupportMessageKeys.CreatedAt]!)"
        singleMessage.isSendByMe = sendBy == "user" ? true : false
        
        return singleMessage
    }
    func convertObjectToSupport(object : [String : Any]) -> Support {
        let singleSupport = Support()
        
        singleSupport.ticketId = object[ChatConstant.FIRSupportTicketKeys.TicketId] as? String ?? ""
        singleSupport.title = object[ChatConstant.FIRSupportTicketKeys.Subject] as? String ?? ""
        singleSupport.descriptionStr = object[ChatConstant.FIRSupportTicketKeys.Description] as? String ?? ""
        let status = object[ChatConstant.FIRSupportTicketKeys.Status] as? String ?? ""
        singleSupport.status = status == "1" ? true : false
        singleSupport.createdAt = "\(object[ChatConstant.FIRSupportTicketKeys.CreatedAt]!)"

        return singleSupport
    }
    public func convertObjectToChat(object : [String : Any]) -> Chat {
        
        let chatId = object[ChatConstant.ChatList.Chat_Id] as? String ?? ""
        
        let providerId = object[ChatConstant.ChatList.ProviderId] as? String ?? ""
        let providerName = object[ChatConstant.ChatList.ProviderName] as? String ?? ""
        let providerFCM = object[ChatConstant.ChatList.ProviderFCMToken] as? String ?? ""
        let userId = object[ChatConstant.ChatList.UserId] as? String ?? ""
        let userName = object[ChatConstant.ChatList.UserName] as? String ?? ""
        let userFCM = object[ChatConstant.ChatList.UserFCMToken] as? String ?? ""
        let mesageType = object[ChatConstant.ChatList.MessageType] as? String ?? ""
        let picture = object[ChatConstant.ChatList.Picture] as? String ?? ""
        let text = object[ChatConstant.ChatList.Text] as? String ?? ""
        let status = object[ChatConstant.ChatList.Status] as? String ?? ""
        let createdAt = object[ChatConstant.ChatList.CreatedAt] as? String ?? ""
        let updatedAt : Int = Int("\(object[ChatConstant.ChatList.UpdatedAt]!)")! / 1000
        
        let chatObject = Chat()
        chatObject.chatId = chatId
        chatObject.providerId = providerId
        chatObject.providerName = providerName
        chatObject.providerFCMToken = providerFCM
        chatObject.userId = userId
        chatObject.userName = userName
        chatObject.userFCMToken = userFCM
        chatObject.messageType = mesageType
        chatObject.picture = picture
        chatObject.text = text
        chatObject.status = status
        chatObject.createdAt = createdAt
        chatObject.updatedAt = "\(updatedAt)"
        
        return chatObject
    }
    
    func getSortedArray(mainArr : [Chat]) -> [Chat] {
        
        var sortedArr = [Chat]()
        sortedArr =  mainArr.sorted(by: { (obj1 : Chat, obj2 : Chat) -> Bool in
            
            let firstTimeStamp : Int = Int("\(obj1.updatedAt!)") ?? 0
            let secondTimeStamp : Int = Int("\(obj2.updatedAt!)") ?? 0
            
            return (firstTimeStamp > secondTimeStamp)
        })
        return sortedArr
    }
}

extension UIView {
    
    /**
     Simply zooming in of a view: set view scale to 0 and zoom to Identity on 'duration' time interval.
     - parameter duration: animation duration
     */
    func zoomIn(duration: TimeInterval = 0.3) {
        self.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
        UIView.animate(withDuration: duration, delay: 0.0, options: [.curveLinear], animations: { () -> Void in
            self.transform = .identity
        }) { (animationCompleted: Bool) -> Void in
        }
    }
    
    /**
     Simply zooming out of a view: set view scale to Identity and zoom out to 0 on 'duration' time interval.
     - parameter duration: animation duration
     */
    func zoomOut(duration: TimeInterval = 0.3) {
        self.transform = .identity
        UIView.animate(withDuration: duration, delay: 0.0, options: [.curveLinear], animations: { () -> Void in
            self.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
        }) { (animationCompleted: Bool) -> Void in
        }
    }
    
    /**
     Zoom in any view with specified offset magnification.
     - parameter duration:     animation duration.
     - parameter easingOffset: easing offset.
     */
    func zoomInWithEasing(duration: TimeInterval = 0.2, easingOffset: CGFloat = 0.2) {
        let easeScale = 1.0 + easingOffset
        let easingDuration = TimeInterval(easingOffset) * duration / TimeInterval(easeScale)
        let scalingDuration = duration - easingDuration
        UIView.animate(withDuration: scalingDuration, delay: 0.0, options: .curveEaseIn, animations: { () -> Void in
            self.transform = CGAffineTransform(scaleX: easeScale, y: easeScale)
        }, completion: { (completed: Bool) -> Void in
            UIView.animate(withDuration: easingDuration, delay: 0.0, options: .curveEaseOut, animations: { () -> Void in
                self.transform = .identity
            }, completion: { (completed: Bool) -> Void in
            })
        })
    }
    
    /**
     Zoom out any view with specified offset magnification.
     - parameter duration:     animation duration.
     - parameter easingOffset: easing offset.
     */
    func zoomOutWithEasing(duration: TimeInterval = 0.2, easingOffset: CGFloat = 0.2) {
        let easeScale = 1.0 + easingOffset
        let easingDuration = TimeInterval(easingOffset) * duration / TimeInterval(easeScale)
        let scalingDuration = duration - easingDuration
        UIView.animate(withDuration: easingDuration, delay: 0.0, options: .curveEaseOut, animations: { () -> Void in
            self.transform = CGAffineTransform(scaleX: easeScale, y: easeScale)
        }, completion: { (completed: Bool) -> Void in
            UIView.animate(withDuration: scalingDuration, delay: 0.0, options: .curveEaseOut, animations: { () -> Void in
                self.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
            }, completion: { (completed: Bool) -> Void in
            })
        })
    }
    
}

