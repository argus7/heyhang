//
//  ChatVC.swift
//  GameUp
//
//  Created by Anup Kumar on 8/24/18.
//  Copyright © 2018 Yatharth Singh. All rights reserved.
//

import UIKit
import Alamofire

//MARK: - Helper Class
class msgSendTblCell: UITableViewCell {
    
    @IBOutlet weak var dateTimeLbl: UILabel!
    @IBOutlet weak var msgLbl: PaddingLabel!
    @IBOutlet weak var bubbleIV: UIImageView!
}
class msgRecieveTblCell: UITableViewCell {
    
    @IBOutlet weak var msgLbl: PaddingLabel!
    @IBOutlet weak var dateTimeLbl: UILabel!
    @IBOutlet weak var bubbleIV: UIImageView!
}
class msgRecieveGroupTblCell: UITableViewCell {
    
    @IBOutlet weak var edgeView: UIView!
    @IBOutlet weak var msgLbl: PaddingLabel!
    @IBOutlet weak var dateTimeLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
}

class msgSendImageTblCell: UITableViewCell {
    
    @IBOutlet weak var imgBtn: UIButton!
    @IBOutlet weak var dateTimeLbl: UILabel!
    @IBOutlet weak var mainIV: UIImageView!
    @IBOutlet weak var bubbleIV: UIImageView!
}
class msgRecieveImageTblCell: UITableViewCell {
    
    @IBOutlet weak var imgBtn: UIButton!
    @IBOutlet weak var mainIV: UIImageView!
    @IBOutlet weak var dateTimeLbl: UILabel!
    @IBOutlet weak var bubbleIV: UIImageView!
}


class ChatVC: UIViewController {
  
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var hdrLbl: UILabel!
    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var typingView: UIView!
    @IBOutlet weak var msgTF: DTTextField!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var attachmentBtnWidthConstraints: NSLayoutConstraint!
    @IBOutlet weak var attchmentBtn: UIButton!
    
    @IBOutlet weak var imagePopUpMainView: UIView!
    @IBOutlet weak var popUpIV: UIImageView!
    
    private var messageList = [MessageSection]()
    private var isSortBydate : Bool = true
    private var isAttachmentAdd : Bool = false
    private let currentUserId : String = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? ""
    private let chatTheme : ChatTheme = .BorderWithCorner
    
    public var hdrTitle: String?
    public var opponentId: String?
    public var requestId: String?
    public var chatId: String?
    public var isMoveForGroup : Bool = false
    public var isAdmin : Bool = false
    
//    private var groupChat: ChatGroup?
//    private var freindChat: ChatUser?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        currentChatId = chatId ?? ""
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("ChatNotificationName"), object: nil)
        isAttachmentAdd = false
        if isMoveForGroup { //For Group Chat
            self.hdrLbl.text = hdrTitle ?? "Group 1"
        } else { //Only Perticular User
            self.hdrLbl.text = hdrTitle ?? "User"
        }
        self.setupChat()
        self.getMessagesListAPI(isPaging: false)
    }
    
    private func setupChat() {
        
        self.imagePopUpMainView.isHidden = true
        self.msgTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.msgTF.delegate = self
        msgTF.borderStyle = .none
        msgTF.floatingDisplayStatus = .never
        self.msgTF.backgroundColor = ChatConstant.leftBubbleBackColor
        self.sendBtn.isUserInteractionEnabled = false
        self.attachmentBtnWidthConstraints.constant = isAttachmentAdd ? 40.0 : 0.0
        self.attchmentBtn.isHidden = isAttachmentAdd ? false : true
        
        switch chatTheme {
        case .Border:
            self.tableView.backgroundView = nil
            self.tableView.backgroundColor = UIColor.white
            break
        case .BorderWithCorner:
            self.tableView.backgroundView = nil
            self.tableView.backgroundColor = UIColor.white
            break
        case .CornerEdge:
            self.tableView.backgroundColor = UIColor.clear
            self.tableView.backgroundView = UIImageView(image: #imageLiteral(resourceName: "ChatBack.png"))
            break
        }
    }
    override func viewWillLayoutSubviews() {
        self.headerView.setSideDropShadow(isUp: false)
        self.typingView.setSideDropShadow(isUp: true)
        self.sendBtn.layer.cornerRadius = 12.0
        self.msgTF.layer.cornerRadius = 10.0
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        
        if let data = notification.userInfo {
            let userImage = data["gcm.notification.profile_image"] as? String ?? ""
            let userName = data["gcm.notification.full_name"] as? String ?? ""
            let userId = data["gcm.notification.user_id"] as? String ?? "0"
            let messageStr = data["gcm.notification.message"] as? String ?? ""
            let msgId = data["gcm.message_id"] as? String ?? "0"
            let time = data["gcm.notification.created_at"] as? String ?? "0"
            
            if !(userId == currentUserId) {
                let message = Message()
                message.id = msgId
                message.text = messageStr
                message.isSendByMe = false
                message.dateTime = time
                message.type = .Text
                message.userName = userName
                message.picture = userImage
                
                self.sendMessageFromSection(object: message)
            }
        }
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickMoreBtn(_ sender: Any) {
        
        var ypos: CGFloat = 100.0
        if let frame = self.moreBtn.superview?.convert(moreBtn.frame, to: self.view) {
            ypos = frame.origin.y
        }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OptionCellVC") as! OptionCellVC
        vc.modalPresentationStyle = .overCurrentContext
        vc.titleArr = isMoveForGroup ? ["Group Information", "Report", "Delete Chat"] : ["Unfriend", "Report", "Delete Chat"]
        vc.delegate = self
        vc.topConstraints = ypos
        vc.tag = 0
        self.present(vc, animated: false, completion: nil)
    }
    @IBAction func onClickSendBtn(_ sender: Any) {
        self.sendSingleMessage(type: .Text, text: self.msgTF.text ?? "", picture: "")
    }
    @IBAction func onClickAttachmentBtn(_ sender: Any) {
        //Module not in this app
    }
    @IBAction func onClickPopUPImgCloseBtn(_ sender: Any) {
        self.imagePopUpMainView.zoomOut()
        self.imagePopUpMainView.isHidden = true
    }
    
    @IBAction func onClickOpenImgFromCellBtn(_ sender: Any) {
        let point : CGPoint = (sender as AnyObject).convert(.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: point)!
        self.imagePopUpMainView.isHidden = false
        
        var tMessages = messageList[indexPath.section].messages
        tMessages = tMessages.sorted(by: { $0.dateTime < $1.dateTime })
        let msgObject = tMessages[indexPath.row]
        
        if msgObject.type == .Picture {
            if msgObject.isSendByMe {
                let cell : msgSendImageTblCell = self.tableView.cellForRow(at: indexPath) as! msgSendImageTblCell
                self.popUpIV.image = cell.mainIV.image
            } else {
                let cell : msgRecieveImageTblCell = self.tableView.cellForRow(at: indexPath) as! msgRecieveImageTblCell
                self.popUpIV.image = cell.mainIV.image
            }
        }
        self.imagePopUpMainView.zoomIn()
    }
}
// MARK: - OptionCellDelegate For getting the action of option button
extension ChatVC: OptionCellDelegate {
    func onOptionSelected(positionTag: Int, index: Int) {
        //Option menu actions
        if index == 0 {
            if isMoveForGroup { //Group Information
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "GroupInformationVC") as! GroupInformationVC
                vc.groupId = self.chatId!
                vc.groupName = self.hdrTitle ?? ""
                vc.isAdmin = self.isAdmin
                self.navigationController?.pushViewController(vc, animated: true)
            } else { //Unfriend
                self.unFriendConfirmAlert(id: self.requestId ?? "0", name: hdrTitle ?? "")
            }
        } else if index == 1 { //Report
            reportUserGroup()
        } else { // Delegte Chat
            if checkInternet() {
                self.executeAPIForDeleteChatGroup()
            }
        }
    }
}
//MARK: - Textfield Delegate
extension ChatVC : UITextFieldDelegate {
    @objc func textFieldDidChange(_ theTextField: UITextField) {
        if (theTextField.text?.isEmpty)! {
            self.sendBtn.isUserInteractionEnabled = false
        } else {
            self.sendBtn.isUserInteractionEnabled = true
        }
    }
}

//MARK: - UITableView Delegate Data Source Methods
extension ChatVC : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return messageList.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageList[section].messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var tMessages = messageList[indexPath.section].messages
        tMessages = tMessages.sorted(by: { $0.dateTime < $1.dateTime })
        let msgObject = tMessages[indexPath.row]
        
        if getTotalMsgCount() % 50 == 0 {
            let currIndex = getObjectIndex(message: msgObject)
            if currIndex == 8 {
                self.getMessagesListAPI(isPaging: true)
            }
        }
        
        if msgObject.type == .Picture {
            if msgObject.isSendByMe {
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "SendImageCell", for: indexPath) as? msgSendImageTblCell
                
                cell?.mainIV.imageFromServerURL(msgObject.picture, placeHolder: #imageLiteral(resourceName: "favoritePlaceholder.png"))
                
                switch chatTheme {
                case .BorderWithCorner :
                    cell?.bubbleIV.isHidden = true
                    cell?.mainIV.layer.borderColor = ChatConstant.rightBubbleBorderColor
                    cell?.mainIV.layer.borderWidth = ChatConstant.bubbleBorderWidth
                    cell?.mainIV.layer.cornerRadius = ChatConstant.bubbleImageCornerRadius
                    break
                case .Border :
                    cell?.bubbleIV.isHidden = true
                    cell?.mainIV.layer.borderColor = ChatConstant.rightBubbleBorderColor
                    cell?.mainIV.layer.borderWidth = ChatConstant.bubbleBorderWidth
                    cell?.mainIV.layer.cornerRadius = 0
                    break
                case .CornerEdge :
                    cell?.mainIV.layer.cornerRadius = 10.0
                    cell?.bubbleIV.tintColor = UIColor(red: 48/255.0, green: 131/255.0, blue: 251/255.0, alpha: 1.0)
                    cell?.bubbleIV.isHidden = false
                    cell?.bubbleIV.image = UIImage.init(named: "chat_bubble_sent")?.resizableImage(withCapInsets: UIEdgeInsets(top: 17, left: 21, bottom: 17, right: 21), resizingMode: .stretch).withRenderingMode(.alwaysTemplate)
                    break
                }
                
                cell?.mainIV.clipsToBounds = true
                let formatter = DateFormatter()
                formatter.dateFormat = ChatConstant.bubbleDateTimeFormat
                if let timeInterval = TimeInterval(msgObject.dateTime) {
                    let tDate = Date(timeIntervalSince1970: timeInterval)
                    cell?.dateTimeLbl.text = formatter.string(from: tDate)
                } else {
                    cell?.dateTimeLbl.text = ""
                }
                cell?.imgBtn.addTarget(self, action: #selector(onClickOpenImgFromCellBtn(_:)), for: .touchUpInside)
                return cell!
            } else {
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "RecieveImageCell", for: indexPath) as? msgRecieveImageTblCell
                
                cell?.mainIV.imageFromServerURL(msgObject.picture, placeHolder: #imageLiteral(resourceName: "favoritePlaceholder.png"))
                
                switch chatTheme {
                case .BorderWithCorner :
                    cell?.bubbleIV.isHidden = true
                    cell?.mainIV.layer.borderColor = ChatConstant.rightBubbleBorderColor
                    cell?.mainIV.layer.borderWidth = ChatConstant.bubbleBorderWidth
                    cell?.mainIV.layer.cornerRadius = ChatConstant.bubbleImageCornerRadius
                    break
                case .Border :
                    cell?.bubbleIV.isHidden = true
                    cell?.mainIV.layer.borderColor = ChatConstant.rightBubbleBorderColor
                    cell?.mainIV.layer.borderWidth = ChatConstant.bubbleBorderWidth
                    cell?.mainIV.layer.cornerRadius = 0
                    break
                case .CornerEdge :
                    cell?.mainIV.layer.cornerRadius = 10.0
                    cell?.bubbleIV.tintColor = UIColor(red: 178/255.0, green: 178/255.0, blue: 178/255.0, alpha: 1.0)
                    cell?.bubbleIV.isHidden = false
                    cell?.bubbleIV.image = UIImage.init(named: "chat_bubble_received")?.resizableImage(withCapInsets: UIEdgeInsets(top: 17, left: 21, bottom: 17, right: 21), resizingMode: .stretch).withRenderingMode(.alwaysTemplate)
                    break
                }
                
                cell?.mainIV.clipsToBounds = true
                
                let formatter = DateFormatter()
                formatter.dateFormat = ChatConstant.bubbleDateTimeFormat
                if let timeInterval = TimeInterval(msgObject.dateTime) {
                    let tDate = Date(timeIntervalSince1970: timeInterval)
                    cell?.dateTimeLbl.text = formatter.string(from: tDate)
                } else {
                    cell?.dateTimeLbl.text = ""
                }
                cell?.imgBtn.addTarget(self, action: #selector(onClickOpenImgFromCellBtn(_:)), for: .touchUpInside)
                return cell!
            }
        } else { //Text
            if msgObject.isSendByMe {
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "SendCell", for: indexPath) as? msgSendTblCell
                
                cell?.msgLbl.text = msgObject.text
                cell?.msgLbl.textColor = UIColor.black
                cell?.bubbleIV.isHidden = true
                
                switch chatTheme {
                case .BorderWithCorner :
                    cell?.msgLbl.clipsToBounds = true
                    cell?.msgLbl.layer.cornerRadius = 10.0
                    cell?.msgLbl.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
                    cell?.msgLbl.backgroundColor = ChatConstant.rightBubbleBackColor
                    cell?.msgLbl.textColor = UIColor.white
                    break
                case .Border :
                    cell?.msgLbl.layer.borderColor = ChatConstant.rightBubbleBorderColor
                    cell?.msgLbl.layer.borderWidth = ChatConstant.bubbleBorderWidth
                    cell?.msgLbl.layer.cornerRadius = 0
                    cell?.msgLbl.textColor = UIColor.black
                    break
                case .CornerEdge :
                    cell?.bubbleIV.isHidden = false
                    cell?.bubbleIV.tintColor = UIColor(red: 48/255.0, green: 131/255.0, blue: 251/255.0, alpha: 1.0)
                    cell?.msgLbl.textColor = UIColor.white
                    cell?.bubbleIV.image = UIImage.init(named: "chat_bubble_sent")?.resizableImage(withCapInsets: UIEdgeInsets(top: 17, left: 21, bottom: 17, right: 21), resizingMode: .stretch).withRenderingMode(.alwaysTemplate)
                    break
                }
                
                let formatter = DateFormatter()
                formatter.dateFormat = ChatConstant.bubbleDateTimeFormat
                if let timeInterval = TimeInterval(msgObject.dateTime) {
                    let tDate = Date(timeIntervalSince1970: timeInterval)
                    cell?.dateTimeLbl.text = formatter.string(from: tDate)
                } else {
                    cell?.dateTimeLbl.text = ""
                }
                
                return cell!
            } else {
                
                if isMoveForGroup {
                    let cell = self.tableView.dequeueReusableCell(withIdentifier: "RecieveGroupCell", for: indexPath) as? msgRecieveGroupTblCell
                    
                    cell?.msgLbl.text = msgObject.text
                    cell?.userNameLbl.text = msgObject.userName
                    cell?.msgLbl.textColor = UIColor.black
                    
                    switch chatTheme {
                    case .BorderWithCorner :
                        cell?.edgeView.clipsToBounds = true
                        cell?.edgeView.layer.cornerRadius = 10.0
                        cell?.edgeView.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
                        cell?.edgeView.backgroundColor = ChatConstant.leftBubbleBackColor
                        cell?.msgLbl.textColor = UIColor.black
                        break
                    case .Border :
                        cell?.msgLbl.layer.borderColor = ChatConstant.rightBubbleBorderColor
                        cell?.msgLbl.layer.borderWidth = ChatConstant.bubbleBorderWidth
                        cell?.msgLbl.layer.cornerRadius = 0
                        break
                    case .CornerEdge :
                        break
                    }
                    
                    let formatter = DateFormatter()
                    formatter.dateFormat = ChatConstant.bubbleDateTimeFormat
                    if let timeInterval = TimeInterval(msgObject.dateTime) {
                        let tDate = Date(timeIntervalSince1970: timeInterval)
                        cell?.dateTimeLbl.text = formatter.string(from: tDate)
                    } else {
                        cell?.dateTimeLbl.text = ""
                    }
                    return cell!
                } else {
                    let cell = self.tableView.dequeueReusableCell(withIdentifier: "RecieveCell", for: indexPath) as? msgRecieveTblCell
                    
                    cell?.msgLbl.text = msgObject.text
                    cell?.msgLbl.textColor = UIColor.black
                    cell?.bubbleIV.isHidden = true
                    
                    switch chatTheme {
                    case .BorderWithCorner :
                        cell?.msgLbl.clipsToBounds = true
                        cell?.msgLbl.layer.cornerRadius = 10.0
                        cell?.msgLbl.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
                        cell?.msgLbl.backgroundColor = ChatConstant.leftBubbleBackColor
                        cell?.msgLbl.textColor = UIColor.black
                        break
                    case .Border :
                        cell?.msgLbl.layer.borderColor = ChatConstant.rightBubbleBorderColor
                        cell?.msgLbl.layer.borderWidth = ChatConstant.bubbleBorderWidth
                        cell?.msgLbl.layer.cornerRadius = 0
                        break
                    case .CornerEdge :
                        cell?.bubbleIV.isHidden = false
                        cell?.bubbleIV.tintColor = UIColor(red: 178/255.0, green: 178/255.0, blue: 178/255.0, alpha: 1.0)
                        cell?.msgLbl.textColor = UIColor.white
                        cell?.bubbleIV.image = UIImage.init(named: "chat_bubble_received")?.resizableImage(withCapInsets: UIEdgeInsets(top: 17, left: 21, bottom: 17, right: 21), resizingMode: .stretch).withRenderingMode(.alwaysTemplate)
                        break
                    }
                    
                    let formatter = DateFormatter()
                    formatter.dateFormat = ChatConstant.bubbleDateTimeFormat
                    if let timeInterval = TimeInterval(msgObject.dateTime) {
                        let tDate = Date(timeIntervalSince1970: timeInterval)
                        cell?.dateTimeLbl.text = formatter.string(from: tDate)
                    } else {
                        cell?.dateTimeLbl.text = ""
                    }
                    return cell!
                }
            }
        }
    }
    private func getObjectIndex(message: Message) -> Int {
        var index = -1
        for list in messageList {
            for object in list.messages {
                index += 1
                if message.dateTime == object.dateTime {
                    return index
                }
            }
        }
        return index
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let msgObject = messageList[indexPath.section].messages.sorted(by: { $0.dateTime < $1.dateTime })[indexPath.row]
        if msgObject.type == .Picture {
            return 120.0
        }
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let msgObject = messageList[indexPath.section].messages.sorted(by: { $0.dateTime < $1.dateTime })[indexPath.row]
        if msgObject.type == .Picture {
            return 120.0
        } else if msgObject.type == .Text{
            return 35.0
        }
        return 0.0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect.init(x: 0, y: 0, width: self.tableView.frame.size.width, height: 25.0))
        view.backgroundColor = UIColor.clear
        
        var headerLblFrame = view.bounds
        headerLblFrame.origin.x = (headerLblFrame.size.width - ChatConstant.sectionViewWidth) / 2
        headerLblFrame.size.width = ChatConstant.sectionViewWidth
        
        let label = UILabel(frame: headerLblFrame)
        label.font = ChatConstant.sectionTitleFont
        label.text = messageList[section].headingTxt
        label.textAlignment = .center
        label.backgroundColor = ChatConstant.sectionBackgroundColor
        label.textColor = ChatConstant.sectionTitleTextColor
        label.setCircleCorner()
        view.addSubview(label)
        return view
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.messageList.count == 0 {
            return 0
        }
        if self.isSortBydate {
            return ChatConstant.sectionViewHeight
        } else {
            return 0.0
        }
    }
}
// MARK: - Helping Methods
extension ChatVC {
    private func refreshChatList() {
//        self.msgTF.resignFirstResponder()
        self.sendBtn.isUserInteractionEnabled = false
        self.messageList = self.messageList.sorted(by: { $0.createdAt! < $1.createdAt! })
        self.tableView.reloadData()
        
        if self.messageList.count > 0 {
            let section : Int = messageList.count - 1
            self.tableView.scrollToRow(at: IndexPath(row: messageList[section].messages.count - 1, section: section), at: .bottom, animated: false)
        }
    }
    private func sendSingleMessage(type : Message.MsgType, text : String, picture : String) {
        
        self.refreshWholeChatByTime()
        
        let message = Message()
        message.text = text
        message.isSendByMe = true
        message.dateTime = String(format : "%.f", Date().timeIntervalSince1970)
        message.type = type
        message.picture = picture
        
        if isMoveForGroup {
            sendGroupChatMessage(message: message)
        } else {
            self.sendMessageAPI(message: message)
        }
        self.msgTF.text = ""
    }
    private func sendMessageFromSection(object : Message) {
        if checkIsAlreadyExist(tempMessage: object) {
            return
        }
        let formatter = DateFormatter()
        formatter.dateFormat = ChatConstant.bubbleDateTimeFormat
        let timeInterval = TimeInterval(object.dateTime)!
        let tDate = Date(timeIntervalSince1970: timeInterval)
        let dateStr : String = ChatHelper.shared.getDiffreceTime(by: tDate)
        
        if let index = getHeaderIndex(headerStr: dateStr) {
            self.messageList[index].messages.append(object)
        } else {
            let singleObject = MessageSection()
            singleObject.headingTxt = dateStr
            singleObject.messages.append(object)
            singleObject.createdAt = tDate
            messageList.append(singleObject)
        }
        self.refreshChatList()
    }
    private func checkIsAlreadyExist(tempMessage : Message) -> Bool {
        for object in messageList {
            for message in object.messages {
                if message.dateTime == tempMessage.dateTime {
                    return true
                }
            }
        }
        return false
    }
    private func getHeaderIndex(headerStr : String) -> Int? {
        var index : Int = 0
        for object in messageList {
            if object.headingTxt == headerStr {
                return index
            }
            index += 1
        }
        return nil
    }
    private func refreshWholeChatByTime() {
        var allMesageArr = [Message]()
        for sectionObject in messageList {
            for singleMessage in sectionObject.messages {
                allMesageArr.append(singleMessage)
            }
        }
        self.messageList = [MessageSection]()
        for object in allMesageArr {
            self.sendMessageFromSection(object: object)
        }
    }
}
/*******************************************************************************/
// MARK: - APIs Method & Support
extension ChatVC {
    
    private func getTotalMsgCount() -> Int {
        var count = 0
        for list in messageList {
            count += list.messages.count
        }
        return count
    }
    
    
    private func getMessagesListAPI(isPaging: Bool) {
        
        let msgCount = getTotalMsgCount()
        
        var params = [String: Any]()
        params["c_id"] = self.chatId ?? "0"
        params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"
        params["start"] = (msgCount == 0) ? "0" : "\(msgCount + 1)"
        
        if !isPaging {
            self.showActivityIndicator(uiView: self.view)
        }
        RestAPI.shared.getMyChatMessageList(isForGroup: isMoveForGroup, params: params) {  (serverData, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                if errorCode == 100 {
                    if let serverData = serverData {
                        for object in serverData {
                            let msgObject = ChatHelper.shared.convertObjectToMessage(object: object)
                            self.sendMessageFromSection(object: msgObject)
                        }
                    }
                } else {
//                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                }
            }
        }
    }
    
    func sendGroupChatMessage(message : Message) {
        
        var params = [String: Any]()
        params["c_id"] = self.chatId ?? "0"
        params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"
        params["message"] = message.text
        
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.sendChatMessage(isForGroup: isMoveForGroup, params: params) {  (isSuccess, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                if isSuccess {
                    let temMsgObject = message
                    temMsgObject.dateTime = "\(Int("\(message.dateTime)")!)"
                    self.sendMessageFromSection(object: temMsgObject)
                } else {
                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                }
            }
        }
    }
    
    private func sendMessageAPI(message : Message) {
        
        var params = [String: Any]()
        params["user_id2"] = self.opponentId ?? "0"
        params["user_id1"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"
        params["message"] = message.text
        
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.sendChatMessage(isForGroup: isMoveForGroup, params: params) {  (isSuccess, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                if isSuccess {
                    let temMsgObject = message
                    temMsgObject.dateTime = "\(Int("\(message.dateTime)")!)"
                    self.sendMessageFromSection(object: temMsgObject)
                } else {
                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                }
            }
        }
    }
    
    private func unFriendConfirmAlert(id: String, name: String) {
        let alertController = UIAlertController.init(title: Constant.APP_NAME, message: "Are you sure want to unfriend \(name)?", preferredStyle: .alert)
        alertController.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
        }))
        alertController.addAction(UIAlertAction.init(title: "Confirm", style: .destructive, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
            self.executeAPIForUnfriend(requestId: id)
        }))
        self.present(alertController, animated: true, completion: nil)
    }
    func executeAPIForUnfriend(requestId: String) {
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.unfriendUser(requestId: requestId) {  (isSuccess, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                if isSuccess {
                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                    self.navigationController?.popViewController(animated: true)
                } else {
                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                }
            }
        }
    }
    func executeAPIForDeleteChatGroup() {
        /*
         //request key->c_id,user_id
         */
        var params = [String: Any]()
        params["user_id"] = currentUserId
        if isMoveForGroup {
            params["group_id"] = currentChatId
        } else {
            params["c_id"] = currentChatId
        }
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.deleteChatGroup(isGroup: isMoveForGroup, params: params) {  (isSuccess, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    func reportUserGroup() {
        //'user_id','reported_id',’type’
        var params = [String: Any]()
        params["user_id"] = self.currentUserId
        params["reported_id"] = isMoveForGroup ? (self.chatId ?? "0") : (opponentId ?? "0")
        params["type"] = isMoveForGroup ? "2" : "1"
        
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.reportUserGroup(params: params) {  (isSuccess, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
            }
        }
    }
}
