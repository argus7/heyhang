//
//  ChatNotification.swift
//  HeyHang
//
//  Created by Anup Kumar on 7/6/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

class ChatNotification: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
}

extension UIViewController {
    func showMAChatAlert(message:String, type: ChatPopUp.Status) {
        ChatPopUp.showMAChatAlert(imageName: "profile_placeholder", userName: "Mohd Arsad", message: message)
    }
}

class ChatPopUp: NSObject {
    
    enum Status {
        case success
        case error
        case warning
    }
    
    class func showMAChatAlert(imageName: String?,userName: String?,message: String?) {
        
        let xpos: CGFloat = 20
        let ypos: CGFloat = 0
        let width: CGFloat = UIScreen.main.bounds.width - (xpos * 2)
        let height: CGFloat = 60
        
        let viewFrame = CGRect(x: xpos, y: ypos, width: width, height: height)
        let pView = MAChatPopUp(frame: viewFrame)
        pView.setupView(imageName: imageName, title: userName, message: message)
        
        let currentWindow: UIWindow? = UIApplication.shared.keyWindow
        currentWindow?.addSubview(pView)
        
        pView.addConstraint(newView: pView)
        currentWindow?.bringSubviewToFront(pView)
        
    }
}

/// Chat PopUp Message Alert Class
class MAChatPopUp: UIView {
    
    var timer: Timer?
    var timeCount: Int = 1
    var profileIV: UIImageView?
    var titleLbl: UILabel?
    var descriptionLbl: UILabel?
    
    private let animationTotalTime: Int = 3
    private let animationTime: TimeInterval = 0.3
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(setupTimerForHideView), userInfo: nil, repeats: true)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func setupTimerForHideView() {
        if timeCount == 3 {
            timer?.invalidate()
            animateToHide()
        }
        timeCount += 1
    }
}
// MARK: - All Setup frames
extension MAChatPopUp {
    
    func setupView(imageName: String?,title: String?,message: String?) {
        
        self.setupDropShadow()
        self.setupProfileImageView()
        self.setupTitleLabel()
        self.setupDescriptionLabel()
        //profile_placeholder
        if let imgName = imageName {
            self.profileIV?.imageFromServerURL(imgName, placeHolder: UIImage(named: "profile_placeholder"))
        } else {
            self.profileIV?.image = UIImage(named: "profile_placeholder")
        }
        if let title = title {
            self.titleLbl?.text = title
        }
        if let descriptionString = message {
            self.descriptionLbl?.text = descriptionString
        }
        animateToShow()
    }
    private func setupDropShadow() {
        self.backgroundColor = UIColor.white
        self.layer.cornerRadius = 5.0
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowOpacity = 0.6
        self.layer.shadowRadius = 10.0
    }
    private func setupProfileImageView() {
        let imageSquareSize: CGFloat = (self.bounds.height - 10)
        let xpos: CGFloat = 10
        let ypos: CGFloat = (self.bounds.height - imageSquareSize) / 2
        let width: CGFloat = imageSquareSize
        let height: CGFloat = imageSquareSize
        
        let frame = CGRect(x: xpos, y: ypos, width: width, height: height)
        self.profileIV = UIImageView(frame: frame)
        self.profileIV?.layer.cornerRadius = height / 2
        self.profileIV?.layer.backgroundColor = UIColor.clear.cgColor
        self.profileIV?.clipsToBounds = true
        self.addSubview(self.profileIV!)
        self.addImageConstraint(newView: self.profileIV!)
    }
    private func setupTitleLabel() {
        let xpos: CGFloat = (self.profileIV!.frame.origin.x + self.profileIV!.frame.size.width) + 10
        let ypos: CGFloat = 5
        let width: CGFloat = self.bounds.width - (xpos + 10)
        let height: CGFloat = 20.0
        
        let frame = CGRect(x: xpos, y: ypos, width: width, height: height)
        self.titleLbl = UILabel(frame: frame)
        self.titleLbl?.textColor = UIColor.darkText
        self.titleLbl?.font = UIFont.boldSystemFont(ofSize: 16.0)
        self.addSubview(self.titleLbl!)
        self.addTitleConstraint(newView: self.titleLbl!)
    }
    private func setupDescriptionLabel() {
        let xpos: CGFloat = (self.profileIV!.frame.origin.x + self.profileIV!.frame.size.width) + 10
        let ypos: CGFloat = 25.0
        let width: CGFloat = self.bounds.width - (xpos + 10)
        let height: CGFloat = self.bounds.height - 30.0
        
        let frame = CGRect(x: xpos, y: ypos, width: width, height: height)
        self.descriptionLbl = UILabel(frame: frame)
        self.descriptionLbl?.textColor = UIColor.darkText
        self.descriptionLbl?.font = UIFont.systemFont(ofSize: 15.0)
        self.addSubview(self.descriptionLbl!)
        self.addMessageConstraint(newView: self.descriptionLbl!)
    }
    
    //========================================================================
    func addConstraint(newView: UIView) {
        newView.translatesAutoresizingMaskIntoConstraints = false
        let leadingConstraints = NSLayoutConstraint(item: newView, attribute: .leading, relatedBy: .equal, toItem: newView.superview, attribute: .leading, multiplier: 1.0, constant: 20)
        
        let trailingConstraints = NSLayoutConstraint(item: newView, attribute: .trailing, relatedBy: .equal, toItem: newView.superview, attribute: .trailing, multiplier: 1.0, constant: -20)
        
        let topConstraints = NSLayoutConstraint(item: newView, attribute: .topMargin, relatedBy: .equal, toItem: newView.superview, attribute: .topMargin, multiplier: 1.0, constant: 20)
        
        let heightConstraint = NSLayoutConstraint(item: newView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.greaterThanOrEqual, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 0.25, constant: 60.0)
        newView.superview!.addConstraints([leadingConstraints, trailingConstraints, topConstraints, heightConstraint])
    }
    private func addTitleConstraint(newView: UILabel) {
        newView.translatesAutoresizingMaskIntoConstraints = false
        let leadingConstraints = NSLayoutConstraint(item: newView, attribute: .leading, relatedBy: .equal, toItem: self.profileIV, attribute: .trailing, multiplier: 1.0, constant: 10.0)
        
        let trailingConstraints = NSLayoutConstraint(item: newView, attribute: .trailing, relatedBy: .equal, toItem: newView.superview, attribute: .trailing, multiplier: 1.0, constant: -10)

        let topConstraints = NSLayoutConstraint(item: newView, attribute: .top, relatedBy: .equal, toItem: newView.superview, attribute: .top, multiplier: 1.0, constant: 10.0)
        
        newView.superview!.addConstraints([leadingConstraints, trailingConstraints, topConstraints])
    }
    private func addMessageConstraint(newView: UILabel) {
        newView.translatesAutoresizingMaskIntoConstraints = false
        let leadingConstraints = NSLayoutConstraint(item: newView, attribute: .leading, relatedBy: .equal, toItem: self.titleLbl, attribute: .leading, multiplier: 1.0, constant: 0.0)
        
        let trailingConstraints = NSLayoutConstraint(item: newView, attribute: .trailing, relatedBy: .equal, toItem: newView.superview, attribute: .trailing, multiplier: 1.0, constant: -10)
        
        let bottomConstraints = NSLayoutConstraint(item: newView, attribute: .bottomMargin, relatedBy: .equal, toItem: newView.superview, attribute: .bottomMargin, multiplier: 1.0, constant: -5)
        
        let topConstraints = NSLayoutConstraint(item: newView, attribute: .top, relatedBy: .equal, toItem: self.titleLbl, attribute: .bottom, multiplier: 1.0, constant: 0)
        
        newView.superview!.addConstraints([leadingConstraints, trailingConstraints, bottomConstraints, topConstraints])
    }
    private func addImageConstraint(newView: UIImageView) {
        newView.translatesAutoresizingMaskIntoConstraints = false
        
        let leadingConstraints = NSLayoutConstraint(item: newView, attribute: .leading, relatedBy: .equal, toItem: newView.superview, attribute: .leading, multiplier: 1.0, constant: 10.0)
        
        let heightConstraint = NSLayoutConstraint(item: newView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: 50.0)
        
        let widthConstraint = NSLayoutConstraint(item: newView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: 50.0)
     
        let verticalCenterConstraints = NSLayoutConstraint(item: newView, attribute: .centerY, relatedBy: .equal, toItem: newView.superview, attribute: .centerY, multiplier: 1.0, constant: 0)
        newView.superview!.addConstraints([leadingConstraints, heightConstraint, widthConstraint, verticalCenterConstraints])
    }
}

// MARK: - MAStatusPopUp Animation
extension MAChatPopUp {
    private func animateToHide() {
        UIView.animate(withDuration: animationTime, animations: {
            var frame = self.frame
            frame.size.height = 0
            self.frame = frame
            self.titleLbl?.alpha = 0.0
            self.descriptionLbl?.alpha = 0.0
            self.profileIV?.alpha = 0.0
        }, completion: { isTrue in
            self.alpha = 0.0
            self.removeFromSuperview()
        })
    }
    private func animateToShow() {
        self.alpha = 0.0
        UIView.animate(withDuration: animationTime, animations: {
            self.alpha = 1.0
            
            let window = UIApplication.shared.keyWindow
            let topPadding = window?.safeAreaInsets.top
            
            var frame = self.frame
            frame.origin.y = topPadding!
            self.frame = frame
        })
    }
}
