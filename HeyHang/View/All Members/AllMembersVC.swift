//
//  AllMembersVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 6/6/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

class AllMembersVC: UIViewController {
    
    enum Navigation {
        case eventDetail
        case allRequest
        case userList
    }
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var hdrLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBtn: UIButton!
    
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var searchViewWidthConstraints: NSLayoutConstraint!
    
    var navigation: Navigation = .eventDetail
    var userList: [User] = []
    var userDisplayList: [User] = []
    var selectedEvent: Event?
    
    private var eventId: String = ""
    private var isEventRequest: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.setupTextField()
        if let event = self.selectedEvent {
            self.eventId = event.eventId ?? ""
        }
        setupTableView()
        switch navigation {
        case .allRequest:
            isEventRequest = eventId.isEmpty ? false : true
            self.searchBtn.isHidden = true
            self.hdrLbl.text = "All Request"
            if checkInternet(){
                self.executeAPIForGetAllEventRequest(isShowAlert: true)
            }
            break
        case .eventDetail:
            self.searchBtn.isHidden = false
            self.hdrLbl.text = "All Members"
            self.userDisplayList = self.userList
            self.tableView.reloadData()
            break
        case .userList:
            self.searchBtn.isHidden = false
            self.hdrLbl.text = "All Members"
            if checkInternet() {
                self.executeAPIForGetAllMembersList()
            }
            break
        }
    }
    override func viewWillLayoutSubviews() {
        self.headerView.setSideDropShadow(isUp: false)
        self.searchView.layer.cornerRadius = 5.0
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickSearchBtn(_ sender: Any) {
        if self.searchViewWidthConstraints.constant == 0.0 {
            showSearchBar()
        } else {
            hideSearchBar()
        }
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateGroupVC") as! CreateGroupVC
//        vc.navigation = .myFriend
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    private func removeSelfFromList() {
        let currentUserId = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? ""
        self.userList = self.userList.filter({ (user) -> Bool in
            return !(user.userId == currentUserId)
        })
    }
}
extension AllMembersVC : UITextFieldDelegate {
    
    private func setupTextField() {
        self.searchViewWidthConstraints.constant = 0
        self.searchTF.isHidden = true
        self.searchTF.delegate = self
        self.searchTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    @objc func textFieldDidChange(_ theTextField: UITextField) {
        
        switch navigation {
        case .allRequest:
            break
        case .userList, .eventDetail:
            if theTextField.text == "" {
                self.userDisplayList = self.userList
                self.tableView.reloadData()
                return
            }
            let filterArr = self.userList.filter({ return (($0.fullName ?? "").lowercased().contains((theTextField.text ?? "").lowercased())) })
            self.userDisplayList = filterArr
            self.tableView.reloadData()
            break
        }
    }
    //Search Show/Hide Methods  for chat list and friend list
    private func showSearchBar() {
        UIView.animate(withDuration: 0.5) {
            self.searchTF.isHidden = false
            self.searchViewWidthConstraints.constant = UIScreen.main.bounds.width - 100
            self.view.layoutIfNeeded()
            self.searchBtn.setImage(UIImage.init(named: "crossWhite"), for: .normal)
        }
    }
    private func hideSearchBar() {
        UIView.animate(withDuration: 0.5) {
            self.view.endEditing(true)
            self.searchTF.isHidden = true
            self.searchViewWidthConstraints.constant = 0.0
            self.view.layoutIfNeeded()
            self.searchBtn.setImage(UIImage(named: "search_white"), for: .normal)
            
            self.searchTF.text = ""
            self.userDisplayList = self.userList
            self.tableView.reloadData()
        }
    }
}
extension AllMembersVC: UITableViewDelegate, UITableViewDataSource {
    fileprivate func setupTableView() {
        self.tableView.register(UINib(nibName: "AllMembersTableCell", bundle: Bundle.main), forCellReuseIdentifier: "AllMembersTableCell")
        self.tableView.register(UINib(nibName: "RequestTblCell", bundle: Bundle.main), forCellReuseIdentifier: "RequestTblCell")
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if userDisplayList.count > 0 {
            self.tableView.backgroundView = nil
            return userDisplayList.count
        } else {
            switch navigation {
            case .allRequest:
                if isEventRequest {
                    self.tableView.showBackgroundMsg(msg: "No Request Found")
                } else {
                    self.tableView.showBackgroundMsg(msg: "No Friend Request")
                }
                break
            case .eventDetail:
                self.tableView.showBackgroundMsg(msg: "No Members Added")
                break
            case .userList:
                self.tableView.showBackgroundMsg(msg: "No Members Found")
                break
            }
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (navigation == .eventDetail || navigation == .userList) {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "AllMembersTableCell", for: indexPath) as! AllMembersTableCell
            
            cell.addFriendsBtnOutlet.tag = indexPath.row
            cell.addFriendsBtnOutlet.addTarget(self, action: #selector(onClickAddFriendCellBtn(_:)), for: .touchUpInside)
            
            cell.user = userDisplayList[indexPath.row]
            
            let currentUserId = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? ""
            if userDisplayList[indexPath.row].userId == currentUserId {
                cell.addFriendsBtnOutlet.isHidden = true
            }
            if let event = self.selectedEvent {
                if event.userId == userDisplayList[indexPath.row].userId {
                    cell.createrHeightConstraints.constant = 20.0
                    cell.createrLbl.isHidden = false
                } else {
                    cell.createrHeightConstraints.constant = 0.0
                    cell.createrLbl.isHidden = true
                }
            } else {
                cell.createrHeightConstraints.constant = 0.0
                cell.createrLbl.isHidden = true
            }
            return cell
        } else {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "RequestTblCell", for: indexPath) as! RequestTblCell
            cell.user = userDisplayList[indexPath.row]
            cell.acceptBtn.tag = indexPath.row
            cell.acceptBtn.addTarget(self, action: #selector(onClickAcceptRequestCellBtn(_:)), for: .touchUpInside)
            cell.deleteBtn.tag = indexPath.row
            cell.deleteBtn.addTarget(self, action: #selector(onClickCancelRequestCellBtn(_:)), for: .touchUpInside)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85.0
    }
    @objc func onClickAddFriendCellBtn(_ sender: UIButton) {
        let user = userDisplayList[sender.tag]
        if checkInternet() {
            if user.isFriend == "2" {
                self.executeAPIForCancelConfirmRequest(user: user, isAccept: false)
            } else {
                self.executeAPIForAddFriend(friendId: user.userId ?? "0", index: sender.tag)
            }
        }
    }
    @objc func onClickAcceptRequestCellBtn(_ sender: UIButton) {
        let user = userDisplayList[sender.tag]
        if checkInternet() {
            self.executeAPIForCancelConfirmRequest(user: user, isAccept: true)
        }
    }
    @objc func onClickCancelRequestCellBtn(_ sender: UIButton) {
        let user = userDisplayList[sender.tag]
        if checkInternet() {
            self.executeAPIForCancelConfirmRequest(user: user, isAccept: false)
        }
    }
}
// MARK: - APIs
extension AllMembersVC {
    func executeAPIForAddFriend(friendId: String, index: Int) {
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.addFriendUser(friendId: friendId) {  (requestId, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                if errorCode == 100 {
                    switch self.navigation {
                    case .allRequest:
                        self.executeAPIForGetAllEventRequest(isShowAlert: false)
                        break
                    case .eventDetail:
                        if let id = requestId {
                            self.updateDetailMemberStatus(status: "2", user: self.userDisplayList[index], requestId: id)
                        }
                        break
                    case .userList:
                        self.executeAPIForGetAllMembersList()
                        break
                    }
                }
            }
        }
    }
    private func updateDetailMemberStatus(status: String, user: User, requestId: String = "") {
        
        func indexOfUser(users: [User]) -> Int? {
            for index in 0..<users.count {
                if users[index].userId == user.userId {
                    return index
                }
            }
            return nil
        }
        if let index = indexOfUser(users: self.userDisplayList) {
            self.userDisplayList[index].isFriend = status
            self.userDisplayList[index].requestId = requestId
        }
        if let index = indexOfUser(users: self.userList) {
            self.userList[index].isFriend = status
            self.userList[index].requestId = requestId
        }
        self.tableView.reloadData()
    }
    //request_id','status','type
    func executeAPIForCancelConfirmRequest(user: User, isAccept: Bool) {
        
       // status => 1=accept 2=reject/cancel
      //  Type => 1=friend 2=event

        var params = [String: Any]()
        params["request_id"] = user.requestId ?? "0"
        params["status"] = isAccept ? "1" : "2"
        params["type"] = isEventRequest ? "2" : "1"
        
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.cancelAcceptRequest(params: params) { (isSuccess, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                if isSuccess {
                    switch self.navigation {
                    case .allRequest:
                        self.executeAPIForGetAllEventRequest(isShowAlert: false)
                        break
                    case .eventDetail:
                        self.updateDetailMemberStatus(status: "0", user: user, requestId: "0")
                        break
                    case .userList:
                        self.executeAPIForGetAllMembersList()
                        break
                    }
                } else {
                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                }
            }
        }
    }
    func executeAPIForGetAllEventRequest(isShowAlert: Bool) {
        self.userDisplayList = []
        self.userList = []
        self.tableView.reloadData()
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.allRequest(type: isEventRequest ? "2" : "1", eventId: self.eventId) { (serverData, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                if let users = serverData {
                    self.userList = users
                    if self.searchViewWidthConstraints.constant != 0.0 {
                        let searhText = self.searchTF.text ?? ""
                        let filterArr = self.userList.filter({ return (($0.fullName ?? "").lowercased().contains((searhText).lowercased())) })
                        self.userDisplayList = filterArr
                    } else {
                        self.userDisplayList = self.userList
                    }
                    self.tableView.reloadData()
                    
                } else {
                    if isShowAlert {
                        self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                    }
                }
            }
        }
    }
    private func executeAPIForGetAllMembersList() {
        //'user_id','event_privacy'
        var params = [String: Any]()
        params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? ""
        params["event_privacy"] = "5"
        params["event_id"] = ""
        
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.userList(params: params) { (users, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                if let userArr = users {
                    self.userList = userArr
                    self.removeSelfFromList()
                    if self.searchViewWidthConstraints.constant != 0.0 {
                        let searhText = self.searchTF.text ?? ""
                        let filterArr = self.userList.filter({ return (($0.fullName ?? "").lowercased().contains((searhText).lowercased())) })
                        self.userDisplayList = filterArr
                    } else {
                        self.userDisplayList = self.userList
                    }
                    self.tableView.reloadData()
                }
            }
        }
    }
}

