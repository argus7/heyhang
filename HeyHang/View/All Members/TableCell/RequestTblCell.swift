//
//  RequestTblCell.swift
//  HeyHang
//
//  Created by Anup Kumar on 7/6/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

class RequestTblCell: UITableViewCell {
    
    @IBOutlet weak var profileIV: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var acceptBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    
    var user: User? {
        didSet {
            updateDetails()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.addFriendsBtnOutlet.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.3).cgColor
//        self.addFriendsBtnOutlet.layer.borderWidth = 1.0
//        self.addFriendsBtnOutlet.layer.borderWidth = 1.0
//        self.addFriendsBtnOutlet.layer.cornerRadius = 10.0
        
        self.profileIV.setCircleCorner()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    private func updateDetails() {
        if let user = user {
            self.nameLbl.text = user.fullName
            self.profileIV.imageFromServerURL(user.image ?? "", placeHolder: UIImage(named: "profile_placeholder"))
        }
    }
}
