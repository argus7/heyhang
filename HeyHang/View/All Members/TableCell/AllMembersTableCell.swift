//
//  AllMembersTableCell.swift
//  HeyHang
//
//  Created by Anup Kumar on 6/6/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

class AllMembersTableCell: UITableViewCell {

    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var addFriendsBtnOutlet: UIButton!
    @IBOutlet weak var createrLbl: UILabel!
    @IBOutlet weak var createrHeightConstraints: NSLayoutConstraint!
    
    var user: User? {
        didSet {
            updateDetails()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.addFriendsBtnOutlet.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.3).cgColor
        self.addFriendsBtnOutlet.layer.borderWidth = 1.0
        self.addFriendsBtnOutlet.layer.borderWidth = 1.0
        self.addFriendsBtnOutlet.layer.cornerRadius = 10.0
        
        self.profileImgView.setCircleCorner()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func updateDetails() {
        if let user = user {
            self.createrHeightConstraints.constant = 0.0
            self.createrLbl.isHidden = true
            self.nameLbl.text = user.fullName
            self.profileImgView.imageFromServerURL(user.image ?? "", placeHolder: UIImage(named: "profile_placeholder"))
            if user.isFriend == "1" {
                self.addFriendsBtnOutlet.isHidden = true
            } else if user.isFriend == "2" {
                self.addFriendsBtnOutlet.setTitle("Cancel Request", for: .normal)
                self.addFriendsBtnOutlet.isHidden = false
            } else {
                self.addFriendsBtnOutlet.setTitle("Add Friend", for: .normal)
                self.addFriendsBtnOutlet.isHidden = false
            }
        }
    }

}
