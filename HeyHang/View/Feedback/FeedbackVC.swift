//
//  FeedbackVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 5/28/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

class FeedbackVC: UIViewController {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var hdrLbl: UILabel!
    
    @IBOutlet weak var rate1Btn: UIButton!
    @IBOutlet weak var rate2Btn: UIButton!
    @IBOutlet weak var rate3Btn: UIButton!
    @IBOutlet weak var rate4Btn: UIButton!
    @IBOutlet weak var rate5Btn: UIButton!
    @IBOutlet weak var descriptionTF: MATextField!
    @IBOutlet weak var submitBtn: UIButton!
    
    private var selectedRate: Int = 0
    var selectedEvent: Event?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setRate(by: selectedRate)
    }
    override func viewWillLayoutSubviews() {
        self.submitBtn.layer.cornerRadius = Constant.kCornerRadius
        self.headerView.setSideDropShadow(isUp: false)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickRate1Btn(_ sender: Any) {
        setRate(by: 1)
    }
    @IBAction func onClickRate2Btn(_ sender: Any) {
        setRate(by: 2)
    }
    @IBAction func onClickRate3Btn(_ sender: Any) {
        setRate(by: 3)
    }
    @IBAction func onClickRate4Btn(_ sender: Any) {
        setRate(by: 4)
    }
    @IBAction func onClickRate5Btn(_ sender: Any) {
        setRate(by: 5)
    }
    @IBAction func onClickSubmitBtn(_ sender: Any) {
        if checkInternet() {
            if let params = validate() {
                executeAPIForFeedback(params: params)
            }
        }
    }
}
extension FeedbackVC {
    private func setRate(by index: Int) {
        selectedRate = index
        if index == 0 {
            self.rate1Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
            self.rate2Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
            self.rate3Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
            self.rate4Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
            self.rate5Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
        } else if index == 1 {
            self.rate1Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
            self.rate2Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
            self.rate3Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
            self.rate4Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
            self.rate5Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
        } else if index == 2 {
            self.rate1Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
            self.rate2Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
            self.rate3Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
            self.rate4Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
            self.rate5Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
        } else if index == 3 {
            self.rate1Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
            self.rate2Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
            self.rate3Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
            self.rate4Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
            self.rate5Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
        } else if index == 4 {
            self.rate1Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
            self.rate2Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
            self.rate3Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
            self.rate4Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
            self.rate5Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
        } else if index == 5{
            self.rate1Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
            self.rate2Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
            self.rate3Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
            self.rate4Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
            self.rate5Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
        }
    }
}

// MARK: - APIs
extension FeedbackVC {
    private func validate() -> [String: Any]? {
        //'user_id', 'event_id','rating','message'
        if self.descriptionTF.text?.count == 0 {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please entern some comments")
            return nil
        }
        var params = [String: Any]()
        params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"
        params["event_id"] = self.selectedEvent?.eventId ?? "0"
        params["rating"] = "\(selectedRate)"
        params["message"] = self.descriptionTF.text ?? ""
        return params
    }
    private func executeAPIForFeedback(params: [String: Any]) {
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.feedback(params: params) { (isSuccess, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                if isSuccess {
                    self.navigationController?.popViewController(animated: true)
                }
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
            }
        }
    }
}
