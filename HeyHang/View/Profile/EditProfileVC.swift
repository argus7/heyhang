//
//  EditProfileVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 5/28/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

class EditProfileVC: UIViewController {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var hdrLbl: UILabel!
    @IBOutlet weak var profileIV: UIImageView!
    @IBOutlet weak var nameTF: MATextField!
    @IBOutlet weak var emailTF: MATextField!
    @IBOutlet weak var mobileTF: MATextField!
    @IBOutlet weak var bioPlaceholderLbl: UILabel!
    @IBOutlet weak var bioTV: UITextView!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var bioStatusLbl: UILabel!
    @IBOutlet weak var profileBtn: UIButton!
    
    private let textViewPlaceholder: String = "Bio description..."
    private let imagePicker =  UIImagePickerController()
    private var updatedImageUrl: String = ""
    var selectedProfile: ProfileViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupTextField()
        updatePreviousProfileDetails()
    }
    override func viewWillLayoutSubviews() {
        self.profileIV.layer.cornerRadius = self.profileIV.bounds.height / 2
        self.profileIV.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.6).cgColor
        self.profileIV.layer.borderWidth = 3.0
        self.profileIV.clipsToBounds = true
        self.saveBtn.layer.cornerRadius = Constant.kCornerRadius
        self.headerView.setSideDropShadow(isUp: false)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func updatePreviousProfileDetails() {
        self.updatedImageUrl = selectedProfile?.image ?? ""
        self.profileIV.imageFromServerURL(updatedImageUrl, placeHolder: UIImage(named: "profile_placeholder"))
        self.nameTF.text = selectedProfile?.name
        self.emailTF.text = selectedProfile?.email ?? ""
        self.mobileTF.text = selectedProfile?.mobile
        
        self.bioTV.text = textViewPlaceholder
        self.bioTV.textColor = UIColor.lightGray
        self.bioPlaceholderLbl.isHidden = true
        
        if let bio = selectedProfile?.bio {
            if bio.count > 0 {
                self.bioTV.text = bio
            }
        }
        
        if self.selectedProfile?.loginType == 2 { //Email
            self.emailTF.isEnabled = false
            self.mobileTF.isEnabled = true
        } else if self.selectedProfile?.loginType == 3 { //Social
            self.emailTF.isEnabled = false
            self.mobileTF.isEnabled = true
        } else { //Mobile
            self.emailTF.isEnabled = true
            self.mobileTF.isEnabled = false
        }
    }
    
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickSaveBtn(_ sender: Any) {
        if checkInternet() {
            if let params = validateParams() {
                self.executeUpdateProfileDataApi(params: params)
            }
        }
    }
    @IBAction func onClickProfileCameraBtn(_ sender: Any) {
        print("Profile Camera Btn")
        self.imagePicker.delegate =  self
        showImageSelectionAlert(picker: imagePicker)
    }
}
extension EditProfileVC : UITextFieldDelegate, UITextViewDelegate {
    
    private func setupTextField() {
        self.nameTF.delegate = self
        self.emailTF.delegate = self
        self.mobileTF.delegate = self
        self.bioTV.delegate = self
        self.nameTF.bottomLbl?.backgroundColor = UIColor.lightGray
        self.emailTF.bottomLbl?.backgroundColor = UIColor.lightGray
        self.mobileTF.bottomLbl?.backgroundColor = UIColor.lightGray
        self.bioStatusLbl.backgroundColor = UIColor.lightGray
        self.bioPlaceholderLbl.isHidden = true
        self.bioTV.text = textViewPlaceholder
        self.bioTV.textColor = UIColor.lightGray
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.nameTF {
            self.nameTF.bottomLbl?.backgroundColor = UIColor.darkText
        } else if textField == self.emailTF {
            self.emailTF.bottomLbl?.backgroundColor = UIColor.darkText
        } else if textField == self.mobileTF {
            self.mobileTF.bottomLbl?.backgroundColor = UIColor.darkText
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.nameTF {
            self.nameTF.bottomLbl?.backgroundColor = UIColor.lightGray
        } else if textField == self.emailTF {
            self.emailTF.bottomLbl?.backgroundColor = UIColor.lightGray
        } else if textField == self.mobileTF {
            self.mobileTF.bottomLbl?.backgroundColor = UIColor.lightGray
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if bioTV.text == textViewPlaceholder {
            self.bioTV.text = ""
        }
        self.bioTV.textColor = UIColor.darkText
        self.bioPlaceholderLbl.isHidden = false
        self.bioPlaceholderLbl.textColor = UIColor.purpleSelected
        self.bioStatusLbl.backgroundColor = UIColor.darkText
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if bioTV.text == "" {
            self.bioTV.text = textViewPlaceholder
            self.bioTV.textColor = UIColor.lightGray
            self.bioPlaceholderLbl.isHidden = true
        } else {
            self.bioPlaceholderLbl.isHidden = false
            self.bioPlaceholderLbl.textColor = UIColor.darkGray
            self.bioTV.textColor = UIColor.darkText
        }
        self.bioStatusLbl.backgroundColor = UIColor.lightGray
    }
}
// MARK: - UIImagePickerControllerDelegate, UINavigationControllerDelegate
extension EditProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            if let imgData = pickedImage.jpegData(compressionQuality: 0.4) {
                self.profileIV.image = pickedImage
                self.executeUploadImageApi(imgData: imgData)
            }
        }
        dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
}

// MARK: - APIs
extension EditProfileVC {
    
    private func validateParams() -> [String: Any]? {
        guard let name  = self.nameTF.text, !(self.nameTF.text?.isEmpty)!  else {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter your name")
            return nil
        }
        guard let email  = self.emailTF.text, !(self.emailTF.text?.isEmpty)!  else {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter your email")
            return nil
        }
        if !self.isValidEmail(email) {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter correct email")
            return nil
        }
        guard let mobile  = self.mobileTF.text, !(self.mobileTF.text?.isEmpty)!  else {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter your mobile number")
            return nil
        }
        if mobile.count < 8 || (mobile.count > 14) {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter correct phone number")
            return nil
        }
        guard let bioStr  = self.bioTV.text, !(self.bioTV.text == textViewPlaceholder)  else {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter bio description")
            return nil
        }
        var params = [String: Any]()
        params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"
        params["full_name"] = name
        params["bio"] = bioStr
        params["profile_image"] = self.updatedImageUrl
        params["email"] = email
        params["mobile"] = mobile
        params["login_type"] = "\(selectedProfile?.loginType ?? 1)"
        return params
        //'user_id','full_name','bio','profile_image','login_type'
    }
    
    private func executeUpdateProfileDataApi(params: [String: Any]) {
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.updateProfile(params: params) { (profile, errorMessage, errorCode) in
            self.hideActivityIndicator(uiView: self.view)
            if errorCode != 100 {
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "")
            } else {
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    private func executeUploadImageApi(imgData: Data) {
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.uploadImage(imgData: imgData) { (imageUrl, errorMessage, errorCode) in
            self.hideActivityIndicator(uiView: self.view)
            if errorCode != 100 {
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "")
            } else {
                self.updatedImageUrl = imageUrl ?? ""
            }
        }
    }
}
