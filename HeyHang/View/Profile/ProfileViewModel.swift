//
//  ProfileViewModel.swift
//  HeyHang
//
//  Created by Mohd Arsad on 11/06/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import Foundation

class ProfileViewModel: NSObject {
    
    var friendList: [User] = []
    var name: String?
    var email: String?
    var mobile: String?
    var image: String?
    var bio: String?
    var attendedEvent: Int = 0
    var rating: Int = 0
    var loginType: Int = 1
    
    var postNotification: Bool = true
    var inviteNotification: Bool = true
    var startNotification: Bool = true
    var friendNotification: Bool = true
    
    init(json: [String: Any]) {
        self.name = json["full_name"] as? String ?? ""
        self.email = json["email"] as? String ?? ""
        self.mobile = json["mobile"] as? String ?? ""
        self.image = json["profile_image"] as? String ?? ""
        self.bio = json["bio"] as? String ?? ""
        self.attendedEvent = Int("\(json["attendedEvent"] ?? "0")") ?? 0
        self.rating = Int("\(json["rating"] ?? "0")") ?? 0
        self.loginType = Int("\(json["login_type"] ?? "0")") ?? 0
        
        if let friends = json["friends"] as? [[String: Any]] {
            self.friendList = friends.map({ return User(jsonData: $0)})
        }
        if let settings = json["settings"] as? [String: Any] {
            self.postNotification = (settings["post_notification"] as? String ?? "") == "1" ? true : false
            self.inviteNotification = (settings["invitation_notification"] as? String ?? "") == "1" ? true : false
            self.startNotification = (settings["before_start_notification"] as? String ?? "") == "1" ? true : false
            self.friendNotification = (settings["friends_notification"] as? String ?? "") == "1" ? true : false
        }
    }
    func resetViewsData(view: MyProfileVC) {
        
        view.profileIV.imageFromServerURL(self.image ?? "", placeHolder: UIImage(named: "profile_placeholder"))
        view.attendedEventCountLbl.text = "\(self.attendedEvent)"
        view.emailLbl.text = self.email ?? ""
        view.mobileLbl.text = self.mobile
        view.bioLbl.text = self.bio
        view.nameLbl.text = self.name ?? ""
        view.setRate(by: self.rating)
        view.friendCV.reloadData()
        
        UserDefaults.standard.set(self.postNotification, forKey: Constant.userDefaults.kNotiPostNoti)
        UserDefaults.standard.set(self.inviteNotification, forKey: Constant.userDefaults.kNotiInviteNoti)
        UserDefaults.standard.set(self.startNotification, forKey: Constant.userDefaults.kNotiStartNoti)
        UserDefaults.standard.set(self.friendNotification, forKey: Constant.userDefaults.kNotiFriendNoti)
    }
}
