//
//  MyProfileVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 5/28/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

class FriendCollCell: UICollectionViewCell {
    
    @IBOutlet weak var profileIV: UIImageView!
    @IBOutlet weak var moreLbl: UILabel!
    
    var user: User? {
        didSet {
            updateDetails()
        }
    }
    private func updateDetails() {
        self.profileIV.imageFromServerURL(user?.image ?? "", placeHolder: UIImage(named: "profile_placeholder"))
    }
}

class MyProfileVC: UIViewController {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var hdrLbl: UILabel!
    
    @IBOutlet weak var profileIV: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var attendedEventCountLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var mobileLbl: UILabel!
    @IBOutlet weak var bioLbl: UILabel!
    @IBOutlet weak var firstCardView: UIView!
    @IBOutlet weak var secondCardView: UIView!
    @IBOutlet weak var friendCV: UICollectionView!
    
    @IBOutlet weak var rate1Btn: UIButton!
    @IBOutlet weak var rate2Btn: UIButton!
    @IBOutlet weak var rate3Btn: UIButton!
    @IBOutlet weak var rate4Btn: UIButton!
    @IBOutlet weak var rate5Btn: UIButton!
    
    private var profileData: ProfileViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.nameLbl.text = ""
        self.emailLbl.text = ""
        self.bioLbl.text = ""
        self.mobileLbl.text = ""
        self.setRate(by: 0)
    }
    override func viewWillAppear(_ animated: Bool) {
        if checkInternet() {
            self.executeGetProfileDataApi()
        }
    }
    override func viewWillLayoutSubviews() {
        self.firstCardView.layer.cornerRadius = 15.0
        self.secondCardView.layer.cornerRadius = 15.0
        self.attendedEventCountLbl.layer.cornerRadius = 7.0
        self.attendedEventCountLbl.clipsToBounds = true
        self.headerView.setSideDropShadow(isUp: false)
        
        self.profileIV.layer.cornerRadius = self.profileIV.bounds.height / 2
        self.profileIV.layer.borderColor = UIColor.white.withAlphaComponent(0.1).cgColor
        self.profileIV.layer.borderWidth = 3.0
        self.profileIV.clipsToBounds = true
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickEditBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
        vc.selectedProfile = self.profileData
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension MyProfileVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let adjustedCount = noOfAdjustedCell()
        let friends = self.profileData?.friendList.count ?? 0
        if adjustedCount < friends {
            return adjustedCount
        }
        return friends
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.friendCV.dequeueReusableCell(withReuseIdentifier: "FCell", for: indexPath) as! FriendCollCell
        cell.profileIV.layer.cornerRadius = cell.profileIV.bounds.height / 2
        cell.profileIV.clipsToBounds = true
        
        let adjustedCount = noOfAdjustedCell()
        let friends = self.profileData?.friendList.count ?? 0
        if adjustedCount < friends {
            if indexPath.row == (noOfAdjustedCell() - 1) {
                cell.moreLbl.isHidden = false
                cell.moreLbl.text = "\(friends - adjustedCount)+"
            } else {
                cell.moreLbl.isHidden = true
            }
        } else {
            cell.moreLbl.isHidden = true
        }
        cell.user = self.profileData?.friendList[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyFriendVC") as! MyFriendVC
        vc.navigation = .myFriend
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize.init(width: 40.0, height: 40.0)
        return size
    }
    private func noOfAdjustedCell() -> Int {
        
        var count: CGFloat = 240 * widthRatio
        count /= 50.0
        return Int(count)
        
    }
}

// MARK: - APIs
extension MyProfileVC {
    
    private func executeGetProfileDataApi() {
        self.showActivityIndicator(uiView: self.view)
        DispatchQueue.global(qos: .background).async {
            RestAPI.shared.getProfile { (profile, errorMessage, errorCode) in
                self.hideActivityIndicator(uiView: self.view)
                if errorCode != 100 {
                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "")
                } else {
                    DispatchQueue.main.async {
                        self.profileData = profile
                        self.profileData?.resetViewsData(view: self)
//                        self.updateProfileDetails(profile: profile!)
                    }
                }
            }
        }
    }
//    private func updateProfileDetails(profile: ProfileViewModel) {
//
//        self.profileData = profile
//        self.profileIV.imageFromServerURL(profile.image ?? "", placeHolder: UIImage(named: "profile_placeholder"))
//        self.attendedEventCountLbl.text = "\(profile.attendedEvent)"
//        self.emailLbl.text = profile.email ?? ""
//        self.mobileLbl.text = profile.mobile
//        self.bioLbl.text = profile.bio
//        self.nameLbl.text = profile.name ?? ""
//        self.setRate(by: profile.rating)
//        self.friendCV.reloadData()
//
//        UserDefaults.standard.set(profile.postNotification, forKey: Constant.userDefaults.kNotiPostNoti)
//        UserDefaults.standard.set(profile.inviteNotification, forKey: Constant.userDefaults.kNotiInviteNoti)
//        UserDefaults.standard.set(profile.startNotification, forKey: Constant.userDefaults.kNotiStartNoti)
//        UserDefaults.standard.set(profile.friendNotification, forKey: Constant.userDefaults.kNotiFriendNoti)
//
//    }
    func setRate(by index: Int) {
        
        if index == 0 {
            self.rate1Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
            self.rate2Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
            self.rate3Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
            self.rate4Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
            self.rate5Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
        } else if index == 1 {
            self.rate1Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
            self.rate2Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
            self.rate3Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
            self.rate4Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
            self.rate5Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
        } else if index == 2 {
            self.rate1Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
            self.rate2Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
            self.rate3Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
            self.rate4Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
            self.rate5Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
        } else if index == 3 {
            self.rate1Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
            self.rate2Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
            self.rate3Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
            self.rate4Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
            self.rate5Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
        } else if index == 4 {
            self.rate1Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
            self.rate2Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
            self.rate3Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
            self.rate4Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
            self.rate5Btn.setImage(UIImage.init(named: "star_normal"), for: .normal)
        } else if index == 5{
            self.rate1Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
            self.rate2Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
            self.rate3Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
            self.rate4Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
            self.rate5Btn.setImage(UIImage.init(named: "star_active"), for: .normal)
        }
    }
}

