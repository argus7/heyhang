//
//  PrivacyTermVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 5/28/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit
import WebKit

class PrivacyTermVC: UIViewController {
    
    enum Navigation {
        case privacy
        case terms
        case aboutUs
    }

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var hdrLbl: UILabel!
    @IBOutlet weak var contentLbl: UILabel!
    @IBOutlet weak var webView: WKWebView!
    
    var navigation: Navigation = .privacy
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        switch navigation {
        case .aboutUs:
            self.hdrLbl.text = "About Us"
            let urlStr = APIConstant.MainUrl + APIConstant.aboutUsUrl
            if let url = URL.init(string: urlStr) {
                let request = URLRequest(url: url)
                self.webView.load(request)
            }
            break
        case .privacy:
            self.hdrLbl.text = "Privacy Policy"
            let urlStr = APIConstant.MainUrl + APIConstant.privacyPolicyUrl
            if let url = URL.init(string: urlStr) {
                let request = URLRequest(url: url)
                self.webView.load(request)
            }
            break
        case .terms:
            self.hdrLbl.text = "Terms & Conditions"
            let urlStr = APIConstant.MainUrl + APIConstant.termsConditionUrl
            if let url = URL.init(string: urlStr) {
                let request = URLRequest(url: url)
                self.webView.load(request)
            }
            break
        }
    }
    override func viewWillLayoutSubviews() {
        self.headerView.setSideDropShadow(isUp: false)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension PrivacyTermVC : WKUIDelegate, WKNavigationDelegate {
    func webView(_ webView: WKWebView, shouldPreviewElement elementInfo: WKPreviewElementInfo) -> Bool {
        return true
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.showActivityIndicator(uiView: self.view)
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.hideActivityIndicator(uiView: self.view)
    }
}
/*
// MARK: - APIs
extension PrivacyTermVC {
    func getDataService() {
        let urlStr = Constant.base_URL + "getCmsData"
        var params = [String : Any]()
        params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? ""
        
        AppHelper.showActivityIndicator(self.view)
        Alamofire.request(urlStr, method: .post, parameters: params, encoding: URLEncoding.default, headers:getHeaderData()).responseJSON{ response in
            AppHelper.hideActivityIndicator(self.view)
            
            guard response.result.isSuccess else {
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: Localisator.localization("NetworkErrorMsg"))
                return
            }
            let jsonResponse =  try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String : Any]
            print(jsonResponse)
            let responseCode =  jsonResponse ["error_code"] as? Int ?? 99
            guard responseCode == 100 else {
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: jsonResponse["message"] as? String ?? "")
                return
            }
            if let data = jsonResponse["data"] as? [String : Any] {
                switch self.navigation {
                case .AboutUs:
                    if let aboutMainData = data["about"] as? [String : Any] {
                        if let aboutData = aboutMainData["about"] as? String {
                            self.webkitView.loadHTMLString(aboutData, baseURL: nil)
                        }
                    }
                    break
                case .FAQ:
                    
                    break
                case .PrivacyPolicy:
                    break
                case .TermsConditions:
                    if let aboutMainData = data["about"] as? [String : Any] {
                        if let aboutData = aboutMainData["terms"] as? String {
                            
                            self.webkitView.loadHTMLString(aboutData, baseURL: nil)
                        }
                    }
                    break
                }
            }
        }
    }
    private func executeAPIForCancelEvent(urlStr: String) {
        
        guard let event = self.selectedEvent else {
            return
        }
        var params = [String: Any]()
        params["event_id"] = event.eventId ?? "0"
        print(params)
        
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.cancelEvent(params: params) { (isSuccess, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                if isSuccess {
                    Sync.instance.leaveCalendarEvent(by: event)
                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Event Cancelled Successfully")
                    self.navigationController?.popViewController(animated: true)
                } else {
                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                }
            }
        }
    }
}
extension String {
    func utf8EncodedString()-> String {
        let messageData = self.data(using: .nonLossyASCII)
        let text = String(data: messageData!, encoding: .utf8)
        return text ?? ""
    }
}
*/
