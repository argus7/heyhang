//
//  ResetPasswordVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 5/27/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

class ResetPasswordVC: UIViewController {
    
    @IBOutlet weak var newPassTF: MATextField!
    @IBOutlet weak var confirmPassTF: MATextField!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var newPassHideBtn: UIButton!
    @IBOutlet weak var confirmPassHideBtn: UIButton!
    
    var userId: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.userId = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"
        setupTextField()
    }
    override func viewWillLayoutSubviews() {
        self.newPassTF.placeholderColor = UIColor.darkGray
        self.confirmPassTF.placeholderColor = UIColor.darkGray
        self.submitBtn.layer.cornerRadius = Constant.kCornerRadius
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func onClickSubmitBtn(_ sender: Any) {
        if checkInternet() {
            if let params = validateParams() {
                self.executeResetPasswordApi(params: params)
            }
        }
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickNewPasshHideBtn(_ sender: Any) {
        if newPassTF.isSecureTextEntry {
            newPassTF.isSecureTextEntry = false
            newPassHideBtn.setTitle("Hide", for: .normal)
        } else {
            newPassTF.isSecureTextEntry = true
            newPassHideBtn.setTitle("Show", for: .normal)
        }
    }
    @IBAction func onClickConfirmPassHideBtn(_ sender: Any) {
        if confirmPassTF.isSecureTextEntry {
            confirmPassTF.isSecureTextEntry = false
            confirmPassHideBtn.setTitle("Hide", for: .normal)
        } else {
            confirmPassTF.isSecureTextEntry = true
            confirmPassHideBtn.setTitle("Show", for: .normal)
        }
    }
}
// MARK: - Success PopUp Delegate Methods
extension ResetPasswordVC: SuccessPopUpDelegate {
    func onActionPerform(on type: SuccessPopUpNavigation) {
        self.moveToLogin()
    }
    //    private func moveToHome() {
    //        for controller: UIViewController in (self.navigationController?.viewControllers)! {
    //            if controller.isKind(of: LoginVC.self) {
    //                self.navigationController?.popToViewController(controller, animated: true)
    //                return
    //            }
    //        }
    //        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
    //        self.navigationController?.pushViewController(vc, animated: true)
    //    }
    private func moveToLogin() {
        for controller: UIViewController in (self.navigationController?.viewControllers)! {
            if controller.isKind(of: LoginVC.self) {
                self.navigationController?.popToViewController(controller, animated: true)
                return
            }
        }
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
// MARK: - Textfield Delegate Methods
extension ResetPasswordVC : UITextFieldDelegate {
    
    private func setupTextField() {
        self.newPassTF.delegate = self
        self.confirmPassTF.delegate = self
        self.newPassTF.bottomLbl?.backgroundColor = UIColor.lightGray
        self.confirmPassTF.bottomLbl?.backgroundColor = UIColor.lightGray
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.newPassTF {
            self.newPassTF.bottomLbl?.backgroundColor = UIColor.darkText
        } else if textField == self.confirmPassTF {
            self.confirmPassTF.bottomLbl?.backgroundColor = UIColor.darkText
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.newPassTF {
            self.newPassTF.bottomLbl?.backgroundColor = UIColor.lightGray
        } else if textField == self.confirmPassTF {
            self.confirmPassTF.bottomLbl?.backgroundColor = UIColor.lightGray
        }
    }
}

// MARK: - APIs
extension ResetPasswordVC {
    private func validateParams() -> [String: Any]? {
        
        guard let newPass  = self.newPassTF.text, !(self.newPassTF.text?.isEmpty)!  else {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter new password")
            return nil
        }
        if newPass.count < 8 {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Password should be minimum 8 character")
            return nil
        }
        guard let confirmPass  = self.confirmPassTF.text, !(self.confirmPassTF.text?.isEmpty)!  else {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter confirm password")
            return nil
        }
        guard (newPass == confirmPass) else {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Confirm password does not mached")
            return nil
        }
        var params = [String: Any]()
        // 'user_id','password'
        params["user_id"] = self.userId
        params["password"] = newPass
        return params
        
    }
    private func executeResetPasswordApi(params: [String: Any]) {
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.resetPassword(params: params) { (errorMessage, errorCode) in
            self.hideActivityIndicator(uiView: self.view)
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred, Please try again later")
            if errorCode == 100 {
                DispatchQueue.main.async {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SuccessPopUpVC") as! SuccessPopUpVC
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.navigation = .resetPassword
                    vc.delegate = self
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
}
