//
//  ChangePasswordVC.swift
//  HeyHang
//
//  Created by Mohd Arsad on 18/06/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {
    
    @IBOutlet weak var oldPassTF: MATextField!
    @IBOutlet weak var newPassTF: MATextField!
    @IBOutlet weak var confirmPassTF: MATextField!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var newPassHideBtn: UIButton!
    @IBOutlet weak var confirmPassHideBtn: UIButton!
    @IBOutlet weak var oldPassHideBtn: UIButton!
    
    var userId: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupTextField()
    }
    override func viewWillLayoutSubviews() {
        self.oldPassTF.placeholderColor = UIColor.darkGray
        self.newPassTF.placeholderColor = UIColor.darkGray
        self.confirmPassTF.placeholderColor = UIColor.darkGray
        self.submitBtn.layer.cornerRadius = Constant.kCornerRadius
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func onClickSubmitBtn(_ sender: Any) {
        if checkInternet() {
            if let params = validateParams() {
                self.executeChangePasswordApi(params: params)
            }
        }
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickOldPasshHideBtn(_ sender: Any) {
        if oldPassTF.isSecureTextEntry {
            oldPassTF.isSecureTextEntry = false
            oldPassHideBtn.setTitle("Hide", for: .normal)
        } else {
            oldPassTF.isSecureTextEntry = true
            oldPassHideBtn.setTitle("Show", for: .normal)
        }
    }
    @IBAction func onClickNewPasshHideBtn(_ sender: Any) {
        if newPassTF.isSecureTextEntry {
            newPassTF.isSecureTextEntry = false
            newPassHideBtn.setTitle("Hide", for: .normal)
        } else {
            newPassTF.isSecureTextEntry = true
            newPassHideBtn.setTitle("Show", for: .normal)
        }
    }
    @IBAction func onClickConfirmPassHideBtn(_ sender: Any) {
        if confirmPassTF.isSecureTextEntry {
            confirmPassTF.isSecureTextEntry = false
            confirmPassHideBtn.setTitle("Hide", for: .normal)
        } else {
            confirmPassTF.isSecureTextEntry = true
            confirmPassHideBtn.setTitle("Show", for: .normal)
        }
    }
}
// MARK: - Success PopUp Delegate Methods
extension ChangePasswordVC: SuccessPopUpDelegate {
    func onActionPerform(on type: SuccessPopUpNavigation) {
        self.navigationController?.popViewController(animated: true)
    }
//    private func moveToLogin() {
//        UserDefaults.standard.setValue("No", forKey: Constant.userDefaults.kIsRegistered)
//        for controller: UIViewController in (self.navigationController?.viewControllers)! {
//            if controller.isKind(of: LoginVC.self) {
//                self.navigationController?.popToViewController(controller, animated: true)
//                return
//            }
//        }
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//        self.navigationController?.pushViewController(vc, animated: true)
//    }
}
// MARK: - Textfield Delegate Methods
extension ChangePasswordVC : UITextFieldDelegate {
    
    private func setupTextField() {
        self.oldPassTF.delegate = self
        self.newPassTF.delegate = self
        self.confirmPassTF.delegate = self
        self.newPassTF.bottomLbl?.backgroundColor = UIColor.lightGray
        self.confirmPassTF.bottomLbl?.backgroundColor = UIColor.lightGray
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.oldPassTF {
            self.oldPassTF.bottomLbl?.backgroundColor = UIColor.darkText
        } else if textField == self.newPassTF {
            self.newPassTF.bottomLbl?.backgroundColor = UIColor.darkText
        } else if textField == self.confirmPassTF {
            self.confirmPassTF.bottomLbl?.backgroundColor = UIColor.darkText
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.oldPassTF {
            self.oldPassTF.bottomLbl?.backgroundColor = UIColor.lightGray
        } else if textField == self.newPassTF {
            self.newPassTF.bottomLbl?.backgroundColor = UIColor.lightGray
        } else if textField == self.confirmPassTF {
            self.confirmPassTF.bottomLbl?.backgroundColor = UIColor.lightGray
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
            return true
        }
        if string == " " {
            return false
        }
        return true
    }
}

// MARK: - APIs
extension ChangePasswordVC {
    private func validateParams() -> [String: Any]? {
        
        guard let oldPass  = self.oldPassTF.text, !(self.oldPassTF.text?.isEmpty)!  else {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter old password")
            return nil
        }
        guard let newPass  = self.newPassTF.text, !(self.newPassTF.text?.isEmpty)!  else {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter new password")
            return nil
        }
        if newPass.count < 8 {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Password should be minimum 8 character")
            return nil
        }
        guard let confirmPass  = self.confirmPassTF.text, !(self.confirmPassTF.text?.isEmpty)!  else {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter confirm password")
            return nil
        }
        guard (newPass == confirmPass) else {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Confirm password does not mached")
            return nil
        }
        var params = [String: Any]()
        // user_id','old_password','new_password'
        params["user_id"] = self.userId
        params["old_password"] = oldPass
        params["new_password"] = newPass
        return params
        
    }
    private func executeChangePasswordApi(params: [String: Any]) {
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.changePassword(params: params) { (errorMessage, errorCode) in
            self.hideActivityIndicator(uiView: self.view)
            if errorCode == 100 {
                DispatchQueue.main.async {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SuccessPopUpVC") as! SuccessPopUpVC
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.navigation = .changePassword
                    vc.delegate = self
                    self.present(vc, animated: true, completion: nil)
                }
            } else {
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred, Please try again later")
            }
        }
    }
}
