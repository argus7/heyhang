//
//  EventDetailVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 6/4/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit
import UberCore
import UberRides
import CoreLocation
import LyftSDK

class EventDetailVC: UIViewController {
    
    enum Navigation {
        case home
        case myHang
        case booked
    }
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var hdrLbl: UILabel!
    @IBOutlet weak var rideBtn: UIButton!
    @IBOutlet weak var liveConcertView: UIView!
    
    @IBOutlet weak var mainBackIV: UIImageView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var profileEventImgView: UIImageView!
    @IBOutlet weak var eventNameLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var paidEventOutlet: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var startDateTimeLbl: UILabel!
    @IBOutlet weak var endDateTimeLbl: UILabel!
    @IBOutlet weak var availableTicketsLbl: UILabel!
    @IBOutlet weak var ticketCostIV: UIImageView!
    @IBOutlet weak var ticketCostLbl: UILabel!
    
    @IBOutlet weak var eventDescView: UIView!
    @IBOutlet weak var eventDescriptionLbl: UILabel!
    @IBOutlet weak var cancelEditView: UIView!
    @IBOutlet weak var eventRequestBtn: UIButton!
    @IBOutlet weak var eventRequestBtnHeightConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var firstImgView: UIImageView!
    @IBOutlet weak var firstMemberBtn: UIButton!
    @IBOutlet weak var secondImgView: UIImageView!
    @IBOutlet weak var secondMemberBtn: UIButton!
    @IBOutlet weak var thirdImgView: UIImageView!
    @IBOutlet weak var thirdMemberBtn: UIButton!
    @IBOutlet weak var fourthImgView: UIImageView!
    @IBOutlet weak var moreUserBtn: UIButton!
    @IBOutlet weak var bookTicketBtnOutlet: UIButton!
    
    @IBOutlet weak var editEventBtnOutlet: UIButton!
    @IBOutlet weak var cancelEventBtnOutlet: UIButton!
    
    @IBOutlet weak var cancelEditViewHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var cancelBtnWidthConstraints: NSLayoutConstraint!
    @IBOutlet weak var editBtnLeadingConstraints: NSLayoutConstraint!
    
    var navigation: Navigation = .home
    var isFreeEvent: Bool = false
    var selectedEvent: Event?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        eventRequestBtn.setAttributedTitle(eventRequestBtn.getUnderlineAttributeString(UIColor.purpleSelected), for: .normal)
        self.eventRequestBtnHeightConstraints.constant = 0.0
        self.cancelBtnWidthConstraints.constant = 130.0 * widthRatio
        self.editBtnLeadingConstraints.constant = 20.0
        self.eventRequestBtn.isHidden = true
        if self.navigation == .home {
            self.cancelEditViewHeightConstraints.constant = 0
            self.cancelEditView.isHidden = true
        } else if self.navigation == .booked {
            self.cancelEditViewHeightConstraints.constant = 0
            self.cancelEditView.isHidden = true
            
            let currentTI = Date().timeIntervalSince1970
            guard let event = self.selectedEvent else { return }
            guard let startTI = event.startDateTimeInterval else { return }
            if startTI > currentTI {
                self.rideBtn.isHidden = false
                self.bookTicketBtnOutlet.isHidden = false
            } else {
                self.rideBtn.isHidden = true
                self.bookTicketBtnOutlet.isHidden = true
            }
            
        } else {
            self.bookTicketBtnOutlet.isHidden = true
            self.rideBtn.isHidden = true
            if let event  = self.selectedEvent {
                if event.type == .paid {
                    self.cancelBtnWidthConstraints.constant = 0.0
                    self.editBtnLeadingConstraints.constant = 0.0
                }
                if let startDateTimeInterval  = event.startDateTimeInterval {
                    let currentDateTimeInterval = Date().timeIntervalSince1970
                    if currentDateTimeInterval > startDateTimeInterval {
                        self.cancelEditViewHeightConstraints.constant = 0
                        self.cancelEditView.isHidden = true
                    }
                }
            }
        }
        self.refreshServerData()
    }
    override func viewWillAppear(_ animated: Bool) {
        if checkInternet() {
            self.executeApiForGetHangDetails()
        }
    }
    
    func setUpView(){
        profileEventImgView.layer.cornerRadius = profileEventImgView.frame.size.width/2
        profileEventImgView.clipsToBounds = true
        profileEventImgView.layer.borderColor = UIColor.white.cgColor
        profileEventImgView.layer.borderWidth = 2.0
        firstImgView.layer.cornerRadius = firstImgView.frame.size.width/2
        firstImgView.clipsToBounds = true
        firstImgView.layer.borderColor = UIColor.white.cgColor
        firstImgView.layer.borderWidth = 1.0
        secondImgView.layer.cornerRadius = secondImgView.frame.size.width/2
        secondImgView.clipsToBounds = true
        secondImgView.layer.borderColor = UIColor.white.cgColor
        secondImgView.layer.borderWidth = 1.0
        thirdImgView.layer.cornerRadius = thirdImgView.frame.size.width/2
        thirdImgView.clipsToBounds = true
        thirdImgView.layer.borderColor = UIColor.white.cgColor
        thirdImgView.layer.borderWidth = 1.0
        fourthImgView.layer.cornerRadius = fourthImgView.frame.size.width/2
        fourthImgView.clipsToBounds = true
        fourthImgView.layer.borderColor = UIColor.white.cgColor
        fourthImgView.layer.borderWidth = 1.0
        
        editEventBtnOutlet.layer.cornerRadius = 5.0
        editEventBtnOutlet.clipsToBounds = true
        paidEventOutlet.layer.cornerRadius = 5.0
        paidEventOutlet.clipsToBounds = true
        
        cancelEventBtnOutlet.layer.cornerRadius = 5.0
        cancelEventBtnOutlet.clipsToBounds = true
        cancelEventBtnOutlet.layer.borderColor = UIColor.red.cgColor
        cancelEventBtnOutlet.layer.borderWidth = 1.0
        
        bookTicketBtnOutlet.layer.cornerRadius = bookTicketBtnOutlet.frame.size.height/2
        bookTicketBtnOutlet.clipsToBounds = true
        
    }
    override func viewWillLayoutSubviews() {
        self.headerView.setSideDropShadow(isUp: false)
        self.gradientView.setBottomToTopGradient(cornerRadius: 0.0, firstColor: .clear, secoundColor: UIColor(red: 0, green: 0, blue: 0, alpha: 0.8))
        setUpView()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func editEventBtnAction(_ sender: UIButton) {
        print("Edit Event")
        guard let event = self.selectedEvent else { return }
        guard let startDateTimeInterval = event.startDateTimeInterval else { return }
        let currentDateTimeInterval = Date().timeIntervalSince1970
        if currentDateTimeInterval < startDateTimeInterval {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateHangMainVC") as! CreateHangMainVC
            vc.selectedEvent = event
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Your hang was expired")
        }
    }
    @IBAction func cancelEventBtnAction(_ sender: UIButton) {
        if let event = self.selectedEvent {
            if event.type == .paid {
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "You can not cancel paid event.")
            } else {
                cancelEventConfirmAlert()
            }
        }
    }
    @IBAction func onClickMoreUserBtn(_ sender: Any) {
        showAllMemberDetails()
    }
    @IBAction func onClickMember1Btn(_ sender: Any) {
        showAllMemberDetails()
    }
    @IBAction func onClickMember2Btn(_ sender: Any) {
        showAllMemberDetails()
    }
    @IBAction func onClickMember3Btn(_ sender: Any) {
        showAllMemberDetails()
    }
    @IBAction func bookTicketBtnAction(_ sender: UIButton) {
        if let event = self.selectedEvent {
            if event.type == .paid {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookTicketsVC") as! BookTicketsVC
                vc.selectedEvent = self.selectedEvent
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                if checkInternet() {
                    if event.status {
                        self.eventActionConfirmAlert(isJoin: false, index: 0)
                    } else {
                        self.eventActionConfirmAlert(isJoin: true, index: 0)
                    }
                }
            }
        }
    }
    @IBAction func onClickRideBtn(_ sender: Any) {
        
        let pickupLocation = CLLocation(latitude: 37.787654, longitude: -122.402760)
        let dropoffLocation = CLLocation(latitude: 37.775200, longitude: -122.417587)
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailPopUpVC") as! EventDetailPopUpVC
        vc.delegate = self
        vc.modalPresentationStyle = .overCurrentContext
        vc.sourceLocation = pickupLocation
        vc.destinationLocation = dropoffLocation
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func onClickEventRequestsBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AllMembersVC") as! AllMembersVC
        vc.navigation = .allRequest
        vc.selectedEvent = self.selectedEvent
        self.navigationController?.pushViewController(vc, animated: true)
    }
    private func showAllMemberDetails() {
        if let friends = self.selectedEvent?.member {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AllMembersVC") as! AllMembersVC
            vc.navigation = .eventDetail
            vc.userList = friends
            vc.selectedEvent = self.selectedEvent
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
extension EventDetailVC: BookRideDelegate {
    func onSuccessRideSelection(type: RideType) {
        switch type {
        case .ola:
//            self.setupLyft()
            break
        case .uber:
//            self.launchUberSetup()
            break
        case .jugnoo:
            break
        case .meru:
            break
        }
    }
  /*  private func launchUberSetup() {
        let loginManager = LoginManager()
        loginManager.login(requestedScopes:[.request], presentingViewController: self, completion: { accessToken, error in
            // Completion block. If accessToken is non-nil, you’re good to go
            // Otherwise, error.code corresponds to the RidesAuthenticationErrorType that occured
            guard let _ = accessToken else {
                print("Error: Access Token Not Found")
                return
            }
            self.requestRide()
        })
    }
    private func requestRide() {
        let ridesClient = RidesClient()
        let pickupLocation = CLLocation(latitude: 37.787654, longitude: -122.402760)
        let dropoffLocation = CLLocation(latitude: 37.775200, longitude: -122.417587)
        let builder = RideParametersBuilder()
        builder.pickupLocation = pickupLocation
        builder.dropoffLocation = dropoffLocation
        
        ridesClient.fetchProducts(pickupLocation: pickupLocation) { products, response in
            guard let uberX = products.filter({$0.productGroup == .uberX}).first else {
                // Handle error, UberX does not exist at this location
                return
            }
            builder.productID = uberX.productID
            ridesClient.fetchRideRequestEstimate(parameters: builder.build(), completion: { rideEstimate, response in
                guard let rideEstimate = rideEstimate else {
                    // Handle error, unable to get an ride request estimate
                    return
                }
                builder.upfrontFare = rideEstimate.fare
                ridesClient.requestRide(parameters: builder.build(), completion: { ride, response in
                    guard let ride = ride else {
                        // Handle error, unable to request ride
                        return
                    }
                    // Ride has been requested!
                })
            })
        }
    }
    private func setupLyft() {
//        let pickup = CLLocationCoordinate2D(latitude: 28.6210, longitude: 77.3812)
//        let destination = CLLocationCoordinate2D(latitude: 28.6304, longitude: 77.2177)
        
        let pickup = CLLocationCoordinate2D(latitude: 37.7833, longitude: -122.4167)
        let destination = CLLocationCoordinate2D(latitude: 37.7794703, longitude: -122.4233223)
        LyftDeepLink.requestRide(using: .native, kind: .Standard, from: pickup, to: destination, couponCode: nil)
    }
    */
}

// MARK: - APIs
extension EventDetailVC {
    private func executeApiForGetHangDetails() {
        self.showActivityIndicator(uiView: self.view)
        let params = ["event_id" : "\(self.selectedEvent?.eventId ?? "0")","user_id" : (UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0")]
        RestAPI.shared.getEventDetails(params: params) { (event, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                if errorCode == 100 {
                    if let event = event {
                        self.selectedEvent = event
                        self.refreshServerData()
                    }
                }
            }
        }
    }
    private func refreshServerData() {
        if let event = self.selectedEvent {
            
            //Owner Details
            self.profileEventImgView.imageFromServerURL(event.ownerImage ?? "", placeHolder: UIImage(named: "profile_placeholder"))
            self.userNameLbl.text = event.ownerName ?? ""
            
            //Event Details
            let imageUrl = event.imageArr.last?.url ?? ""
            self.mainBackIV.imageFromServerURL(imageUrl, placeHolder: nil)
            self.eventNameLbl.text = event.name ?? ""
            self.locationLbl.text = event.location ?? ""
            self.availableTicketsLbl.text = "\(event.seatAvailable) Available"
            self.ticketCostLbl.text = "\(Constant.Currency)\(event.ticketPrice ?? "0")"
            self.eventDescriptionLbl.text = event.descriptionStr
            
            let startDate = Date(timeIntervalSince1970: event.startDateTimeInterval ?? 0.0)
            let endDate = Date(timeIntervalSince1970: event.endDateTimeInterval ?? 0.0)
            let formatter = DateFormatter()
            formatter.dateFormat = "dd MMM | hh:mm a"
            let startDateTime = formatter.string(from: startDate)
            let endDateTime = formatter.string(from: endDate)
            
            self.startDateTimeLbl.text = startDateTime
            self.endDateTimeLbl.text = endDateTime
            
            if event.type == .free {
                self.paidEventOutlet.backgroundColor = UIColor.eventTypePurple
                self.paidEventOutlet.text = "Free Hang"
                if event.status {
                    self.bookTicketBtnOutlet.setTitle("Leave", for: .normal)
                } else {
                    self.bookTicketBtnOutlet.setTitle("Join", for: .normal)
                }
                if event.privacy == "4" { //Auto Accept
                    self.bookTicketBtnOutlet.isHidden = true
                }
                if event.privacy == "3" { //Only Public
                    if self.navigation == .myHang {
                        self.eventRequestBtnHeightConstraints.constant = 70.0
                        self.eventRequestBtn.isHidden = false
                    }
                }
                self.ticketCostIV.isHidden = true
                self.ticketCostLbl.isHidden = true
            } else{
                if self.navigation == .booked || event.status {
                    self.bookTicketBtnOutlet.isHidden = true
                }
                self.bookTicketBtnOutlet.setTitle("Book Tickets", for: .normal)
                self.paidEventOutlet.backgroundColor = UIColor.eventTypeYellow
                self.paidEventOutlet.text = "Paid Hang"
            }
            
            if event.member.count > 4 {
                let remain = event.member.count - 4
                self.moreUserBtn.setTitle("\(remain)+", for: .normal)
            } else {
                self.moreUserBtn.setTitle("", for: .normal)
            }
            
            if event.member.count > 3 {
                self.fourthImgView.isHidden = false
                self.fourthImgView.imageFromServerURL(event.member[3].image ?? "", placeHolder: UIImage(named: "profile_placeholder"))
                self.moreUserBtn.isHidden = false
            } else {
                self.fourthImgView.isHidden = true
                self.moreUserBtn.isHidden = true
            }
            if event.member.count > 2 {
                self.thirdImgView.isHidden = false
                self.thirdImgView.imageFromServerURL(event.member[2].image ?? "", placeHolder: UIImage(named: "profile_placeholder"))
                self.thirdMemberBtn.isHidden = false
            } else {
                self.thirdImgView.isHidden = true
                self.thirdMemberBtn.isHidden = true
            }
            if event.member.count > 1 {
                self.secondImgView.isHidden = false
                self.secondImgView.imageFromServerURL(event.member[1].image ?? "", placeHolder: UIImage(named: "profile_placeholder"))
                self.secondMemberBtn.isHidden = false
            } else {
                self.secondImgView.isHidden = true
                self.secondMemberBtn.isHidden = true
            }
            if event.member.count > 0 {
                self.firstImgView.isHidden = false
                self.firstImgView.imageFromServerURL(event.member[0].image ?? "", placeHolder: UIImage(named: "profile_placeholder"))
                self.firstMemberBtn.isHidden = false
            } else {
                self.firstImgView.isHidden = true
                self.firstMemberBtn.isHidden = true
            }
        }
    }
    private func executeAPIForJoinEvent() {
        
        guard let event = self.selectedEvent else {
            return
        }
        var params = [String: Any]()
        params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"
        params["event_id"] = event.eventId ?? "0"
        params["event_type"] = (event.type == .paid) ? "1" : "2"
        params["ticket_count"] = ""
        params["total_amount"] = ""
        print(params)
        
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.bookTicket(params: params) { (isSuccess, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                if isSuccess {
                    Sync.instance.joinCalendarEvent(by: event)
                    if event.privacy == "3" { //Public
                        self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                        self.executeApiForGetHangDetails()
                    } else {
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PaymentSuccessfullVC") as! PaymentSuccessfullVC
                        vc.type = .seat
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    
                } else {
                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                }
            }
        }
    }
    private func executeAPIForLeaveEvent() {
        
        guard let event = self.selectedEvent else {
            return
        }
        var params = [String: Any]()
        params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"
        params["event_id"] = event.eventId ?? "0"
        print(params)
        
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.leaveEventTicket(params: params) { (isSuccess, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                if isSuccess {
                    Sync.instance.leaveCalendarEvent(by: event)
                    self.selectedEvent?.status = false
                    self.bookTicketBtnOutlet.setTitle("Join", for: .normal)
                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Leave Successfully")
                } else {
                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                }
            }
        }
    }
    private func executeAPIForCancelEvent() {
        
        guard let event = self.selectedEvent else {
            return
        }
        var params = [String: Any]()
        params["event_id"] = event.eventId ?? "0"
        print(params)
        
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.cancelEvent(params: params) { (isSuccess, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                if isSuccess {
                    Sync.instance.leaveCalendarEvent(by: event)
                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Event Cancelled Successfully")
                    self.navigationController?.popViewController(animated: true)
                } else {
                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                }
            }
        }
    }
    private func cancelEventConfirmAlert() {
        let alertController = UIAlertController.init(title: Constant.APP_NAME, message: "Are you sure want to cancel this hang?", preferredStyle: .alert)
        alertController.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
        }))
        alertController.addAction(UIAlertAction.init(title: "Confirm", style: .destructive, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
            if self.checkInternet() {
                self.executeAPIForCancelEvent()
            }
        }))
        self.present(alertController, animated: true, completion: nil)
    }
    private func eventActionConfirmAlert(isJoin: Bool, index: Int) {
        let alertController = UIAlertController.init(title: Constant.APP_NAME, message: "Are you sure want to \(isJoin ? "join" : "leave") this hang?", preferredStyle: .alert)
        alertController.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
        }))
        alertController.addAction(UIAlertAction.init(title: "Confirm", style: .destructive, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
            if isJoin {
                self.executeAPIForJoinEvent()
            } else {
                self.executeAPIForLeaveEvent()
            }
        }))
        self.present(alertController, animated: true, completion: nil)
    }
}


