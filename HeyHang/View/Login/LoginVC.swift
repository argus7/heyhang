//
//  LoginVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 5/27/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit
import AKSideMenu
import GoogleSignIn

class LoginVC: UIViewController {
    
    @IBOutlet weak var emailTF: MATextField!
    @IBOutlet weak var passwordTF: MATextField!
    @IBOutlet weak var gmailView: UIView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var registerBtn: UIButton!
    
    private var socialDict : [String : Any]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupTextField()
        registerBtn.setAttributedTitle(registerBtn.getUnderlineAttributeString(), for: .normal)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.emailTF.text = ""
        self.passwordTF.text = ""
    }
    override func viewWillLayoutSubviews() {
        self.nextBtn.layer.cornerRadius = Constant.kCornerRadius
        self.gmailView.setLightDropShadow(cornerRadius: Constant.kCornerRadius)
    }
    
    @IBAction func onClickForgotPasswordBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onClickNextBtn(_ sender: Any) {
        if checkInternet() {
            if let params = validateParams() {
                self.executeLoginApi(params: params)
            }
        }
    }
    @IBAction func onClickGmailBtn(_ sender: Any) {
        print("Gmail Btn")
        if checkInternet() {
            self.gmailconfiguration()
        }
    }
    @IBAction func onClickRegisterBtn(_ sender: Any) {
        self.moveToSignUp()
    }
    private func moveToSignUp() {
        for controller: UIViewController in (self.navigationController?.viewControllers)! {
            if controller.isKind(of: StartedVC.self) {
                self.navigationController?.popToViewController(controller, animated: true)
                return
            }
        }
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "StartedVC") as! StartedVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension LoginVC : UITextFieldDelegate {
    
    private func setupTextField() {
        
        self.emailTF.delegate = self
        self.passwordTF.delegate = self
        self.emailTF.bottomLbl?.backgroundColor = UIColor.lightGray
        self.passwordTF.bottomLbl?.backgroundColor = UIColor.lightGray
        self.emailTF.placeholderColor = UIColor.darkGray
        self.passwordTF.placeholderColor = UIColor.darkGray
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.emailTF {
            self.emailTF.bottomLbl?.backgroundColor = UIColor.darkText
        } else if textField == self.passwordTF {
            self.passwordTF.bottomLbl?.backgroundColor = UIColor.darkText
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.emailTF {
            self.emailTF.bottomLbl?.backgroundColor = UIColor.lightGray
        } else if textField == self.passwordTF {
            self.passwordTF.bottomLbl?.backgroundColor = UIColor.lightGray
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
            return true
        }
        if textField == self.emailTF {
            if (self.emailTF.text?.isEmpty)! && ((string == "0") || (string == " ")) {
                return false
            } else if !isValidAlphaNumericWithEmail(string) {
                return false
            }
        }
        return true
    }
}
// MARK: - APIs
extension LoginVC {
    private func validateParams() -> [String: Any]? {
        guard let emailName  = self.emailTF.text, !(self.emailTF.text?.isEmpty)!  else {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter your email address/mobile number")
            return nil
        }
        if emailName.contains("@") {
            if !self.isValidEmail(emailName) {
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter correct email")
                return nil
            }
        } else {
            if emailName.count < 8 || (emailName.count > 14) {
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter correct phone number")
            }
        }
        guard let password  = self.passwordTF.text, !(self.passwordTF.text?.isEmpty)!  else {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter password")
            return nil
        }
        var params = [String: Any]()
        if emailName.contains("@") {
            params["email"] = emailName
            params["login_type"] = "2"
        } else {
            params["mobile"] = emailName
            params["login_type"] = "1"
        }
        params["device_type"] = "2"
        params["password"] = password
        params["device_id"] = UIDevice.current.identifierForVendor?.uuidString
        params["device_token"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kDeviceToken) ?? "qwertyuiop"
        return params
        
    }
    private func executeLoginApi(params: [String: Any]) {
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.login(params: params) { (userId, errorMessage, errorCode) in
            self.hideActivityIndicator(uiView: self.view)
            if errorCode != 100 {
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "")
                if errorCode == 101 {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "VerifyOTPVC") as! VerifyOTPVC
                    vc.userId = userId ?? "0"
                    let emailName = self.emailTF.text ?? ""
                    if emailName.contains("@") {
                        vc.navigation = .emailVerify
                    } else {
                        vc.navigation = .phoneVerify
                    }
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            } else {
                DispatchQueue.main.async {
                    let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                    let sideVC = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
                    let vc: AKSideMenu = AKSideMenu(contentViewController: homeVC, leftMenuViewController: sideVC, rightMenuViewController: nil)
                    vc.interactivePopGestureRecognizerEnabled = true
                    vc.panFromEdge = false
                    vc.bouncesHorizontally = false
                    vc.contentViewShadowEnabled = true
                    vc.contentViewShadowOpacity = 10.0
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    private func executeSocialApi() {
        var params: [String: Any] = [:]
        params["login_type"] = "3"
        params["email"] = self.socialDict["email"] as? String ?? ""
        params["full_name"] = self.socialDict["name"] as? String ?? ""
        params["social_id"] = self.socialDict["uid"] as? String ?? ""
        params["device_type"] = "2"
        params["device_id"] = UIDevice.current.identifierForVendor?.uuidString
        params["device_token"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kDeviceToken) ?? "qwertyuiop"
        print("Params: \(params)")
        
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.socialLogin(params: params) { (userId, errorMessage, errorCode) in
            self.hideActivityIndicator(uiView: self.view)
            if errorCode != 100 {
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "")
                if errorCode == 101 {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "VerifyOTPVC") as! VerifyOTPVC
                    vc.userId = userId ?? "0"
                    vc.navigation = .socialVerify
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            } else {
                DispatchQueue.main.async {
                    let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                    let sideVC = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
                    let vc: AKSideMenu = AKSideMenu(contentViewController: homeVC, leftMenuViewController: sideVC, rightMenuViewController: nil)
                    vc.interactivePopGestureRecognizerEnabled = true
                    vc.panFromEdge = false
                    vc.bouncesHorizontally = false
                    vc.contentViewShadowEnabled = true
                    vc.contentViewShadowOpacity = 10.0
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
}
extension LoginVC : GIDSignInUIDelegate, GIDSignInDelegate {
    
    private func gmailconfiguration(){
        let signIn:GIDSignIn = GIDSignIn.sharedInstance()
        signIn.delegate = self
        signIn.uiDelegate = self
        signIn.shouldFetchBasicProfile = true
        signIn.signIn()
    }
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        self.hideActivityIndicator(uiView: self.view)
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
        
        print("Sign in presented")
        
    }
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
        self.hideActivityIndicator(uiView: self.view)
        print("Sign in dismissed")
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        self.showActivityIndicator(uiView: self.view)
        if (error == nil) {
            
            let userId = user.userID
            let f_name = user.profile.givenName as String
            let LastName = user.profile.familyName as String
            let email = user.profile.email as String
            
            var socialDictT = [String : Any]()
            
            GIDSignIn.sharedInstance().signOut()
            socialDictT["uid"] = userId
            socialDictT["name"] = f_name + " " + LastName
            socialDictT["email"] = email
            socialDictT["password"] = ""
            self.socialDict = socialDictT
            print("Social Google Data is: \(socialDict!)")
            self.hideActivityIndicator(uiView: self.view)
            self.executeSocialApi()
        } else {
            print("\(error)")
            self.hideActivityIndicator(uiView: self.view)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user:GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
}
