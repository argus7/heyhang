//
//  HomeVC.swift
//  Ajjerlli
//
//  Created by Anup Kumar on 4/30/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit
import AKSideMenu
import GooglePlacePicker

class HomeVC: UIViewController {
    
    @IBOutlet weak var tableView: MATableView!
    @IBOutlet weak var yourLocationFixedLbl: UILabel!
    @IBOutlet weak var currentLocationLbl: UILabel!
    @IBOutlet weak var changeLocationBtn: UIButton!
    @IBOutlet weak var addEventBtn: UIButton!
    @IBOutlet weak var myEventBtn: UIButton!
    
    @IBOutlet weak var floatingMainPopUpView: UIView!
    @IBOutlet weak var floatingPopupView: UIView!
    
    private var eventList: [Event] = []
    private var isFilterActive: Bool = false
    private var filterParams: [String: Any] = [:]
    private var latitude: String = ""
    private var longitude: String = ""
    private var isExecuting: Bool = false
    private var isFirstExecuting: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.latitude = UserDefaults.standard.string(forKey: Constant.userDefaults.kLatitude) ?? "0.0"
        self.longitude = UserDefaults.standard.string(forKey: Constant.userDefaults.kLongitude) ?? "0.0"
        self.currentLocationLbl.text = UserDefaults.standard.string(forKey: Constant.userDefaults.kLocAddress) ?? ""
        setupTableView()
        if checkInternet() {
            self.executeAPIForEventList(isPaging: false)
        }
    }
    override func viewWillLayoutSubviews() {
        self.changeLocationBtn.layer.cornerRadius = 10.0
        self.addEventBtn.setDropShadow(cornerRadius: 27.5)
        self.addEventBtn.backgroundColor = UIColor.clear
        self.floatingPopupView.layer.cornerRadius = 20.0
        self.floatingPopupView.clipsToBounds = true
    }
    override func viewWillAppear(_ animated: Bool) {
        self.floatingMainPopUpView.alpha = 0.0
        self.floatingMainPopUpView.isHidden = true
        self.tableView.reloadData()
    }
    override func viewDidAppear(_ animated: Bool) {
        isFilterActive = false
        self.filterParams = [:]
        if isFirstExecuting {
            isFirstExecuting = false
            Sync.instance.createCalendar() //Create Heyhang calendar if its not in your mobile phone calendar.
            Sync.instance.syncContacts() //Upload contact to server for update your friend list automatically
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func onClickMenuBtn(_ sender: Any) {
        self.sideMenuViewController!.presentLeftMenuViewController()
    }
    @IBAction func onClickChatBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyFriendVC") as! MyFriendVC
        vc.navigation = .myChat
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onClickFilterBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        vc.delegate = self
        vc.filterParams = self.filterParams
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func onClickSearchBtn(_ sender: Any) {
        print("Search Btn")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyEventVC") as! MyEventVC
        vc.isMoveForSearch = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onClickChangeLocationBtn(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.coordinate.rawValue) | UInt(GMSPlaceField.formattedAddress.rawValue))!
        autocompleteController.placeFields = fields
        
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .noFilter
        autocompleteController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)
    }
    @IBAction func onClickLocateCurrentBtn(_ sender: Any) {
        LocationHelper.instance.getuserCurrentLattLong(delegate: self)
    }
    @IBAction func onClickMyEventEventHdrBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateHangMainVC") as! CreateHangMainVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onClickAddEventBtn(_ sender: Any) {
        if self.floatingMainPopUpView.isHidden {
            self.showFloating()
        } else {
            self.hideFloating()
        }
    }
    @IBAction func onClickNotificationBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onClickFloatingCreateEventBtn(_ sender: Any) {
        self.hideFloating()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateHangMainVC") as! CreateHangMainVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onClickFloatingCreateGroupBtn(_ sender: Any) {
        self.hideFloating()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyFriendVC") as! MyFriendVC
        vc.navigation = .myGroup
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onClickFloatingAddPalBtn(_ sender: Any) {
        self.hideFloating()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AllMembersVC") as! AllMembersVC
        vc.navigation = .userList
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onClickFloatingBackBtn(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: {
            self.floatingMainPopUpView.alpha = 0.0
            self.addEventBtn.transform = .identity
        }) { (isComplete) in
            self.floatingMainPopUpView.isHidden = true
        }
    }
    private func showFloating() {
        self.floatingMainPopUpView.alpha = 0.0
        self.floatingMainPopUpView.isHidden = false
        UIView.animate(withDuration: 0.3) {
            self.floatingMainPopUpView.alpha = 1.0
            self.addEventBtn.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi / 4))
        }
    }
    private func hideFloating() {
        UIView.animate(withDuration: 0.3, animations: {
            self.floatingMainPopUpView.alpha = 0.0
            self.addEventBtn.transform = .identity
        }) { (isComplete) in
            self.floatingMainPopUpView.isHidden = true
        }
    }
}
extension HomeVC: LocationHelperDelegate {
    func locationHelper(address: String) {
        self.currentLocationLbl.text = address
        UserDefaults.standard.setValue(address, forKey: Constant.userDefaults.kLocAddress)
    }
    func locationHelper(latitude: String, longitude: String) {
        self.latitude = latitude
        self.longitude = longitude
        
        self.yourLocationFixedLbl.text = "Your current location"
        UserDefaults.standard.setValue(latitude, forKey: Constant.userDefaults.kLatitude)
        UserDefaults.standard.setValue(longitude, forKey: Constant.userDefaults.kLongitude)
        print("Latitude: \(latitude), Longitude: \(longitude)")
        if !isExecuting {
            self.executeAPIForEventList(isPaging: false)
        }
    }
}
extension HomeVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.latitude = String(place.coordinate.latitude)
        self.longitude = String(place.coordinate.longitude)
        self.yourLocationFixedLbl.text = "Your location"
        self.currentLocationLbl.text = place.formattedAddress?.components(separatedBy: ", ")
            .joined(separator: ",") ?? ""
        print("Latitude: \(latitude), Longitude: \(longitude)")
        if !isExecuting {
            self.executeAPIForEventList(isPaging: false)
        }
        dismiss(animated: true, completion: nil)
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
extension HomeVC: FilterDelegate {
    func onFilterSelected(params: [String : Any]) {
        isFilterActive = true
        print("Filter Selected: \(params)")
        self.filterParams = params
        self.eventList = []
        self.tableView.reloadData()
        self.executeAPIForFilterEventList(isPaging: false)
    }
}
extension HomeVC {
    fileprivate func setupTableView() {
        self.tableView.register(UINib(nibName: "EventTblCell", bundle: Bundle.main), forCellReuseIdentifier: "EventTblCell")
        self.tableView.pullToRefreshDelegate = self
    }
}
extension HomeVC: UITableViewDelegate, UITableViewDataSource, UITableViewPullRefreshDelegate {
    func tableView(_ tableView: UITableView, pulltoRefreshStart: Bool) {
        self.filterParams = [:]
        self.eventList = []
        self.tableView.reloadData()
        if checkInternet() {
            self.executeAPIForEventList(isPaging: false)
        } else {
            self.tableView.reloadData()
            self.tableView.pullToRefreshControl?.endRefreshing()
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if eventList.count > 0 {
            self.tableView.backgroundView = nil
            return eventList.count
        } else {
            self.tableView.showBackgroundMsg(msg: "No Hang Found")
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "EventTblCell", for: indexPath) as! EventTblCell
        
        cell.event = eventList[indexPath.row]
        
        cell.detailBtn.tag = indexPath.row
        cell.detailBtn.addTarget(self, action: #selector(onClickDetailCellBtn(_:)), for: .touchUpInside)
        cell.joinBtn.tag = indexPath.row
        cell.joinBtn.addTarget(self, action: #selector(onClickJoinCellBtn(_:)), for: .touchUpInside)
        
        var frame = cell.gradientView.frame
        frame.size.width = UIScreen.main.bounds.width - 40
        frame.size.height = (250 * widthRatio) - 64.0
        cell.gradientView.frame = frame
        cell.gradientView.setBottomToTopGradient(cornerRadius: 0.0, firstColor: .clear, secoundColor: UIColor(red: 0, green: 0, blue: 0, alpha: 0.8))
        
        if (self.eventList.count) % 10 == 0 {
            if indexPath.row == (self.eventList.count - 3) {
                if isFilterActive {
                    self.executeAPIForFilterEventList(isPaging: true)
                } else {
                    self.executeAPIForEventList(isPaging: true)
                }
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250 * widthRatio
    }
    @objc func onClickDetailCellBtn(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailVC") as! EventDetailVC
        vc.navigation = .home
        vc.selectedEvent = eventList[sender.tag]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func onClickJoinCellBtn(_ sender: UIButton) {
        print("Join/Leave Btn")
        let event = eventList[sender.tag]
        if event.type == .paid {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookTicketsVC") as! BookTicketsVC
            vc.selectedEvent = event
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            if checkInternet() {
                if event.status {
                    self.eventActionConfirmAlert(isJoin: false, index: sender.tag)
                } else {
                    self.eventActionConfirmAlert(isJoin: true, index: sender.tag)
                }
            }
        }
    }
}

// MARK: - APIs Data
extension HomeVC {
    private func executeAPIForEventList(isPaging: Bool) {
        //'user_id','latitude','longitude','start'
        
        if !isPaging {
            self.showActivityIndicator(uiView: self.view)
            self.eventList = []
            self.tableView.reloadData()
        }
        let count = self.eventList.count
        
        var params = [String: Any]()
        params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"
        params["latitude"] = self.latitude
        params["longitude"] = self.longitude
        params["start"] = "\(count == 0 ? "0" : "\(count+1)")"
        params["search"] = ""
        self.isExecuting = true
        RestAPI.shared.getHomeEventList(params: params) { (events, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.isExecuting = false
                self.hideActivityIndicator(uiView: self.view)
                self.tableView.pullToRefreshControl?.endRefreshing()
                if errorCode != 100 {
                } else {
                    if isPaging {
                        self.eventList = []
                    }
                    for event in (events ?? []) {
                        self.eventList.append(event)
                    }
                }
                self.tableView.reloadData()
            }
        }
    }
    private func executeAPIForFilterEventList(isPaging: Bool) {
        var params = self.filterParams
        params["start"] = "\(self.eventList.count + 1)"
        if !isPaging {
            self.showActivityIndicator(uiView: self.view)
        }
        RestAPI.shared.getHomeFilterEventList(params: params) { (events, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                self.tableView.pullToRefreshControl?.endRefreshing()
                if errorCode != 100 {
                } else {
                    for event in (events ?? []) {
                        self.eventList.append(event)
                    }
                }
                self.tableView.reloadData()
            }
        }
    }
    private func executeAPIForJoinEvent(index: Int) {
       
        let event = eventList[index]
        
        var params = [String: Any]()
        params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"
        params["event_id"] = event.eventId ?? "0"
        params["event_type"] = (event.type == .paid) ? "1" : "2"
        params["ticket_count"] = ""
        params["total_amount"] = ""
        print(params)
        
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.bookTicket(params: params) { (isSuccess, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                if isSuccess {
                    Sync.instance.joinCalendarEvent(by: event)
                    
                    let indexPath = IndexPath(row: index, section: 0)
                    self.eventList[index].status = true
                    self.tableView.reloadRows(at: [indexPath], with: .automatic)
                    
                    if event.privacy == "3" { //Public
                        self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                    } else {
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PaymentSuccessfullVC") as! PaymentSuccessfullVC
                        vc.type = .seat
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                } else {
                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                }
            }
        }
    }
    private func executeAPIForLeaveEvent(index: Int) {
        
        var params = [String: Any]()
        params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"
        params["event_id"] = eventList[index].eventId ?? "0"
        print(params)
        
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.leaveEventTicket(params: params) { (isSuccess, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                if isSuccess {
                    let indexPath = IndexPath(row: index, section: 0)
                    self.eventList[index].status = false
                    self.tableView.reloadRows(at: [indexPath], with: .automatic)
                    Sync.instance.leaveCalendarEvent(by: self.eventList[index])
                } else {
                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                }
            }
        }
    }
    private func eventActionConfirmAlert(isJoin: Bool, index: Int) {
        let alertController = UIAlertController.init(title: Constant.APP_NAME, message: "Are you sure want to \(isJoin ? "join" : "leave") this event?", preferredStyle: .alert)
        alertController.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
        }))
        alertController.addAction(UIAlertAction.init(title: "Confirm", style: .destructive, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
            if isJoin {
                self.executeAPIForJoinEvent(index: index)
            } else {
                self.executeAPIForLeaveEvent(index: index)
            }
        }))
        self.present(alertController, animated: true, completion: nil)
    }
}
