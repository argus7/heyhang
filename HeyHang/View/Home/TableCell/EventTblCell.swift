//
//  EventTblCell.swift
//  HeyHang
//
//  Created by Anup Kumar on 5/28/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit


class EventTblCell: UITableViewCell {
    
    

    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var dateTimeLbl: UILabel! //06 May | 05:00 PM
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var mainIV: UIImageView!
    @IBOutlet weak var leftPlaceLbl: UILabel! //1 Place Left
    @IBOutlet weak var timerLbl: UILabel! //In 15:42
    
    @IBOutlet weak var detailBtn: UIButton!
    @IBOutlet weak var joinBtn: UIButton!
    @IBOutlet weak var eventTypeView: UIView!
    @IBOutlet weak var eventTypeLbl: UILabel!
    
    @IBOutlet weak var firstIV: UIImageView!
    @IBOutlet weak var secondIV: UIImageView!
    @IBOutlet weak var thirdIV: UIImageView!
    @IBOutlet weak var fourthIV: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    
    var startDate = Date()
    var seconds = 10
    var timer = Timer()
    
    var event: Event? {
        didSet {
            updateDetails()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.cardView.layer.cornerRadius = 15.0
        self.cardView.clipsToBounds = true
//        self.cardView.layer.borderColor = UIColor.black.withAlphaComponent(0.5).cgColor
//        self.cardView.layer.borderWidth = 2.0
        
        let corner: CGFloat = 7.0
        
        self.leftPlaceLbl.layer.cornerRadius = corner
        self.leftPlaceLbl.clipsToBounds = true
        self.timerLbl.layer.cornerRadius = corner
        self.timerLbl.clipsToBounds = true
        self.eventTypeView.layer.cornerRadius = corner
        self.joinBtn.layer.cornerRadius = corner
        self.detailBtn.layer.cornerRadius = corner
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func updateDetails() {

        if let eventT = event {
            
            DispatchQueue.main.async {
                let cornerRadius: CGFloat = 1.5
                
                self.firstIV.layer.borderColor = UIColor.white.cgColor
                self.firstIV.layer.borderWidth = cornerRadius
                self.firstIV.layer.cornerRadius = self.firstIV.bounds.height / 2
                self.firstIV.clipsToBounds = true
                
                self.secondIV.layer.borderColor = UIColor.white.cgColor
                self.secondIV.layer.borderWidth = cornerRadius
                self.secondIV.layer.cornerRadius = self.secondIV.bounds.height / 2
                self.secondIV.clipsToBounds = true
                
                self.thirdIV.layer.borderColor = UIColor.white.cgColor
                self.thirdIV.layer.borderWidth = cornerRadius
                self.thirdIV.layer.cornerRadius = self.thirdIV.bounds.height / 2
                self.thirdIV.clipsToBounds = true
                
                self.fourthIV.layer.borderColor = UIColor.white.cgColor
                self.fourthIV.layer.borderWidth = cornerRadius
                self.fourthIV.layer.cornerRadius = self.fourthIV.bounds.height / 2
                self.fourthIV.clipsToBounds = true
            }
            
            self.joinBtn.isHidden = false
            if eventT.type == .paid { //Paid
                self.eventTypeLbl.text = "Paid Hang"
                self.eventTypeView.backgroundColor = UIColor.eventTypeYellow
                if eventT.status {
                    self.joinBtn.isHidden = true
                }
            } //Free
            else {
                self.eventTypeLbl.text = "Free Hang"
                self.eventTypeView.backgroundColor = UIColor.eventTypePurple
                if eventT.privacy == "4" { //Auto Accept
                    self.joinBtn.isHidden = true
                }
            }
            
            if eventT.status {
                self.joinBtn.backgroundColor = UIColor.redLeave
                self.joinBtn.setTitle("Leave", for: .normal)
            } else {
                self.joinBtn.backgroundColor = UIColor.greenJoin
                self.joinBtn.setTitle("Join", for: .normal)
            }
            
            let startDate = Date(timeIntervalSince1970: eventT.startDateTimeInterval ?? 0.0)
            let formatter = DateFormatter()
            formatter.dateFormat = "dd MMM | hh:mm a"
            let startDateTime = formatter.string(from: startDate)
            
            self.nameLbl.text = eventT.name ?? ""
            self.locationLbl.text = eventT.location ?? ""
            self.dateTimeLbl.text = startDateTime
            self.distanceLbl.text = eventT.distance
            self.leftPlaceLbl.text = "\(eventT.seatAvailable < 0 ? 0 : eventT.seatAvailable) Seat Left"
            
            let image = eventT.imageArr.last?.url ?? ""
            self.mainIV.imageFromServerURL(image, placeHolder: nil)
            
            if eventT.member.count > 3 {
                self.fourthIV.isHidden = false
                self.fourthIV.imageFromServerURL(eventT.member[3].image ?? "", placeHolder: UIImage(named: "profile_placeholder"))
            } else {
                self.fourthIV.isHidden = true
            }
            if eventT.member.count > 2 {
                self.thirdIV.isHidden = false
                self.thirdIV.imageFromServerURL(eventT.member[2].image ?? "", placeHolder: UIImage(named: "profile_placeholder"))
            } else {
                self.thirdIV.isHidden = true
            }
            if eventT.member.count > 1 {
                self.secondIV.isHidden = false
                self.secondIV.imageFromServerURL(eventT.member[1].image ?? "", placeHolder: UIImage(named: "profile_placeholder"))
            } else {
                self.secondIV.isHidden = true
            }
            if eventT.member.count > 0 {
                self.firstIV.isHidden = false
                self.firstIV.imageFromServerURL(eventT.member[0].image ?? "", placeHolder: UIImage(named: "profile_placeholder"))
            } else {
                self.firstIV.isHidden = true
            }
            
            guard let interval = eventT.startDateTimeInterval else {
                self.invalidateTimer()
                return
            }
            let nDate = Date(timeIntervalSince1970: interval)
            self.startDate = nDate
            self.runTimer()
        }
    }
    
    func runTimer() {
        self.timerLbl.text = self.dateOffsetFrom(date: startDate)
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
    }
    func invalidateTimer() {
        self.timer.invalidate()
    }
    @objc func updateTimer() {
        if seconds == 0 {
            self.timerLbl.isHidden = true
            self.timer.invalidate()
        }
        seconds = self.getRaminingSeconds(date: startDate)
        self.timerLbl.text = self.dateOffsetFrom(date: startDate)
    }
    
    func getRaminingSeconds(date: Date) -> Int {
        
        var rSecond = 0
        let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: Date(), to: date);
        
        rSecond = rSecond + (difference.day! * 24 * 60 * 60)
        rSecond = rSecond + (difference.hour! * 60 * 60)
        rSecond = rSecond + (difference.minute! * 60)
        rSecond = rSecond + (difference.second!)
        
        return rSecond
    }
    func dateOffsetFrom(date: Date) -> String {
        
        let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: Date(), to: date);
        
        let hrs = difference.hour ?? 0
        let mnts = difference.minute ?? 0
        let minutes = "\(mnts < 10 ? "0\(mnts)" : "\(mnts)")"
        let hours = "\(hrs < 10 ? "0\(hrs)" : "\(hrs)")"
        
        var mainStr = ""
        mainStr = "In \(hours):\(minutes)"

        if let day = difference.day {
            if day > 0 {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MMM-yyyy"
                return formatter.string(from: date)
            } else {
                return mainStr
            }
        } else {
            return mainStr
        }
    }
}
