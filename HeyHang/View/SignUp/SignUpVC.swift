//
//  SignUpVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 5/27/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

class SignUpVC: UIViewController {
    
    enum Navigation {
        case email
        case phone
    }
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var hdrLbl: UILabel!
    @IBOutlet weak var nameTF: MATextField!
    @IBOutlet weak var emailTF: MATextField!
    @IBOutlet weak var passwordTF: MATextField!
    @IBOutlet weak var loginBtn: UIButton!
    
    @IBOutlet weak var termIV: UIImageView!
    @IBOutlet weak var termsConditionBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    
    var navigation: Navigation = .email
    private var isTermActive: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupTextField()
        loginBtn.setAttributedTitle(loginBtn.getUnderlineAttributeString(), for: .normal)
        termsConditionBtn.setAttributedTitle(termsConditionBtn.getUnderlineAttributeString(), for: .normal)
        updateTerms(isActive: isTermActive)
    }
    override func viewWillLayoutSubviews() {
        self.nameTF.placeholderColor = UIColor.darkGray
        self.emailTF.placeholderColor = UIColor.darkGray
        self.passwordTF.placeholderColor = UIColor.darkGray
        self.nextBtn.layer.cornerRadius = Constant.kCornerRadius
    }
    @IBAction func onClickLoginBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onClickTermCheckBtn(_ sender: Any) {
        updateTerms(isActive: !isTermActive)
    }
    @IBAction func onClickTermsConditionBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyTermVC") as! PrivacyTermVC
        vc.navigation = .terms
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onClickNextBtn(_ sender: Any) {
        if checkInternet() {
            if let params = validateParams() {
                self.executeSignUpApi(params: params)
            }
        }
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        //        self.moveToLogin()
        self.navigationController?.popViewController(animated: true)
    }
    private func updateTerms(isActive: Bool) {
        self.isTermActive = isActive
        if isTermActive {
            self.termIV.image = UIImage.init(named: "check_active")
        } else {
            self.termIV.image = UIImage.init(named: "check_normal")
        }
    }
    private func moveToLogin() {
        for controller: UIViewController in (self.navigationController?.viewControllers)! {
            if controller.isKind(of: LoginVC.self) {
                self.navigationController?.popToViewController(controller, animated: true)
                return
            }
        }
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension SignUpVC : UITextFieldDelegate {
    
    private func setupTextField() {
        self.nameTF.delegate = self
        self.emailTF.delegate = self
        self.passwordTF.delegate = self
        self.nameTF.bottomLbl?.backgroundColor = UIColor.lightGray
        self.emailTF.bottomLbl?.backgroundColor = UIColor.lightGray
        self.passwordTF.bottomLbl?.backgroundColor = UIColor.lightGray
        
        if navigation == .email {
            self.emailTF.placeholder = "Email Address"
            self.emailTF.keyboardType = .emailAddress
            self.hdrLbl.text = "Sign Up with Email"
        } else {
            self.emailTF.placeholder = "Phone Number"
            self.emailTF.keyboardType = .numberPad
            self.hdrLbl.text = "Sign Up with Phone"
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.emailTF {
            self.emailTF.bottomLbl?.backgroundColor = UIColor.darkText
        } else if textField == self.passwordTF {
            self.passwordTF.bottomLbl?.backgroundColor = UIColor.darkText
        } else if textField == self.nameTF {
            self.nameTF.bottomLbl?.backgroundColor = UIColor.darkText
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.emailTF {
            self.emailTF.bottomLbl?.backgroundColor = UIColor.lightGray
        } else if textField == self.passwordTF {
            self.passwordTF.bottomLbl?.backgroundColor = UIColor.lightGray
        } else if textField == self.nameTF {
            self.nameTF.bottomLbl?.backgroundColor = UIColor.lightGray
        }
    }
}
// MARK: - APIs
extension SignUpVC {
    private func validateParams() -> [String: Any]? {
        guard let name  = self.nameTF.text, !(self.nameTF.text?.isEmpty)!  else {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter your name")
            return nil
        }
        guard let email  = self.emailTF.text, !(self.emailTF.text?.isEmpty)!  else {
            if navigation == .email {
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter your email")
            } else {
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter your phone number")
            }
            return nil
        }
        if navigation == .email {
            if !self.isValidEmail(email) {
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter correct email")
                return nil
            }
        } else {
            if email.count < 8 || (email.count > 14) {
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter correct phone number")
                return nil
            }
        }
        
        guard let password  = self.passwordTF.text, !(self.passwordTF.text?.isEmpty)!  else {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter password")
            return nil
        }
        if password.count < 8{
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Password should be minimum 8 character")
            return nil
        }
        guard isTermActive else {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please accept \(Constant.APP_NAME) terms & conditions")
            return nil
        }
        var params = [String: Any]()
        params["full_name"] = name
        params["password"] = password
        if navigation == .email {
            params["login_type"] = "2"
            params["email"] = email
        } else {
            params["login_type"] = "1"
            params["mobile"] = email
        }
        return params
    }
    private func executeSignUpApi(params: [String: Any]) {
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.signUp(isForMobile: (navigation == .email ? false : true), params: params) { (userId, errorMessage, errorCode) in
            self.hideActivityIndicator(uiView: self.view)
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred, Please try again later")
            if errorCode == 100 {
                DispatchQueue.main.async {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "VerifyOTPVC") as! VerifyOTPVC
                    vc.userId = userId ?? "0"
                    vc.navigation = (self.navigation == .email ? .emailVerify : .phoneVerify)
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
}
