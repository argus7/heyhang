//
//  ForgotPasswordVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 5/27/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {
    
    @IBOutlet weak var mobileView: UIView!
    @IBOutlet weak var mobileTF: MATextField!
    @IBOutlet weak var submitBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupTextField()
    }
    override func viewWillLayoutSubviews() {
        self.mobileTF.placeholderColor = UIColor.darkGray
        self.submitBtn.layer.cornerRadius = Constant.kCornerRadius
    }
    
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickSubmitBtn(_ sender: Any) {
        if checkInternet() {
            if let params = validateParams() {
                self.executeForgotPasswordApi(params: params)
            }
        }
    }
}
extension ForgotPasswordVC : UITextFieldDelegate {
    
    private func setupTextField() {
        self.mobileTF.delegate = self
        self.mobileTF.bottomLbl?.backgroundColor = UIColor.lightGray
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.mobileTF {
            self.mobileTF.bottomLbl?.backgroundColor = UIColor.darkText
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.mobileTF {
            self.mobileTF.bottomLbl?.backgroundColor = UIColor.lightGray
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
            return true
        }
        if textField == self.mobileTF {
            if (self.mobileTF.text?.isEmpty)! && ((string == "0") || (string == " ")) {
                return false
            } else if !isValidAlphaNumericWithEmail(string) {
                return false
            }
        }
        return true
    }
}

// MARK: - APIs
extension ForgotPasswordVC {
    private func validateParams() -> [String: Any]? {
        //Note: mobileTF is used for both email address and mobile number
        guard let emailName  = self.mobileTF.text, !(self.mobileTF.text?.isEmpty)!  else {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter your email address/mobile number")
            return nil
        }
        if emailName.contains("@") {
            if !self.isValidEmail(emailName) {
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter correct email")
                return nil
            }
        } else {
            if emailName.count < 8 || (emailName.count > 14) {
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter correct phone number")
                return nil
            }
        }
        var params = [String: Any]()
        if emailName.contains("@") {
            params["email"] = emailName
            params["login_type"] = "2"
        } else {
            params["mobile"] = emailName
            params["login_type"] = "1"
        }
        return params
        
    }
    private func executeForgotPasswordApi(params: [String: Any]) {
        self.view.endEditing(true)
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.forgotPassword(params: params) { (userId, errorMessage, errorCode) in
            self.hideActivityIndicator(uiView: self.view)
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred, Please try again later")
            if errorCode == 100 {
                DispatchQueue.main.async {
                    let emailName  = self.mobileTF.text ?? ""
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "VerifyOTPVC") as! VerifyOTPVC
                    vc.userId = userId ?? "0"
                    vc.resetLoginType = emailName.contains("@") ? "2" : "1"
                    vc.navigation = .resetPassVerify
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
}
