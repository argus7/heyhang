//
//  SuccessPopUpVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 5/28/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

protocol SuccessPopUpDelegate {
    func onActionPerform(on type: SuccessPopUpNavigation)
}
enum SuccessPopUpNavigation {
    case changePassword
    case resetPassword
    case emailVerify
    case phoneVerify
    case createHang
    case updateHang
}
class SuccessPopUpVC: UIViewController {

    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var logoIV: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var actionBtn: UIButton!
    
    var navigation: SuccessPopUpNavigation = .resetPassword
    var delegate: SuccessPopUpDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        initializeNavigation()
    }
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.3) {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.15)
        }
    }
    override func viewWillLayoutSubviews() {
        self.popUpView.layer.cornerRadius = 20.0
        self.actionBtn.layer.cornerRadius = 10.0
    }
    @IBAction func onClickActionBtn(_ sender: Any) {
        print("Action Btn")
        self.dismiss(animated: true) {
            self.delegate?.onActionPerform(on: self.navigation)
        }
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
// MARK: - Action Handling
extension SuccessPopUpVC {
    private func initializeNavigation() {
        switch navigation {
        case .changePassword:
            self.logoIV.image = UIImage(named: "password_circle_blue")
            self.titleLbl.text = "Congratulation"
            self.descriptionLbl.text = "Your password has been changed"
            self.actionBtn.setTitle("Done", for: .normal)
            break
        case .resetPassword:
            self.logoIV.image = UIImage(named: "password_circle_blue")
            self.titleLbl.text = "Congratulation"
            self.descriptionLbl.text = "Your password has been changed"
            self.actionBtn.setTitle("Login", for: .normal)
            break
        case .emailVerify:
            self.logoIV.image = UIImage(named: "email_circle_blue")
            self.titleLbl.text = "Congratulation"
            self.descriptionLbl.text = "Your Email address has been verified successfully"
            self.actionBtn.setTitle("OK", for: .normal)
            break
        case .phoneVerify:
            self.logoIV.image = UIImage(named: "mobile_circle_blue")
            self.titleLbl.text = "Congratulation"
            self.descriptionLbl.text = "Your phone number has been verified successfully"
            self.actionBtn.setTitle("OK", for: .normal)
            break
        case .createHang:
            self.logoIV.image = UIImage(named: "event_purple")
            self.titleLbl.text = "Hang successfully created"
            self.descriptionLbl.text = ""
            self.actionBtn.setTitle("Go to my post", for: .normal)
            break
        case .updateHang:
            self.logoIV.image = UIImage(named: "event_purple")
            self.titleLbl.text = "Hang successfully updated"
            self.descriptionLbl.text = ""
            self.actionBtn.setTitle("Go to my post", for: .normal)
            break
        }
    }
}
