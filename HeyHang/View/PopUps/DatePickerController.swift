//
//  DatePickerController.swift
//  HeyHang
//
//  Created by Anup Kumar on 6/24/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

protocol DatePickerDelegate {
    func datePicker(onSelectedDate date: Date, tag: Int)
}
class DatePickerController: UIViewController {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var delegate: DatePickerDelegate?
    var dateFormatter: String = "dd-MM-yyyy"
    var dateStr: String = ""
    var tag: Int = 0
    var datePickerMode: UIDatePicker.Mode = .dateAndTime
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        self.datePicker.minimumDate = Calendar.current.date(
            byAdding: .hour,
            value: +1,
            to: Date())
        self.datePicker.datePickerMode = datePickerMode
        
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormatter
        if let date = formatter.date(from: dateStr) {
            self.datePicker.date = date
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.3) {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.15)
        }
    }
    override func viewWillLayoutSubviews() {
        self.popupView.setCorner(corners: [.topLeft, .topRight], radius: 20.0)
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onClickDoneBtn(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate?.datePicker(onSelectedDate: self.datePicker.date, tag: self.tag)
        }
    }
}
