//
//  MyEventVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 5/28/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit
import AKSideMenu

class MyEventVC: UIViewController {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var hdrLbl: UILabel!
    @IBOutlet weak var tableView: MATableView!
    
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var searchViewWidthConstraints: NSLayoutConstraint!
    @IBOutlet weak var addBtn: UIButton!
    
    private var eventList: [Event] = []
    var isMoveForSearch: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initialization()
        if isMoveForSearch {
            self.hdrLbl.text = "Search Hang"
            executeAPIForSearchEventList(searchText: "")
        } else {
            self.hdrLbl.text = "My Hang"
            if checkInternet() {
                self.executeAPIForEventList(isPaging: false)
            }
        }
    }
    override func viewWillLayoutSubviews() {
        self.headerView.setSideDropShadow(isUp: false)
        self.searchView.layer.cornerRadius = 5.0
    }
    override func viewDidAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        for controller: UIViewController in (self.navigationController?.viewControllers)! {
            if controller.isKind(of: AKSideMenu.self) {
                self.navigationController?.popToViewController(controller, animated: true)
                return
            }
        }
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickAddBtn(_ sender: Any) {
        if self.searchViewWidthConstraints.constant == 0.0 {
            showSearchBar()
        } else {
            hideSearchBar()
        }
    }
}
extension MyEventVC : UITextFieldDelegate {
    
    private func setupTextField() {
        self.searchTF.delegate = self
        self.searchTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    @objc func textFieldDidChange(_ theTextField: UITextField) {
       executeAPIForSearchEventList(searchText: theTextField.text ?? "")
    }
    private func initialization() {
        if !isMoveForSearch {
            self.addBtn.isHidden = true
        }
        self.searchViewWidthConstraints.constant = 0
        self.searchTF.isHidden = true
        setupTextField()
        setupTableView()
    }
    //Search Show/Hide Methods  for chat list and friend list
    private func showSearchBar() {
        UIView.animate(withDuration: 0.5) {
            self.searchTF.isHidden = false
            self.searchViewWidthConstraints.constant = UIScreen.main.bounds.width - 100
            self.view.layoutIfNeeded()
            self.addBtn.setImage(UIImage.init(named: "crossWhite"), for: .normal)
        }
    }
    private func hideSearchBar() {
        UIView.animate(withDuration: 0.5) {
            self.view.endEditing(true)
            self.searchTF.isHidden = true
            self.searchViewWidthConstraints.constant = 0.0
            self.view.layoutIfNeeded()
            self.addBtn.setImage(UIImage.init(named: "search_white"), for: .normal)
            self.searchTF.text = ""
            self.executeAPIForSearchEventList(searchText: "")
        }
    }
}
extension MyEventVC: UITableViewDelegate, UITableViewDataSource, UITableViewPullRefreshDelegate {
    func tableView(_ tableView: UITableView, pulltoRefreshStart: Bool) {
        self.eventList = []
        self.tableView.reloadData()
        if checkInternet() {
            self.executeAPIForEventList(isPaging: false)
        }
    }
    fileprivate func setupTableView() {
        self.tableView.register(UINib(nibName: "EventTblCell", bundle: Bundle.main), forCellReuseIdentifier: "EventTblCell")
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if eventList.count > 0 {
            self.tableView.backgroundView = nil
            return eventList.count
        } else {
            if isMoveForSearch {
                if self.searchTF.text?.count == 0 {
                    self.tableView.showBackgroundMsg(msg: "Search hang...")
                } else {
                    self.tableView.showBackgroundMsg(msg: "No Hang Found")
                }
            } else {
                self.tableView.showBackgroundMsg(msg: "No Hang Found")
            }
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "EventTblCell", for: indexPath) as! EventTblCell
        
        cell.event = eventList[indexPath.row]
        
        if !isMoveForSearch {
            cell.timerLbl.isHidden = true
            cell.leftPlaceLbl.isHidden = true
            cell.joinBtn.isHidden = true
            cell.distanceLbl.isHidden = true
        }
        var frame = cell.gradientView.frame
        frame.size.width = UIScreen.main.bounds.width - 40
        frame.size.height = (250 * widthRatio) - 64.0
        cell.gradientView.frame = frame
        cell.gradientView.setBottomToTopGradient(cornerRadius: 0.0, firstColor: .clear, secoundColor: UIColor(red: 0, green: 0, blue: 0, alpha: 0.8))
        
        cell.detailBtn.tag = indexPath.row
        cell.detailBtn.addTarget(self, action: #selector(detailsBtnAction(_:)), for: .touchUpInside)
        cell.joinBtn.tag = indexPath.row
        cell.joinBtn.addTarget(self, action: #selector(onClickJoinCellBtn(_:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250 * widthRatio
    }
    
    @objc func detailsBtnAction(_ sender: UIButton){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailVC") as! EventDetailVC
        vc.navigation = isMoveForSearch ? .home : .myHang
        vc.selectedEvent = self.eventList[sender.tag]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func onClickJoinCellBtn(_ sender: UIButton) {
        print("Join/Leave Btn")
        let event = eventList[sender.tag]
        if event.type == .paid {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookTicketsVC") as! BookTicketsVC
            vc.selectedEvent = event
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            if checkInternet() {
                if event.status {
                    self.eventActionConfirmAlert(isJoin: false, index: sender.tag)
                } else {
                    self.eventActionConfirmAlert(isJoin: true, index: sender.tag)
                }
            }
        }
    }
}
// MARK: - APIs Data
extension MyEventVC {
    private func executeAPIForEventList(isPaging: Bool) {
        var params = [String: Any]()
        params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"
        if !isPaging {
            self.showActivityIndicator(uiView: self.view)
        }
        RestAPI.shared.getMyEventList(params: params) { (events, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                self.tableView.pullToRefreshControl?.endRefreshing()
                if errorCode != 100 {
                } else {
                    for event in (events ?? []) {
                        self.eventList.append(event)
                    }
                }
                self.tableView.reloadData()
            }
        }
    }
    private func executeAPIForSearchEventList(searchText: String) {
        
        self.showActivityIndicator(uiView: self.view)
        var params = [String: Any]()
        params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"
        params["latitude"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kLatitude) ?? "0"
        params["longitude"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kLongitude) ?? "0"
        params["start"] = "0"
        params["search"] = searchText
        RestAPI.shared.getHomeEventList(params: params) { (events, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                self.tableView.pullToRefreshControl?.endRefreshing()
                if errorCode == 100 {
                    self.eventList = events ?? []
                } else {
                    self.eventList = []
                }
                self.tableView.reloadData()
            }
        }
    }
    private func executeAPIForJoinEvent(index: Int) {
        
        let event = eventList[index]
        
        var params = [String: Any]()
        params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"
        params["event_id"] = event.eventId ?? "0"
        params["event_type"] = (event.type == .paid) ? "1" : "2"
        params["ticket_count"] = ""
        params["total_amount"] = ""
        print(params)
        
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.bookTicket(params: params) { (isSuccess, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                if isSuccess {
                    Sync.instance.joinCalendarEvent(by: event)
                    let indexPath = IndexPath(row: index, section: 0)
                    self.eventList[index].status = true
                    self.tableView.reloadRows(at: [indexPath], with: .automatic)
                    
                    if event.privacy == "3" { //Public
                        self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                    } else {
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PaymentSuccessfullVC") as! PaymentSuccessfullVC
                        vc.type = .seat
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    
                } else {
                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                }
            }
        }
    }
    private func executeAPIForLeaveEvent(index: Int) {
        
        var params = [String: Any]()
        params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"
        params["event_id"] = eventList[index].eventId ?? "0"
        print(params)
        
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.leaveEventTicket(params: params) { (isSuccess, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                if isSuccess {
                    let indexPath = IndexPath(row: index, section: 0)
                    self.eventList[index].status = false
                    self.tableView.reloadRows(at: [indexPath], with: .automatic)
                    Sync.instance.leaveCalendarEvent(by: self.eventList[index])
                } else {
                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred!")
                }
            }
        }
    }
    private func eventActionConfirmAlert(isJoin: Bool, index: Int) {
        let alertController = UIAlertController.init(title: Constant.APP_NAME, message: "Are you sure want to \(isJoin ? "join" : "leave") this event?", preferredStyle: .alert)
        alertController.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
        }))
        alertController.addAction(UIAlertAction.init(title: "Confirm", style: .destructive, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
            if isJoin {
                self.executeAPIForJoinEvent(index: index)
            } else {
                self.executeAPIForLeaveEvent(index: index)
            }
        }))
        self.present(alertController, animated: true, completion: nil)
    }
}
