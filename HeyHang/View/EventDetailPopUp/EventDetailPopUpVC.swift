//
//  EventDetailPopUpVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 6/12/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit
import LyftSDK
import CoreLocation
import UberRides

protocol BookRideDelegate {
    func onSuccessRideSelection(type: RideType)
}
class EventDetailPopUpVC: UIViewController {
    
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var uberView: UIView!
    @IBOutlet weak var lyftView: UIView!
    
    var delegate: BookRideDelegate?
    var sourceLocation: CLLocation?
    var destinationLocation: CLLocation?
    
    private let btnLyft = LyftButton()
    private let uberBtn = RideRequestButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        setupLyftBtn()
    }
    
    private func setupLyftBtn() {
        
        let width: CGFloat = UIScreen.main.bounds.width - 40.0
        let height: CGFloat = 45.0
        let frame = CGRect(x: 0, y: 0, width: width, height: height)
        btnLyft.frame = frame
        btnLyft.style = LyftButtonStyle.mulberryDark
        self.lyftView.addSubview(btnLyft)
        
        uberBtn.frame = frame
        self.uberView.addSubview(uberBtn)
        
        guard let source = sourceLocation else {
            return
        }
        guard let destination = destinationLocation else {
            return
        }
        
        btnLyft.configure(rideKind: LyftSDK.RideKind.Standard, pickup: source.coordinate, destination: destination.coordinate)
        self.requestRide(source: source, destination: destination)
//        let pickup = CLLocationCoordinate2D(latitude: 37.7833, longitude: -122.4167)
//        let destination = CLLocationCoordinate2D(latitude: 37.7794703, longitude: -122.4233223)
    }
    private func requestRide(source: CLLocation, destination: CLLocation) {
        let ridesClient = RidesClient()
        let builder = RideParametersBuilder()
        builder.pickupLocation = source
        builder.dropoffLocation = destination
        
        ridesClient.fetchProducts(pickupLocation: source) { products, response in
            guard let uberX = products.filter({$0.productGroup == .uberX}).first else {
                // Handle error, UberX does not exist at this location
                return
            }
            builder.productID = uberX.productID
            self.uberBtn.rideParameters = builder.build()
            self.uberBtn.loadRideInformation()
        }
    }
    
    override func viewWillLayoutSubviews() {
        self.popUpView.layer.cornerRadius = 20.0
//        btnLyft.frame = self.lyftView.bounds
    }
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.3) {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.15)
        }
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
//    @IBAction func onClickOLABtn(_ sender: Any) {
//        self.dismiss(animated: true) {
//            self.delegate?.onSuccessRideSelection(type: .ola)
//        }
//    }
//    @IBAction func onClickMeruBtn(_ sender: Any) {
//        self.dismiss(animated: true) {
//            self.delegate?.onSuccessRideSelection(type: .meru)
//        }
//    }
//    @IBAction func onClickUberBtn(_ sender: Any) {
//        self.dismiss(animated: true) {
//            self.delegate?.onSuccessRideSelection(type: .uber)
//        }
//    }
//    @IBAction func onClickJugnooBtn(_ sender: Any) {
//        self.dismiss(animated: true) {
//            self.delegate?.onSuccessRideSelection(type: .jugnoo)
//        }
//    }
}
