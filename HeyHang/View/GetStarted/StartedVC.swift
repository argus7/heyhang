//
//  StartedVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 5/27/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit
import AKSideMenu
import GoogleSignIn

class StartedVC: UIViewController {
    
    @IBOutlet weak var mobileView: UIView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var gmailView: UIView!
    @IBOutlet weak var loginBtn: UIButton!
    
    private var socialDict: [String : Any] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        loginBtn.setAttributedTitle(loginBtn.getUnderlineAttributeString(), for: .normal)
    }
    override func viewWillLayoutSubviews() {
        func setBorder(on tView: UIView) {
            let borderColor = UIColor(red: 191/255.0, green: 196/255.0, blue: 213/255.0, alpha: 0.7).cgColor
            let borderWidth: CGFloat = 1.0
            tView.layer.borderColor = borderColor
            tView.layer.borderWidth = borderWidth
            tView.layer.cornerRadius = Constant.kCornerRadius
        }
        setBorder(on: self.mobileView)
        setBorder(on: emailView)
        self.gmailView.setLightDropShadow(cornerRadius: Constant.kCornerRadius)
    }
    
    @IBAction func onClickMobileNo(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        vc.navigation = .phone
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onClickEmailBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        vc.navigation = .email
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onClickGmailBtn(_ sender: Any) {
        if checkInternet() {
            self.gmailconfiguration()
        }
    }
    @IBAction func onClickLoginBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension StartedVC {
    
    private func executeSocialApi() {
        var params: [String: Any] = [:]
        params["login_type"] = "3"
        params["email"] = self.socialDict["email"] as? String ?? ""
        params["full_name"] = self.socialDict["name"] as? String ?? ""
        params["social_id"] = self.socialDict["uid"] as? String ?? ""
        params["device_type"] = "2"
        params["device_id"] = UIDevice.current.identifierForVendor?.uuidString
        params["device_token"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kDeviceToken) ?? "qwertyuiop"
        print("Params: \(params)")
        
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.socialLogin(params: params) { (userId, errorMessage, errorCode) in
            self.hideActivityIndicator(uiView: self.view)
            if errorCode != 100 {
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "")
                if errorCode == 101 {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "VerifyOTPVC") as! VerifyOTPVC
                    vc.userId = userId ?? "0"
                    vc.navigation = .socialVerify
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            } else {
                DispatchQueue.main.async {
                    let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                    let sideVC = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
                    let vc: AKSideMenu = AKSideMenu(contentViewController: homeVC, leftMenuViewController: sideVC, rightMenuViewController: nil)
                    vc.interactivePopGestureRecognizerEnabled = true
                    vc.panFromEdge = false
                    vc.bouncesHorizontally = false
                    vc.contentViewShadowEnabled = true
                    vc.contentViewShadowOpacity = 10.0
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
}
extension StartedVC : GIDSignInUIDelegate, GIDSignInDelegate {
    
    private func gmailconfiguration(){
        let signIn:GIDSignIn = GIDSignIn.sharedInstance()
        signIn.delegate = self
        signIn.uiDelegate = self
        signIn.shouldFetchBasicProfile = true
        signIn.signIn()
    }
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        self.hideActivityIndicator(uiView: self.view)
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
        
        print("Sign in presented")
        
    }
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
        self.hideActivityIndicator(uiView: self.view)
        print("Sign in dismissed")
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        self.showActivityIndicator(uiView: self.view)
        if (error == nil) {
            
            let userId = user.userID
            let f_name = user.profile.givenName as String
            let LastName = user.profile.familyName as String
            let email = user.profile.email as String
            
            var socialDictT = [String : Any]()
            
            GIDSignIn.sharedInstance().signOut()
            socialDictT["uid"] = userId
            socialDictT["name"] = f_name + " " + LastName
            socialDictT["email"] = email
            socialDictT["password"] = ""
            self.socialDict = socialDictT
            print("Social Google Data is: \(socialDict)")
            self.hideActivityIndicator(uiView: self.view)
            self.executeSocialApi()
        } else {
            print("\(error)")
            self.hideActivityIndicator(uiView: self.view)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user:GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
}


