//
//  NotificationSettingVC.swift
//  HeyHang
//
//  Created by Mohd Arsad on 18/06/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

class NotificationSettingVC: UIViewController {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var hdrLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    private var selectedSettings: [Int] = [0,0,0,0]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshSettings()
        setupTableView()
    }
    override func viewWillLayoutSubviews() {
        self.headerView.setSideDropShadow(isUp: false)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension NotificationSettingVC: UITableViewDelegate, UITableViewDataSource {
    fileprivate func setupTableView() {
        self.tableView.register(UINib(nibName: "SettingTblNCell", bundle: Bundle.main), forCellReuseIdentifier: "SettingTblNCell")
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedSettings.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "SettingTblNCell", for: indexPath) as! SettingTblNCell
        
        cell.descriptionLblHeightConstraints.constant = 0
        cell.toggleBtnWidthConstraints.constant = 40
        cell.toggleBtn.isHidden = false
        if indexPath.row == 0 {
            cell.titleLbl.text = "Event Post Notification"
            cell.logoIV.image = UIImage.init(named: "event_green")
            cell.toggleBtn.setImage((selectedSettings[0] == 0 ? UIImage(named: "toggle_off") : UIImage(named: "toggle_on")), for: .normal)
        } else if indexPath.row == 1 {
            cell.titleLbl.text = "Event Invitation"
            cell.logoIV.image = UIImage.init(named: "invite")
            cell.toggleBtn.setImage((selectedSettings[1] == 0 ? UIImage(named: "toggle_off") : UIImage(named: "toggle_on")), for: .normal)
        } else if indexPath.row == 2 {
            cell.titleLbl.text = "Before starting an event"
            cell.logoIV.image = UIImage.init(named: "event_green")
            cell.toggleBtn.setImage((selectedSettings[2] == 0 ? UIImage(named: "toggle_off") : UIImage(named: "toggle_on")), for: .normal)
        } else {
            cell.titleLbl.text = "When your friend free"
            cell.logoIV.image = UIImage.init(named: "notify")
            cell.toggleBtn.setImage((selectedSettings[3] == 0 ? UIImage(named: "toggle_off") : UIImage(named: "toggle_on")), for: .normal)
        }
        cell.detailBtn.tag = indexPath.row
        cell.detailBtn.addTarget(self, action: #selector(onClickDetailBtn(_:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    @objc func onClickDetailBtn(_ sender: UIButton) {
        let indexPath = IndexPath(row: sender.tag, section: 0)
        if checkInternet() {
            executeAPIUpdateNotificationSettings(index: indexPath.row)
        }
    }
    
    private func refreshSettings() {
        selectedSettings[0] = UserDefaults.standard.bool(forKey: Constant.userDefaults.kNotiPostNoti) ? 1 : 0
        selectedSettings[1] = UserDefaults.standard.bool(forKey: Constant.userDefaults.kNotiInviteNoti) ? 1 : 0
        selectedSettings[2] = UserDefaults.standard.bool(forKey: Constant.userDefaults.kNotiStartNoti) ? 1 : 0
        selectedSettings[3] = UserDefaults.standard.bool(forKey: Constant.userDefaults.kNotiFriendNoti) ? 1 : 0
        self.tableView.reloadData()
    }
}
// MARK: - APIs
extension NotificationSettingVC {
    private func executeAPIUpdateNotificationSettings(index: Int) {
        
        let updatedvalue = (self.selectedSettings[index] == 0 ? 1 : 0)
      
        var params = [String: Any]()
        params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"
        params["post_notification"] = "\(selectedSettings[0])"
        params["invitation_notification"] = "\(selectedSettings[1])"
        params["before_start_notification"] = "\(selectedSettings[2])"
        params["friends_notification"] = "\(selectedSettings[3])"
        
        if index == 0 {
            params["post_notification"] = "\(updatedvalue)"
        } else if index == 1 {
            params["invitation_notification"] = "\(updatedvalue)"
        } else if index == 2 {
            params["before_start_notification"] = "\(updatedvalue)"
        } else {
            params["friends_notification"] = "\(updatedvalue)"
        }
        
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.updateNotificationSettings(params: params) { (serverData, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                if let settings = serverData {
                    UserDefaults.standard.set(((settings["post_notification"] as? String ?? "") == "1" ? true : false), forKey: Constant.userDefaults.kNotiPostNoti)
                    UserDefaults.standard.set(((settings["invitation_notification"] as? String ?? "") == "1" ? true : false), forKey: Constant.userDefaults.kNotiInviteNoti)
                    UserDefaults.standard.set(((settings["before_start_notification"] as? String ?? "") == "1" ? true : false), forKey: Constant.userDefaults.kNotiStartNoti)
                    UserDefaults.standard.set(((settings["friends_notification"] as? String ?? "") == "1" ? true : false), forKey: Constant.userDefaults.kNotiFriendNoti)
                    self.refreshSettings()
                }
            }
        }
    }
}
