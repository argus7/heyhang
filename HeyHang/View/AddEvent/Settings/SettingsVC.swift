//
//  SettingsVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 6/7/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var hdrLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    override func viewWillLayoutSubviews() {
        self.headerView.setSideDropShadow(isUp: false)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension SettingsVC: UITableViewDelegate, UITableViewDataSource {
    fileprivate func setupTableView() {
        self.tableView.register(UINib(nibName: "SettingTblNCell", bundle: Bundle.main), forCellReuseIdentifier: "SettingTblNCell")
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "SettingTblNCell", for: indexPath) as! SettingTblNCell
        cell.toggleBtnWidthConstraints.constant = 0
        cell.descriptionLblHeightConstraints.constant = 0.0
        cell.toggleBtn.isHidden = true
        if indexPath.row == 0 { //Notification Setting
            cell.titleLbl.text = "Notification Setting"
            cell.logoIV.image = UIImage.init(named: "notify")
        } else if indexPath.row == 1 { //Change Password
            cell.titleLbl.text = "Change Password"
            cell.logoIV.image = UIImage.init(named: "change_password")
        } else if indexPath.row == 2 { //Delete Account
            cell.titleLbl.text = "Delete Account"
            cell.logoIV.image = UIImage.init(named: "logout")
        } else { //Logout
            cell.titleLbl.text = "Logout"
            cell.logoIV.image = UIImage.init(named: "logout")
        }
        cell.detailBtn.tag = indexPath.row
        cell.detailBtn.addTarget(self, action: #selector(onClickDetailBtn(_:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    @objc func onClickDetailBtn(_ sender: UIButton) {
        let indexPath = IndexPath(row: sender.tag, section: 0)
        if indexPath.row == 0 { //Notification Setting
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationSettingVC") as! NotificationSettingVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else if indexPath.row == 1 { //Change Password
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
            vc.userId = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"
            self.navigationController?.pushViewController(vc, animated: true)
        } else if indexPath.row == 2 { //Delete Account
            deleteConfirmAlert()
        } else { //Logout
            logoutConfirmAlert()
        }
    }
    private func moveToLogin() {
        for controller: UIViewController in (self.navigationController?.viewControllers)! {
            if controller.isKind(of: LoginVC.self) {
                self.navigationController?.popToViewController(controller, animated: true)
                return
            }
        }
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    private func deleteConfirmAlert() {
        let alertController = UIAlertController.init(title: Constant.APP_NAME, message: "Are you sure want to delete your account?", preferredStyle: .alert)
        alertController.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
        }))
        alertController.addAction(UIAlertAction.init(title: "Confirm", style: .destructive, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
            self.executeAPIForDeleteAccount()
        }))
        self.present(alertController, animated: true, completion: nil)
    }
    private func logoutConfirmAlert() {
        let alertController = UIAlertController.init(title: Constant.APP_NAME, message: "Are you sure want to logout?", preferredStyle: .alert)
        alertController.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
        }))
        alertController.addAction(UIAlertAction.init(title: "Logout", style: .destructive, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
            UserDefaults.standard.setValue("No", forKey: Constant.userDefaults.kIsRegistered)
            self.moveToLogin()
        }))
        self.present(alertController, animated: true, completion: nil)
    }
}
// MARK: - APIs
extension SettingsVC {
    private func executeAPIForDeleteAccount() {
        let params: [String: Any] = ["user_id" : UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"]
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.deleteAccount(params: params) { (errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred")
                if errorCode == 100 {
                    UserDefaults.standard.setValue("No", forKey: Constant.userDefaults.kIsRegistered)
                    self.moveToLogin()
                }
            }
        }
    }
}
