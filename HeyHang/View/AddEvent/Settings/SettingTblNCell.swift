//
//  SettingTblNCell.swift
//  HeyHang
//
//  Created by Mohd Arsad on 18/06/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

class SettingTblNCell: UITableViewCell {
    
    @IBOutlet weak var logoIV: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var descriptionLblHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var toggleBtn: UIButton!
    @IBOutlet weak var detailBtn: UIButton!
    
    @IBOutlet weak var toggleBtnWidthConstraints: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
