//
//  AddEventVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 6/7/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit
import EventKit
import GooglePlacePicker

class AddEventVC: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var eventTitleTF: MATextField!
    @IBOutlet weak var eventDateTF: MATextField!
    @IBOutlet weak var endDateTF: MATextField!
    @IBOutlet weak var eventLocationTF: MATextField!
    @IBOutlet weak var submitBtn: UIButton!
    
    private var timeFormatter: String = "dd-MM-yyyy hh:mm a"
    private var latitude: Double = 0.0
    private var longitude: Double = 0.0
    private var startDate: Date = Date()
    private var endDate: Date = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.eventTitleTF.floatingDisplayStatus = .never
        self.eventDateTF.floatingDisplayStatus = .never
        self.endDateTF.floatingDisplayStatus = .never
        self.eventLocationTF.floatingDisplayStatus = .never
    }
    override func viewWillLayoutSubviews() {
        self.cardView.setLightDropShadow(cornerRadius: 10.0)
        self.submitBtn.layer.cornerRadius = Constant.kCornerRadius
        self.submitBtn.clipsToBounds = true
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickEventStartDateBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerController") as! DatePickerController
        vc.modalPresentationStyle = .overCurrentContext
        vc.delegate = self
        vc.dateFormatter = timeFormatter
        vc.dateStr = self.eventDateTF.text ?? ""
        vc.tag = 0
        vc.datePickerMode = .dateAndTime
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func onClickStartEndDateBtn(_ sender: Any) {
        if self.eventDateTF.text?.count == 0 {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please select start time first")
            return
        }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerController") as! DatePickerController
        vc.modalPresentationStyle = .overCurrentContext
        vc.delegate = self
        vc.dateFormatter = timeFormatter
        vc.dateStr = self.endDateTF.text ?? ""
        vc.tag = 1
        vc.datePickerMode = .dateAndTime
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func onClickLocationBtn(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.coordinate.rawValue) | UInt(GMSPlaceField.formattedAddress.rawValue))!
        autocompleteController.placeFields = fields
        
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .noFilter
        autocompleteController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)
    }
    @IBAction func onClickSubmitBtn(_ sender: Any) {
        if validate() {
            self.insertNewEvent()
        }
    }
    private func validate() -> Bool {
        if (self.eventTitleTF.text?.isEmpty)! {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter hang title")
            return false
        }
        if (self.eventDateTF.text?.isEmpty)! {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please select hang start date and time")
            return false
        }
        if (self.endDateTF.text?.isEmpty)! {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please select hang end date and time")
            return false
        }
        if (self.eventLocationTF.text?.isEmpty)! {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please select hang location")
            return false
        }
        return true
    }
}
extension AddEventVC: DatePickerDelegate {
    func datePicker(onSelectedDate date: Date, tag: Int) {
        let formatter = DateFormatter()
        formatter.dateFormat = timeFormatter
        if tag == 0 {
            self.eventDateTF.text = formatter.string(from: date)
            self.startDate = date
            self.endDateTF.text = ""
        } else {
            if let startTime = formatter.date(from: self.eventDateTF.text ?? "") {
                if date.compare(startTime) == .orderedAscending {
                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "End date time must be greater than start date time")
                    return
                }
            }
            self.endDateTF.text = formatter.string(from: date)
            self.endDate = date
        }
    }
}
extension AddEventVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.latitude = place.coordinate.latitude
        self.longitude = place.coordinate.longitude
        self.eventLocationTF.text = place.formattedAddress?.components(separatedBy: ", ")
            .joined(separator: ",") ?? ""
        
        dismiss(animated: true, completion: nil)
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
// MARK: - Initialize Calendar Creation
extension AddEventVC {
    private func insertNewEvent() {
        if let eventId = Sync.instance.insertNewEvent(by: self.eventTitleTF.text ?? "", start: startDate, end: endDate, location: self.eventLocationTF.text ?? "", latitude: self.latitude, longitude: self.longitude) {
            print("Event Identifier: \(eventId)")
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Hang successfully added in your calendar")
            self.navigationController?.popViewController(animated: true)
        }
    }
}
