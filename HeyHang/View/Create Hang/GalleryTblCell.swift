//
//  GalleryTblCell.swift
//  HeyHang
//
//  Created by Mohd Arsad on 18/06/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

class GalleryTblCell: UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var collectionVIew: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
