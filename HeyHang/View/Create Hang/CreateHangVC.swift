//
//  CreateHangVC.swift
//  HeyHang
//
//  Created by Mohd Arsad on 18/06/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

class InviteCollCell: UICollectionViewCell {
    
    @IBOutlet weak var mainIV: UIImageView!
    @IBOutlet weak var deleteIV: UIImageView!
    @IBOutlet weak var deleteBtn: UIButton!
    
    var user: User? {
        didSet {
            updateDetails()
        }
    }
    
    private func updateDetails() {
        if let user = user {
            self.mainIV.imageFromServerURL(user.image ?? "", placeHolder: UIImage.init(named: "profile_placeholder"))
        }
    }
}
class CreateHangVC: UIViewController {
    
    enum Privacy: Int {
        case privateT = 1
        case friendsT = 2
        case publicT = 3
        case autoAcceptT = 4
    }
    
    @IBOutlet weak var hrLbl: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var hangTypeView: UIView!
    @IBOutlet weak var paidBtn: UIButton!
    @IBOutlet weak var freeBtn: UIButton!
    @IBOutlet weak var inviteAddBtn: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var privacyMainView: UIView!
    @IBOutlet weak var privateView: UIView!
    @IBOutlet weak var privateIV: UIImageView!
    @IBOutlet weak var friendsVIew: UIView!
    @IBOutlet weak var friendIV: UIImageView!
    @IBOutlet weak var publicView: UIView!
    @IBOutlet weak var publicIV: UIImageView!
    @IBOutlet weak var autoAcceptView: UIView!
    @IBOutlet weak var autoAcceptIV: UIImageView!
    @IBOutlet weak var totalTicketTF: UITextField!
    @IBOutlet weak var amountPerTicketTF: UITextField!
    @IBOutlet weak var dinnerTonightBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    
    @IBOutlet weak var amountTicketFixedLbl: UILabel!
    @IBOutlet weak var amountTicketView: UIView!
    @IBOutlet weak var amountTicketViewHeightConstraints: NSLayoutConstraint! // 45.0
    @IBOutlet weak var amountTicketFixedLblHeightConstraints: NSLayoutConstraint! //20.0
    @IBOutlet weak var amountTicketFixedLblTopConstraints: NSLayoutConstraint! //20
    
    private var isPaidActive: Bool = false
    private var privacyType: Privacy = .privateT
    private var users: [User] = []
    private var selectedUsers: [User] = []
    
    public var eventId: String = ""
    public var selectedEvent: Event?
    public var editableParams: [String: Any]?
    
    public var paramsForCalendar: [String: Any] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       
        self.dinnerTonightBtn.isHidden = true
        dinnerTonightBtn.setAttributedTitle(dinnerTonightBtn.getUnderlineAttributeString(UIColor.white), for: .normal)
        
        updateSeletedValues()
        updateType(isPaid: isPaidActive)
        updatePrivacyType(type: privacyType)
    }
    //87 179 143
    override func viewWillLayoutSubviews() {
        self.headerView.setSideDropShadow(isUp: false)
        self.hangTypeView.layer.cornerRadius = 5.0
        self.hangTypeView.clipsToBounds = true
        
        self.inviteAddBtn.setCircleCorner()
        
        self.privateView.layer.cornerRadius = 10.0
        self.privateView.clipsToBounds = true
        self.friendsVIew.layer.cornerRadius = 10.0
        self.friendsVIew.clipsToBounds = true
        self.publicView.layer.cornerRadius = 10.0
        self.publicView.clipsToBounds = true
        self.autoAcceptView.layer.cornerRadius = 10.0
        self.autoAcceptView.clipsToBounds = true
        
        self.nextBtn.layer.cornerRadius = Constant.kCornerRadius
        self.nextBtn.clipsToBounds = true
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    private func updateSeletedValues() {
        if let event = self.selectedEvent {
            self.hrLbl.text = "Update Hang"
            isPaidActive = (event.type == .paid) ? true : false
            self.totalTicketTF.text = "\(event.ticketCount)"
            self.amountPerTicketTF.text = event.ticketPrice
            self.totalTicketTF.isEnabled = false
            self.amountPerTicketTF.isEnabled = false
            if event.privacy == "1" {
                privacyType = .privateT
            } else if event.privacy == "2" {
                privacyType = .friendsT
            } else if event.privacy == "3" {
                privacyType = .publicT
            } else if event.privacy == "4" {
                privacyType = .autoAcceptT
            }
        }
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickFreeBtn(_ sender: Any) {
        if self.selectedEvent != nil {
            return
        }
        updateType(isPaid: false)
    }
    @IBAction func onCLickPaidBtn(_ sender: Any) {
        if self.selectedEvent != nil {
            return
        }
        updateType(isPaid: true)
    }
    @IBAction func onClickPrivateBtn(_ sender: Any) {
        if self.selectedEvent != nil {
            return
        }
        updatePrivacyType(type: .privateT)
    }
    @IBAction func onClickFriendBtn(_ sender: Any) {
        if self.selectedEvent != nil {
            return
        }
        updatePrivacyType(type: .friendsT)
    }
    @IBAction func onClickPublicBtn(_ sender: Any) {
        if self.selectedEvent != nil {
            return
        }
        updatePrivacyType(type: .publicT)
    }
    @IBAction func onClickAutoAcceptBtn(_ sender: Any) {
        if self.selectedEvent != nil {
            return
        }
        updatePrivacyType(type: .autoAcceptT)
    }
    @IBAction func onClickDinnerTonightBtn(_ sender: Any) {
        print("Dinner Tonight Btn")
    }
    @IBAction func onClickNextBtn(_ sender: Any) {
        if checkInternet() {
            if let param = self.validateParams() {
                if let event = self.selectedEvent {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateHangGalleryVC") as! CreateHangGalleryVC
                    vc.eventId = self.eventId
                    vc.selectedEvent = event
                    vc.editableParams = param
                    vc.paramsForCalendar = self.paramsForCalendar
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    self.executeAPIForAddingDetails(params: param)
                }
            }
        }
    }
    
    @IBAction func onClickInviteAddBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MemberListVC") as! MemberListVC
        vc.modalPresentationStyle = .overCurrentContext
        vc.users = self.users
        vc.selectedIds = self.selectedUsers.map({ return ($0.userId ?? "0") })
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
}
extension CreateHangVC: MemberListDelegate {
    func memberList(seelectedUsers users: [User]) {
        self.selectedUsers = users
        self.collectionView.reloadData()
    }
}
extension CreateHangVC {
    private func updateType(isPaid: Bool) {
        self.isPaidActive = isPaid
        if isPaid {
            self.paidBtn.backgroundColor = UIColor.init(red: 87/255.0, green: 179/255.0, blue: 143/255.0, alpha: 1.0)
            self.paidBtn.layer.cornerRadius = 5.0
            self.freeBtn.backgroundColor = UIColor.clear
            self.autoAcceptView.isHidden = true
            
            self.amountTicketFixedLblTopConstraints.constant = 20.0
            self.amountTicketFixedLblHeightConstraints.constant = 20.0
            self.amountTicketViewHeightConstraints.constant = 45.0
            self.amountTicketFixedLbl.isHidden = false
            self.amountTicketView.isHidden = false
            
        } else {
            self.freeBtn.backgroundColor = UIColor.init(red: 87/255.0, green: 179/255.0, blue: 143/255.0, alpha: 1.0)
            self.freeBtn.layer.cornerRadius = 5.0
            self.paidBtn.backgroundColor = UIColor.clear
            self.autoAcceptView.isHidden = false
            
            self.amountTicketFixedLblTopConstraints.constant = 0.0
            self.amountTicketFixedLblHeightConstraints.constant = 0.0
            self.amountTicketViewHeightConstraints.constant = 0.0
            self.amountTicketFixedLbl.isHidden = true
            self.amountTicketView.isHidden = true
        }
    }
    private func updatePrivacyType(type: Privacy) {
        self.privacyType = type
        self.privateIV.image = UIImage.init(named: "radio_normal")
        self.friendIV.image = UIImage.init(named: "radio_normal")
        self.publicIV.image = UIImage.init(named: "radio_normal")
        self.autoAcceptIV.image = UIImage.init(named: "radio_normal")
        switch type {
        case .privateT:
            self.privateIV.image = UIImage.init(named: "tick_green")
            break
        case .friendsT:
            self.friendIV.image = UIImage.init(named: "tick_green")
            break
        case .publicT:
            self.publicIV.image = UIImage.init(named: "tick_green")
            break
        case .autoAcceptT:
            self.autoAcceptIV.image = UIImage.init(named: "tick_green")
            break
        }
        if checkInternet() {
            self.executeAPIForUserList()
        }
    }
}
extension CreateHangVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.selectedUsers.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! InviteCollCell
        cell.user = self.selectedUsers[indexPath.row]
        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action: #selector(onClickDeleteUserCellBtn(_:)), for: .touchUpInside)
        cell.mainIV.setCircleCorner()
        if !self.selectedUsers[indexPath.row].isInvited {
            cell.deleteIV.isHidden = false
            cell.deleteBtn.isHidden = false
        } else {
            cell.deleteIV.isHidden = true
            cell.deleteBtn.isHidden = true
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 45.0, height: 45.0)
    }
    @IBAction func onClickDeleteUserCellBtn(_ sender: UIButton) {
        self.selectedUsers.remove(at: sender.tag)
        self.collectionView.reloadData()
    }
}

// MARK: - APIs
extension CreateHangVC {
    private func validateParams() -> [String: Any]? {
        
        let tempArr = self.selectedUsers.filter({ return !($0.isInvited) })
        if self.selectedEvent == nil {
            if (self.privacyType == .privateT) || (self.privacyType == .friendsT) {
                if tempArr.count == 0 {
                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please invite at least one member")
                    return nil
                }
            }
        }
        guard let ticket = self.totalTicketTF.text, !(ticket.count == 0), !(ticket == "0") else {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter number of people")
            return nil
        }
        if isPaidActive {
            guard !(self.amountPerTicketTF.text?.count == 0) else {
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter amount of ticket")
                return nil
            }
            guard !(self.privacyType == .autoAcceptT) else {
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please select privacy type")
                return nil
            }
        }
        
        if let paramsT = editableParams {
            let selectedIds = tempArr.map({ return ($0.userId ?? "0") })
            
            var params = paramsT
            params["event_id"] = self.eventId
            params["event_type"] = isPaidActive ? "1" : "2"
            params["event_privacy"] = "\(self.privacyType.rawValue)"
            params["members"] = selectedIds.joined(separator: ",")
            params["total_ticket"] = ticket
            params["ticket_price"] = isPaidActive ? (self.amountPerTicketTF.text ?? "0") : ""
            params["group_id"] = ""
            return params
        } else {
            let selectedIds = tempArr.map({ return ($0.userId ?? "0") })
            
            var params = [String: Any]()
            params["event_id"] = self.eventId
            params["event_type"] = isPaidActive ? "1" : "2"
            params["event_privacy"] = "\(self.privacyType.rawValue)"
            params["members"] = selectedIds.joined(separator: ",")
            params["total_ticket"] = ticket
            params["group_id"] = ""
            params["ticket_price"] = isPaidActive ? (self.amountPerTicketTF.text ?? "0") : ""
            return params
        }
        
        //('event_id', 'event_type','event_privacy', 'members', 'total_ticket', 'ticket_price')
    }
    private func executeAPIForAddingDetails(params: [String: Any]) {
        print(params)
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.addDetailCreateEvent(params: params) { (images, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                if let images = images {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateHangGalleryVC") as! CreateHangGalleryVC
                    vc.suggestedImages = images
                    vc.eventId = self.eventId
                    vc.paramsForCalendar = self.paramsForCalendar
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred")
                }
            }
        }
    }
    
    private func executeAPIForUserList() {
        //'user_id','event_privacy'
        var params = [String: Any]()
        params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? ""
        params["event_privacy"] = "\(self.privacyType.rawValue)"
        params["event_id"] = self.eventId
        
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.userList(params: params) { (users, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                if let userArr = users {
                    self.users = userArr
                    self.selectedUsers = self.users.filter({ return $0.isInvited })
                    self.collectionView.reloadData()
                }
            }
        }
    }
}
