//
//  CreateHangMainVC.swift
//  HeyHang
//
//  Created by Mohd Arsad on 18/06/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit
import GooglePlacePicker

class CreateHangMainVC: UIViewController {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var hdrLbl: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var titleTF: UITextField!
    
    @IBOutlet weak var locationTF: UITextField!
    @IBOutlet weak var startTimeTF: UITextField!
    @IBOutlet weak var endTimeTF: UITextField!
    @IBOutlet weak var descriptionTV: UITextView!
    @IBOutlet weak var nextBtn: UIButton!
    
    private var timeFormatter: String = "dd-MM-yyyy hh:mm a"
    private var latitude: String = ""
    private var longitude: String = ""
    private let textViewPlaceholder: String = "Enter some description"
    private var startDate: Date?
    private var endDate: Date?
    
    var selectedEvent: Event?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        self.titleTF.attributedPlaceholder = NSAttributedString(string: "Hang Title*",
                                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        self.locationTF.attributedPlaceholder = NSAttributedString(string: "Location",
                                                                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        self.startTimeTF.attributedPlaceholder = NSAttributedString(string: "Pick a date time",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        self.endTimeTF.attributedPlaceholder = NSAttributedString(string: "Pick a date time",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        self.locationTF.isEnabled = false
        self.startTimeTF.isEnabled = false
        self.endTimeTF.isEnabled = false
        self.descriptionTV.textColor = UIColor.lightGray
        self.descriptionTV.text = textViewPlaceholder
        self.descriptionTV.delegate = self
        self.locationTF.text = ""
        
        if let event = self.selectedEvent {
            
            let startDate = Date(timeIntervalSince1970: event.startDateTimeInterval ?? 0.0)
            let endDate = Date(timeIntervalSince1970: event.endDateTimeInterval ?? 0.0)
            let formatter = DateFormatter()
            formatter.dateFormat = timeFormatter
            let startDateTime = formatter.string(from: startDate)
            let endDateTime = formatter.string(from: endDate)
            
            self.startTimeTF.text = startDateTime
            self.endTimeTF.text = endDateTime
            
            self.startDate = startDate
            self.endDate = endDate
            
            self.hdrLbl.text = "Update Hang"
            self.titleTF.text = event.name
            self.locationTF.text = event.location
            self.descriptionTV.text = event.descriptionStr
            
            self.latitude = event.latitude ?? ""
            self.longitude = event.longitude ?? ""
            
            self.titleTF.isEnabled = false
            self.startTimeTF.isEnabled = false
            self.endTimeTF.isEnabled = false
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillLayoutSubviews() {
        self.nextBtn.layer.cornerRadius = Constant.kCornerRadius
        self.nextBtn.clipsToBounds = true
        self.headerView.setSideDropShadow(isUp: false)
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickLocationBtn(_ sender: Any) {
        if self.selectedEvent != nil {
            return
        }
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.coordinate.rawValue) | UInt(GMSPlaceField.formattedAddress.rawValue))!
        autocompleteController.placeFields = fields
        
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .noFilter
        autocompleteController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)
    }
    @IBAction func onCLickStartTimeBtn(_ sender: Any) {
        if self.selectedEvent != nil {
            return
        }
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerController") as! DatePickerController
        vc.modalPresentationStyle = .overCurrentContext
        vc.delegate = self
        vc.dateFormatter = timeFormatter
        vc.dateStr = self.startTimeTF.text ?? ""
        vc.tag = 0
        vc.datePickerMode = .dateAndTime
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func onCLickEndTimeBtn(_ sender: Any) {
        if self.selectedEvent != nil {
            return
        }
        if self.startTimeTF.text?.count == 0 {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please select start time first")
            return
        }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerController") as! DatePickerController
        vc.modalPresentationStyle = .overCurrentContext
        vc.delegate = self
        vc.dateFormatter = timeFormatter
        vc.dateStr = self.endTimeTF.text ?? ""
        vc.tag = 1
        vc.datePickerMode = .dateAndTime
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func onCLickNextBtn(_ sender: Any) {
        if checkInternet() {
            if let params = validateParams() {
                print(params)
                if let event = self.selectedEvent {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateHangVC") as! CreateHangVC
                    vc.eventId = event.eventId ?? "0"
                    vc.editableParams = params
                    vc.selectedEvent = self.selectedEvent
                    vc.paramsForCalendar = params
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    self.executeAPIForCreateHang(params: params)
                }
            }
        }
    }
}
extension CreateHangMainVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.latitude = String(place.coordinate.latitude)
        self.longitude = String(place.coordinate.longitude)
        self.locationTF.text = place.formattedAddress?.components(separatedBy: ", ")
            .joined(separator: ",") ?? ""
        
        dismiss(animated: true, completion: nil)
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
extension CreateHangMainVC: DatePickerDelegate {
    func datePicker(onSelectedDate date: Date, tag: Int) {
        let formatter = DateFormatter()
        formatter.dateFormat = timeFormatter
        if tag == 0 {
            self.startTimeTF.text = formatter.string(from: date)
            self.startDate = date
            self.endTimeTF.text = ""
        } else {
            if let startTime = formatter.date(from: self.startTimeTF.text ?? "") {
                if ((date.compare(startTime) == .orderedAscending) || (date.compare(startTime) == .orderedSame)) {
                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "End date time must be greater than start date time")
                    return
                }
            }
            self.endTimeTF.text = formatter.string(from: date)
            self.endDate = date
        }
    }
}
extension CreateHangMainVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == textViewPlaceholder {
            self.descriptionTV.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            self.descriptionTV.text = textViewPlaceholder
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "" {
            return true
        }
        if textView.text.count == 0 && text == " " {
            return false
        }
        return true
    }
}
// MARK: - APIs helping Method
extension CreateHangMainVC {
    private func validateParams() -> [String: Any]? {
        
        guard let name = self.titleTF.text, !(self.titleTF.text?.count == 0) else {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter title")
            return nil
        }
        guard let location = self.locationTF.text, !(self.locationTF.text?.count == 0) else {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please select location")
            return nil
        }
        guard !(self.startTimeTF.text?.count == 0) else {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please select start date time")
            return nil
        }
//        if (self.descriptionTV.text == textViewPlaceholder) {
//            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please enter hang description")
//            return nil
//        }
//        guard !(self.endTimeTF.text?.count == 0) else {
//            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please select end date time")
//            return nil
//        }
        
        var params = [String: Any]()
        params["event_id"] = ""
        params["user_id"] = UserDefaults.standard.string(forKey: Constant.userDefaults.kUserId) ?? "0"
        params["event_name"] = name
        params["event_location"] = location
        if let sDate = startDate {
            params["start_datetime"] = String(format: "%.f", sDate.timeIntervalSince1970)
        } else {
            params["start_datetime"] = ""
        }
        if let eDate = endDate {
            params["end_datetime"] = String(format: "%.f", eDate.timeIntervalSince1970)
        } else {
            params["end_datetime"] = ""
        }
        params["event_description"] = (self.descriptionTV.text == textViewPlaceholder) ? "" : self.descriptionTV.text
        params["lat"] = self.latitude
        params["lng"] = self.longitude
        return params
    }
    private func executeAPIForCreateHang(params: [String: Any]) {
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.createEvent(params: params) { (eventId, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                if errorCode == 100 {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateHangVC") as! CreateHangVC
                    vc.eventId = eventId ?? "0"
                    vc.paramsForCalendar = params
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred")
                }
            }
        }
    }
}
