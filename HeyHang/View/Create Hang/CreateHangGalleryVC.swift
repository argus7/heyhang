//
//  CreateHangGalleryVC.swift
//  HeyHang
//
//  Created by Mohd Arsad on 18/06/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit
import AKSideMenu

class CreateHangGalleryVC: UIViewController {

    @IBOutlet weak var hdrLbl: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var createBtn: UIButton!
    
    var suggestedImages: [SImage] = []
    var eventId: String = ""
    private var attachmentImages: [SImage] = []
    private let imagePicker =  UIImagePickerController()
    public var selectedEvent: Event?
    public var editableParams: [String: Any]?
    
    public var paramsForCalendar: [String: Any] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let _ = self.selectedEvent {
            self.hdrLbl.text = "Update Hang"
            self.createBtn.setTitle("Update", for: .normal)
        }
        self.attachmentImages.append(SImage(jsonData: [:]))
        if editableParams != nil {
            self.suggestedImages = getSelectedSuggested()
            self.updateSelectedAttached()
        }
        setupTableView()
    }
    override func viewWillLayoutSubviews() {
        self.headerView.setSideDropShadow(isUp: false)
        self.createBtn.layer.cornerRadius = Constant.kCornerRadius
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickCreateBtn(_ sender: Any) {
        if checkInternet() {
            if let param = validateParams() {
                if editableParams != nil {
                    self.executeAPIForUpdateEditDetails(params: param)
                } else {
                    self.executeAPIForAddingGalleryDetails(params: param)
                }
            }
        }
    }
    
    private func getSelectedSuggested() -> [SImage] {
        var stemp: [SImage] = []
        if let event = self.selectedEvent {
            for object in event.suggestedImgArr {
                let isExist = event.imageArr.filter { (first) -> Bool in
                    return first.id == object.id
                }
                if isExist.count > 0 {
                    object.isSelected = true
                }
                stemp.append(object)
            }
        }
        return stemp
    }
    private func updateSelectedAttached() {
        if let event = self.selectedEvent {
            for object in event.imageArr {
                let isExist = self.suggestedImages.filter { (first) -> Bool in
                    return (first.id == object.id)
                }
                if isExist.count > 0 { } else {
                    object.isSelected = true
                    self.attachmentImages.append(object)
                }
            }
        }
    }
}
extension CreateHangGalleryVC: SuccessPopUpDelegate {
    func onActionPerform(on type: SuccessPopUpNavigation) {
        if (type == .createHang) || (type == .updateHang) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyEventVC") as! MyEventVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
extension CreateHangGalleryVC: UITableViewDelegate, UITableViewDataSource {
    fileprivate func setupTableView() {
        self.tableView.register(UINib(nibName: "GalleryTblCell", bundle: Bundle.main), forCellReuseIdentifier: "TCell")
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if suggestedImages.count > 0 {
            return 2
        } else {
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "TCell", for: indexPath) as! GalleryTblCell
        
        if indexPath.row == 0 {
            cell.titleLbl.text = "Hang Media/ Images"
            cell.descriptionLbl.text = "Upload from your device"
        } else {
            cell.titleLbl.text = "Suggested Images"
            cell.descriptionLbl.text = "Based on selected location"
        }
        
        cell.collectionVIew.tag = indexPath.row
        cell.collectionVIew.register(UINib(nibName: "GalleryCollCell", bundle: Bundle.main), forCellWithReuseIdentifier: "CCell")
        cell.collectionVIew.delegate = self
        cell.collectionVIew.dataSource = self
        cell.collectionVIew.isScrollEnabled = false
        cell.collectionVIew.reloadData()
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return (CGFloat(getAttachedRows()) * 110.0) + 55.0
        } else {
            return (CGFloat(getSuggestedRows()) * 110.0) + 55.0
        }
    }
    private func getSuggestedRows() -> Int {
        var rows = (self.suggestedImages.count / 2)
        rows = (rows * 2 == self.suggestedImages.count) ? rows : (rows + 1)
        return rows
    }
    private func getAttachedRows() -> Int {
        var rows = (self.attachmentImages.count / 2)
        rows = (rows * 2 == self.attachmentImages.count) ? rows : (rows + 1)
        return rows
    }
}
// MARK: - UIImagePickerControllerDelegate, UINavigationControllerDelegate
extension CreateHangGalleryVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            if let imgData = pickedImage.jpegData(compressionQuality: 0.3) {
                self.executeUploadImageApi(imgData: imgData, image: pickedImage)
            }
        }
        dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
}
extension CreateHangGalleryVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 0 {
            return self.attachmentImages.count
        } else {
            return self.suggestedImages.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CCell", for: indexPath) as! GalleryCollCell
        cell.addBtn.layer.cornerRadius = 10.0
        cell.mainIV.layer.cornerRadius = 10.0
        cell.mainIV.clipsToBounds = true
        
        cell.mainIV.contentMode = .scaleAspectFill
        if collectionView.tag == 0 { //For Attached Images
            if indexPath.row == 0 {
                cell.mainIV.isHidden = true
                cell.closeBtn.isHidden = true
                cell.addBtn.isHidden = false
                cell.addBtn.addTarget(self, action: #selector(onClickAddImages(_:)), for: .touchUpInside)
            } else {
                cell.mainIV.isHidden = false
                cell.closeBtn.isHidden = false
                cell.addBtn.isHidden = true
                cell.closeBtn.setImage(UIImage(named: "cross_circle_blue")!, for: .normal)
                
                let object = self.attachmentImages[indexPath.row]
                cell.mainIV.imageFromServerURL(object.url ?? "", placeHolder: nil)
                cell.closeBtn.isHidden = false
            }
            cell.closeBtn.tag = indexPath.row
            cell.closeBtn.addTarget(self, action: #selector(onClickDeleteImages(_:)), for: .touchUpInside)
        } else {
            cell.mainIV.isHidden = false
            cell.addBtn.isHidden = true
            cell.closeBtn.isHidden = false
            
            let object = self.suggestedImages[indexPath.row]
            cell.mainIV.imageFromServerURL(object.url ?? "", placeHolder: nil)
            if object.isSelected {
                cell.closeBtn.isHidden = false
            } else {
                cell.closeBtn.isHidden = true
            }
            cell.closeBtn.setImage(UIImage(named: "tick_green")!, for: .normal)
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let totalWidth = collectionView.bounds.width
        return CGSize(width: (totalWidth - 20)/2, height: 90)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 0 { //Attached Images
            if indexPath.row > 0 {
                attachmentImages[indexPath.row].isSelected = !attachmentImages[indexPath.row].isSelected
            }
            let indexPath = IndexPath.init(row: 0, section: 0)
            self.tableView.reloadRows(at: [indexPath], with: .none)
        } else { //Suggested Images
            suggestedImages[indexPath.row].isSelected = !suggestedImages[indexPath.row].isSelected
            let indexPathT = IndexPath(row: 1, section: 0)
            self.tableView.reloadRows(at: [indexPathT], with: .none)
        }
    }
    @IBAction func onClickAddImages(_ sender: Any) {
        self.imagePicker.delegate =  self
        showImageSelectionAlert(picker: imagePicker)
    }
    @IBAction func onClickDeleteImages(_ sender: UIButton) {
        if sender.tag < self.attachmentImages.count {
            self.attachmentImages.remove(at: sender.tag)
            let indexPath = IndexPath.init(row: 0, section: 0)
            self.tableView.reloadRows(at: [indexPath], with: .none)
        }
    }
}
extension CreateHangGalleryVC {
    private func validateParams() -> [String: Any]? {
        var idArr = self.suggestedImages.filter({ return $0.isSelected })
        let attachedIdArr = self.attachmentImages.filter({ return $0.isSelected })
        idArr += attachedIdArr
        let idTStr = idArr.map({ return $0.id ?? "" })
        if idArr.count == 0 {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Please select images")
            return nil
        }
        //event_id','images
        if var params = self.editableParams {
            params["event_id"] = self.eventId
            params["images"] = idTStr.joined(separator: ",")
            if let eventIdentifier = self.getUpdateEventID() {
                params["calendar_event_id"] = eventIdentifier
            }
//            else {
//                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Event not updated in calendar")
//                return nil
//            }
            return params
        } else {
            var params = [String: Any]()
            params["event_id"] = self.eventId
            params["images"] = idTStr.joined(separator: ",")
            if let eventIdentifier = self.getCreatedEventID() {
                params["calendar_event_id"] = eventIdentifier
            }
//            else {
//                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "Event not created in calendar")
//                return nil
//            }
            return params
        }
    }
    private func executeAPIForAddingGalleryDetails(params: [String: Any]) {
        print(params)
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.addGalleryDetailCreateEvent(params: params) { (isSuccess, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                if isSuccess {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SuccessPopUpVC") as! SuccessPopUpVC
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.navigation = .createHang
                    vc.delegate = self
                    self.present(vc, animated: true, completion: nil)
                } else {
                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred")
                }
            }
        }
    }
    private func executeAPIForUpdateEditDetails(params: [String: Any]) {
        print(params)
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.updateEventDetailUpdateEvent(params: params) { (isSuccess, errorMessage, errorCode) in
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
                if isSuccess {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SuccessPopUpVC") as! SuccessPopUpVC
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.navigation = .updateHang
                    vc.delegate = self
                    self.present(vc, animated: true, completion: nil)
                } else {
                    self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "Some Error Occurred")
                }
            }
        }
    }
    private func executeUploadImageApi(imgData: Data, image: UIImage?) {
        self.showActivityIndicator(uiView: self.view)
        RestAPI.shared.uploadImageId(imgData: imgData) { (imageObject, errorMessage, errorCode) in
            self.hideActivityIndicator(uiView: self.view)
            if errorCode != 100 {
                self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: errorMessage ?? "")
            } else {
                if let object = imageObject {
                    let sImage = SImage(jsonData: [:])
                    sImage.id = object.keys.first
                    sImage.url = object.values.first
                    sImage.image = image
                    sImage.isSelected = true
                    self.attachmentImages.append(sImage)
                    
                    let indexPath = IndexPath(row: 0, section: 0)
                    self.tableView.reloadRows(at: [indexPath], with: .none)
                }
            }
        }
    }
    
    //Caledar Suppport Methods
    private func getUpdateEventID() -> String? {
        let startDateT = TimeInterval("\(self.paramsForCalendar["start_datetime"] ?? "0.0")") ?? 0.0
        let endDateT = TimeInterval("\(self.paramsForCalendar["end_datetime"] ?? "0.0")") ?? 0.0
        
        let calEventId = self.selectedEvent?.calendarEventId ?? ""
        let title = self.paramsForCalendar["event_name"] as? String ?? ""
        let startDate = Date(timeIntervalSince1970: startDateT)
        let endDate = Date(timeIntervalSince1970: endDateT)
        let location = self.paramsForCalendar["event_location"] as? String ?? ""
        let latitude = Double("\(self.paramsForCalendar["lat"] ?? "0.0")") ?? 0.0
        let longitude = Double("\(self.paramsForCalendar["lng"] ?? "0.0")") ?? 0.0
        
        return Sync.instance.updateOldEvent(by: title, identifier: calEventId, start: startDate, end: endDate, location: location, latitude: latitude, longitude: longitude)
    }
    private func getCreatedEventID() -> String? {
        
        let startDateT = TimeInterval("\(self.paramsForCalendar["start_datetime"] ?? "0.0")") ?? 0.0
        let endDateT = TimeInterval("\(self.paramsForCalendar["end_datetime"] ?? "0.0")") ?? 0.0
        
        let title = self.paramsForCalendar["event_name"] as? String ?? ""
        let startDate = Date(timeIntervalSince1970: startDateT)
        let endDate = Date(timeIntervalSince1970: endDateT)
        let location = self.paramsForCalendar["event_location"] as? String ?? ""
        let latitude = Double("\(self.paramsForCalendar["lat"] ?? "0.0")") ?? 0.0
        let longitude = Double("\(self.paramsForCalendar["lng"] ?? "0.0")") ?? 0.0
        
        return Sync.instance.insertNewEvent(by: title, start: startDate, end: endDate, location: location, latitude: latitude, longitude: longitude)
    }
}
