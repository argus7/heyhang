//
//  MemberListVC.swift
//  HeyHang
//
//  Created by Anup Kumar on 6/24/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit

protocol MemberListDelegate {
    func memberList(seelectedUsers users:[User])
}
class MemberListVC: UIViewController {
    
    @IBOutlet weak var backPopUpView: UIView!
    @IBOutlet weak var mainPopUpView: UIView!
    @IBOutlet weak var mainPopUpHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var sliderLbl: UILabel!
    @IBOutlet weak var hdrLbl: UILabel!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    var delegate: MemberListDelegate?
    private var panGesture = UIPanGestureRecognizer()
    var users: [User] = []
    private var displayUsers: [User] = []
    var selectedIds: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.displayUsers = users
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        self.setupTableView()
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.draggedView(_:)))
        mainPopUpView.isUserInteractionEnabled = true
        mainPopUpView.addGestureRecognizer(panGesture)
        self.searchTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.3) {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.15)
        }
    }
    override func viewWillLayoutSubviews() {
        self.mainPopUpView.setCorner(corners: [.topLeft, .topRight], radius: 20.0)
    }
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onClickDoneBtn(_ sender: Any) {
        self.dismiss(animated: true) {
            let userTemp = self.displayUsers.filter({ (userT) -> Bool in
                return self.selectedIds.contains(userT.userId ?? "0")
            })
            self.delegate?.memberList(seelectedUsers: userTemp)
        }
    }
    @objc func draggedView(_ sender:UIPanGestureRecognizer){

        let translation = sender.location(in: self.view)
        
        let window = UIApplication.shared.keyWindow
        let topPadding = window?.safeAreaInsets.top ?? 20.0
        let headerHeight: CGFloat = 44.0
        let animationTime: TimeInterval = 0.2
//        let animationHideTime: TimeInterval = 0.5
        let initialHeight: CGFloat = 220.0
//        let viewHeightForHide: CGFloat = 0.0
        
        if translation.y > (topPadding + headerHeight) {
            let height: CGFloat = UIScreen.main.bounds.height - translation.y
            if height < initialHeight {
//                UIView.animate(withDuration: animationHideTime, animations: {
//                    DispatchQueue.main.async {
//                        self.mainPopUpHeightConstraints.constant = viewHeightForHide
//                        self.viewWillLayoutSubviews()
//                    }
//                }) { (isSuccess) in
//                    self.dismiss(animated: false, completion: nil)
//                }
            } else {
                UIView.animate(withDuration: animationTime) {
                    DispatchQueue.main.async {
                        self.mainPopUpHeightConstraints.constant = height
                        self.viewWillLayoutSubviews()
                    }
                }
            }
        }
    }
}
extension MemberListVC: UITextFieldDelegate {
    @objc func textFieldDidChange(_ theTextField: UITextField) {
        let filterData = self.users.filter { (user) -> Bool in
            if (user.fullName ?? "").lowercased().contains(theTextField.text?.lowercased() ?? "") {
                return true
            }
            return false
        }
        self.displayUsers = (filterData.count > 0) ? filterData : self.users
        self.tableView.reloadData()
    }
}
extension MemberListVC: UITableViewDelegate, UITableViewDataSource {
    fileprivate func setupTableView() {
        self.tableView.register(UINib(nibName: "CreateGroupTableCell", bundle: Bundle.main), forCellReuseIdentifier: "CreateGroupTableCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.displayUsers.count > 0 {
            self.tableView.backgroundView = nil
            return self.displayUsers.count
        } else {
            self.tableView.showBackgroundMsg(msg: "No Users Found")
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "CreateGroupTableCell", for: indexPath) as! CreateGroupTableCell
        let user = self.displayUsers[indexPath.row]
        cell.selectedIV.image = UIImage(named: "tick_circle_green")
        if selectedIds.contains(user.userId ?? "0") {
            cell.selectedIV.isHidden = false
        } else {
            cell.selectedIV.isHidden = true
        }
        cell.user = user
        cell.detailBtn.tag = indexPath.row
        cell.detailBtn.addTarget(self, action: #selector(onClickDetailBtn(_:)), for: .touchUpInside)
        if !user.isInvited {
            cell.alpha = 1.0
//            cell.detailBtn.isUserInteractionEnabled = true
        } else {
            cell.alpha = 0.4
//            cell.detailBtn.isUserInteractionEnabled = false
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    @objc func onClickDetailBtn(_ sender: UIButton) {
        let user = self.displayUsers[sender.tag]
        if !user.isInvited {
            if selectedIds.contains(user.userId ?? "0") {
                let index = getIndex(object: user.userId ?? "0")
                if index < selectedIds.count {
                    selectedIds.remove(at: index)
                }
            } else {
                selectedIds.append(user.userId ?? "0")
            }
            self.tableView.reloadData()
        } else {
            self.showAlertMessage(titleStr: Constant.APP_NAME, messageStr: "\(user.fullName ?? "User") is already invited!")
        }
    }
    private func getIndex(object: String) -> Int {
        var count: Int = 0
        for obj in selectedIds {
            if object == obj {
                return count
            }
            count += 1
        }
        return 0
    }
}

