//
//  AppDelegate.swift
//  HeyHang
//
//  Created by Anup Kumar on 5/27/19.
//  Copyright © 2019 Mohd Arsad. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import AKSideMenu
import UserNotifications
import CoreLocation
import GooglePlacePicker
import GoogleMaps
import GooglePlaces
import GoogleSignIn
import Firebase
import UberCore
import LyftSDK
import Stripe

var isAppTerminate: Bool = false
var isAppInBackground: Bool = false
var currentChatId: String = ""

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,CLLocationManagerDelegate {

    var window: UIWindow?
    var navigation : UINavigationController?
    var locationManager  =  CLLocationManager()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        isAppTerminate = true
        IQKeyboardManager.shared.enable =  true
        self.navigation = self.window?.rootViewController as? UINavigationController
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        registerNotificationMethods(application : application)
        UIApplication.shared.applicationIconBadgeNumber = 0;
        
        LocationHelper.instance.getuserCurrentLattLong(delegate: nil)
        
        GMSPlacesClient.provideAPIKey("AIzaSyBUQgbaFxvzE79Ft3Biphb90Zj6NRrOWGA")
        GMSServices.provideAPIKey("AIzaSyBUQgbaFxvzE79Ft3Biphb90Zj6NRrOWGA")
        GIDSignIn.sharedInstance().clientID = "150778564273-bueldsvh99s6dcsj4irdfh29vbf6ueva.apps.googleusercontent.com"
        
        
        //Lyft Setup
        LyftConfiguration.developer = (token: "...", clientId: "...")
        
        //Stripe Payment Gateway Setup
        //pk_test_TYooMQauvdEDq54NiTphI7jx
        //pk_test_VFrc7pgPjwEw1pvh2MMfoMUo00NxLI0796
        STPPaymentConfiguration.shared().publishableKey = "pk_test_VFrc7pgPjwEwlpvh2MMfoMUo00NxLI0796"
        
        if UserDefaults.standard.bool(forKey: Constant.userDefaults.kIsRegistered) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            let sideVC = storyboard.instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
            let vc: AKSideMenu = AKSideMenu(contentViewController: homeVC, leftMenuViewController: sideVC, rightMenuViewController: nil)
            vc.interactivePopGestureRecognizerEnabled = true
            vc.panFromEdge = false
            vc.bouncesHorizontally = false
            vc.contentViewShadowEnabled = true
            vc.contentViewShadowOpacity = 10.0
            self.navigation?.pushViewController(vc, animated: true)
        }
        
        if let remoteNotification = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [AnyHashable : Any] {
            if UserDefaults.standard.bool(forKey: Constant.userDefaults.kIsRegistered) {
                print("Did Finish Push Notification: \(remoteNotification)")
                let userId = remoteNotification["gcm.notification.user_id_fk"] as? String ?? "0"
                let chatId = remoteNotification["gcm.notification.c_id"] as? String ?? "0"
                let userName = remoteNotification["gcm.notification.full_name"] as? String ?? ""

                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
                vc.hdrTitle = userName
                vc.opponentId = userId
                vc.chatId = chatId
                let notiType = remoteNotification["gcm.notification.type"] as? String ?? ""
                if notiType == "user_chat" {
                    vc.isMoveForGroup = false
                } else if notiType == "group_chat" {
                    vc.isMoveForGroup = true
                }
                self.navigation?.pushViewController(vc, animated: true)
            }
        }
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        isAppInBackground = true
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        isAppInBackground = true
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        isAppInBackground = false
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        isAppTerminate = true
        currentChatId = ""
    }
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        let handledUberURL = UberAppDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        
        return handledUberURL
    }
    
    func getuserCurrentLattLong(){
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let location = locations.last! as CLLocation
        let currentLattitude =  "\(location.coordinate.latitude)"
        let currentLongitude =  "\(location.coordinate.longitude)"
        UserDefaults.standard.set(currentLattitude, forKey: Constant.userDefaults.kLatitude)
        UserDefaults.standard.set(currentLongitude, forKey: Constant.userDefaults.kLongitude)
    }
}

// MARK: - Push Notification Delegate Methods
extension AppDelegate : UNUserNotificationCenterDelegate, MessagingDelegate {
    /***************************
     Push Notification
     **************************/
    func registerNotificationMethods(application : UIApplication) {
        
        UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
        application.registerForRemoteNotifications()
    }
    // Called when APNs has assigned the device a unique token
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Convert token to string
        let deviceTokenStr = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("APNs device token: \(deviceTokenStr)")
//        UserDefaults.standard.setValue(deviceTokenStr, forKey: Constant.userDefaults.kDeviceToken)
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
        // Print notification payload data
        print("Push notification received: \(data)")
        
        let userImage = data["gcm.notification.profile_image"] as? String ?? ""
        let userName = data["gcm.notification.full_name"] as? String ?? ""
        let message = data["gcm.notification.message"] as? String ?? ""
        let userId = data["gcm.notification.user_id_fk"] as? String ?? "0"
        let chatId = data["gcm.notification.c_id"] as? String ?? "0"
        
        let notiType = data["gcm.notification.type"] as? String ?? ""
        if notiType == "user_chat" {
            if isAppInBackground {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
                vc.hdrTitle = userName
                vc.opponentId = userId
                vc.chatId = chatId
                vc.isMoveForGroup = false
                self.navigation?.pushViewController(vc, animated: true)
            } else {
                if currentChatId == "ChatList" {
                    NotificationCenter.default.post(name: Notification.Name("ChatNotificationName"), object: nil, userInfo: data)
                }
                else if currentChatId == chatId { //Only pass data if message from the current chat thread
                    NotificationCenter.default.post(name: Notification.Name("ChatNotificationName"), object: nil, userInfo: data)
                } else { //Show other user/group message
                    ChatPopUp.showMAChatAlert(imageName: userImage, userName: userName, message: message)
                }
            }
        } else if notiType == "group_chat" {
            if isAppInBackground {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
                vc.hdrTitle = userName
                vc.opponentId = userId
                vc.chatId = chatId
                vc.isMoveForGroup = true
                self.navigation?.pushViewController(vc, animated: true)
            } else {
                if currentChatId == "ChatList" {
                    NotificationCenter.default.post(name: Notification.Name("ChatNotificationName"), object: nil, userInfo: data)
                }
                else if currentChatId == chatId { //Only pass data if message from the current chat thread
                    NotificationCenter.default.post(name: Notification.Name("ChatNotificationName"), object: nil, userInfo: data)
                } else { //Show other user/group message
                    ChatPopUp.showMAChatAlert(imageName: userImage, userName: userName, message: message)
                }
            }
        }
    }
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
        UserDefaults.standard.setValue("1234567890qwertyuiop", forKey: Constant.userDefaults.kDeviceToken)
    }
    
    //MARK: - FCM CONFIGURATION
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
//        let dataDict:[String: String] = ["token": fcmToken]
//        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        
        UserDefaults.standard.setValue(fcmToken, forKey: Constant.userDefaults.kDeviceToken)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    func application(application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        Messaging.messaging().apnsToken = deviceToken as Data
    }
}
